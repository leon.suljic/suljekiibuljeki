package hr.fer.suljekibuljeki.healthwallet;

import hr.fer.suljekibuljeki.healthwallet.dao.DoctorRepository;
import hr.fer.suljekibuljeki.healthwallet.dao.HospitalRepository;
import hr.fer.suljekibuljeki.healthwallet.dao.PatientRepository;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;
import hr.fer.suljekibuljeki.healthwallet.domain.Person;
import hr.fer.suljekibuljeki.healthwallet.service.impl.PatientServiceJpa;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class PatientServiceTest {

    @Mock
    private PatientRepository patientRepository;

    @Mock
    private DoctorRepository doctorRepository;

    @Mock
    private HospitalRepository hospitalRepository;

    @InjectMocks
    private PatientServiceJpa patientService;

    @Test
    public void testCreatePatientWrongEmail() {
        // Test if the patientService returns the correct field when we input an email in the wrong format
        Patient example = PatientTestUtil.defaultPatient();
        List<String> invalidFields = new LinkedList<>();

        Patient p = new Patient();
        p.setEmail("defaultAddress@NONO");
        p.setPassword("defaultPassword");
        p.setFirstName("defName");
        p.setLastName("defSurname");
        p.setPin("0000");
        p.setHealthCardNumber("123456789");

        given(patientRepository.save(any())).willReturn(example);
        invalidFields = patientService.createPatient(p);
        Assert.assertEquals(invalidFields.size(), 1);
        Assert.assertEquals(invalidFields.get(0), "email");
    }

    @Test
    public void testUpdatePatient() {
        List<String> invalidFields = new LinkedList<>();
        Patient example = PatientTestUtil.defaultPatient();
        example.setId(1);
        example.setOib("01234567890");
        example.setAddress("defaultAddress");
        example.setBirthDate(Date.valueOf("1998-06-25"));
        example.setSex(Person.Sex.MALE);
        example.setZipCode("10291");
        example.setEmail("defaultpatient@testing.hw");
        Assert.assertEquals(patientService.findById(1), Optional.empty());

        patientRepository.save(example);
        example.setSex(Person.Sex.FEMALE);

        invalidFields = patientService.updatePatient(example);

        Assert.assertEquals(invalidFields, null);
    }

}
