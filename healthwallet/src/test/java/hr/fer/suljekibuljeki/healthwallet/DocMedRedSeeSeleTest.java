package hr.fer.suljekibuljeki.healthwallet;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

public class DocMedRedSeeSeleTest {

    //Test doctor looking at his medical records

    @Test
    public void testSeeMedRed() throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "C:/Program Files (x86)/Chrome Driver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://healthwallet.herokuapp.com/login");

        WebElement elementEmail = driver.findElement(By.name("username"));
        elementEmail.sendKeys("sandra.ri4521@gmail.com");
        WebElement elementPassword = driver.findElement(By.name("password"));
        elementPassword.sendKeys("doctordoctor");
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/div/div/div/div/div/div/div/div[3]/button/span[1]")).click();

        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div[2]/div/div/div[2]/ul/a[3]/div/div")).click();

        try {
            driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div/div/div/div/div[2]/div/table/tbody/tr[1]")).click();
            driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div[2]/div/div/div[2]/ul/a[2]/div/span[1]")).click();

            try {
                driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div/div/div/div/div/div[3]/div/table/tbody/tr")).click();
                System.out.println("Nalaz uspjesno ocitan");
            }catch (org.openqa.selenium.NoSuchElementException e){
                System.out.println("Ovaj pacijent nema nalaza u sustavu");
            }

        } catch (org.openqa.selenium.NoSuchElementException e) {
            System.out.println("Ovaj lijecnik nema pacjenata u sustavu.");
        }

        driver.close();
        driver.quit();


    }

    public void main(String[] args) throws InterruptedException {
        testSeeMedRed();
    }
}
