package hr.fer.suljekibuljeki.healthwallet;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

public class PatMedRedSeleTest {

    //Tests looking at medical record by patient

    @Test
    public void testPatMedRec() throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "C:/Program Files (x86)/Chrome Driver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://healthwallet.herokuapp.com/login");

        WebElement elementEmail = driver.findElement(By.name("username"));
        elementEmail.sendKeys("ja@hwfer.hr");
        WebElement elementPassword = driver.findElement(By.name("password"));
        elementPassword.sendKeys("ja");
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/div/div/div/div/div/div/div/div[3]/button/span[1]")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div[2]/div/div/div[2]/ul/a[2]/div/div")).click();

        try {
            driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div/div/div/div/div/div[3]/div/table/tbody/tr[1]/td[2]")).click();
            System.out.println("Nalaz uspjesno ocitan");

        } catch (org.openqa.selenium.NoSuchElementException e) {
            System.out.println("Ovaj pacijent nema nalaze u sustavu.");
        }

        driver.close();
        driver.quit();

    }

    public void main(String[] args) throws InterruptedException {
        testPatMedRec();
    }
}
