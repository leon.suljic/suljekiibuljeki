package hr.fer.suljekibuljeki.healthwallet;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


import java.util.concurrent.TimeUnit;

public class HospitalAddDocSeleTest {

    //Tests adding doctors by hospital

    @Test
    public void testAddDoc() throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "C:/Program Files (x86)/Chrome Driver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://healthwallet.herokuapp.com/login");

        WebElement elementEmail = driver.findElement(By.name("username"));
        elementEmail.sendKeys("kbcri");
        WebElement elementPassword = driver.findElement(By.name("password"));
        elementPassword.sendKeys("kbcri");
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/div/div/div/div/div/div/div/div[3]/button/span[1]")).click();

        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div[2]/div/div/div[2]/ul/a[2]/div/div")).click();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div/div/div/div/div[2]/button/span[1]")).click();

        WebElement emailDoc = driver.findElement(By.xpath("//*[@id=\"email\"]"));
        emailDoc.sendKeys("aanaa@hwfer.hr");
        WebElement nameDoc = driver.findElement(By.xpath("//*[@id=\"firstName\"]"));
        nameDoc.sendKeys("ana");
        WebElement surnameDoc = driver.findElement(By.xpath("//*[@id=\"lastName\"]"));
        surnameDoc.sendKeys("ana");
        WebElement oibDoc = driver.findElement(By.xpath("//*[@id=\"oib\"]"));
        oibDoc.sendKeys("11111111111");
        WebElement licenceDoc = driver.findElement(By.xpath("//*[@id=\"licenceNumber\"]"));
        licenceDoc.sendKeys("11111");
        WebElement specialityDoc = driver.findElement(By.xpath("//*[@id=\"speciality\"]"));
        specialityDoc.sendKeys("kirurg");

        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div/div/div/div/div/div/div/div[3]/button[1]")).click();

        try {
            driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div/div/div/div/div[2]/button/span[1]")).click();
            System.out.println("Lijecnik je dodan");

        }catch (org.openqa.selenium.NoSuchElementException e){
            System.out.println("Lijecnik nije dodan");
        }

        driver.close();
        driver.quit();
    }

    public void main(String[] args) throws InterruptedException {
        testAddDoc();
    }
}
