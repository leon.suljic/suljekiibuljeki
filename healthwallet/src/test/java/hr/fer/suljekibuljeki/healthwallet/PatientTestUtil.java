package hr.fer.suljekibuljeki.healthwallet;

import hr.fer.suljekibuljeki.healthwallet.domain.Patient;

public class PatientTestUtil {

    public static Patient defaultPatient() {
        Patient pat = new Patient();
        pat.setEmail("defaultPatient@testing.hw");
        pat.setPassword("defaultPassword");
        pat.setFirstName("defName");
        pat.setLastName("defSurname");
        pat.setPin("0000");
        pat.setHealthCardNumber("123456789");
        return pat;
    }
}
