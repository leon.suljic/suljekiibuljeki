package hr.fer.suljekibuljeki.healthwallet;

import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;

public class HospitalTestUtil {
    public static Hospital defaultHospital() {
        Hospital hospital = new Hospital();
        hospital.setEmail("defaultHospital@testing.hw");
        hospital.setPassword("defaultPassword");
        hospital.setName("Rebro");
        hospital.setOib("01234567890");
        hospital.setAddress("Zagreb");
        hospital.setZipCode("10000");
        return hospital;
    }
}
