package hr.fer.suljekibuljeki.healthwallet;

import hr.fer.suljekibuljeki.healthwallet.dao.DoctorRepository;
import hr.fer.suljekibuljeki.healthwallet.dao.HospitalRepository;
import hr.fer.suljekibuljeki.healthwallet.dao.PatientRepository;
import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;
import hr.fer.suljekibuljeki.healthwallet.service.DoctorService;
import hr.fer.suljekibuljeki.healthwallet.service.impl.DoctorServiceJpa;
import hr.fer.suljekibuljeki.healthwallet.service.impl.HospitalServiceJpa;
import hr.fer.suljekibuljeki.healthwallet.service.impl.PatientServiceJpa;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class HospitalServiceTest {

    @Mock
    private PatientRepository patientRepository;

    @Mock
    private DoctorRepository doctorRepository;

    @Mock
    private HospitalRepository hospitalRepository;

    @Mock
    private DoctorServiceJpa doctorService;

    @InjectMocks
    private HospitalServiceJpa hospitalService;

    @Test
    public void testDeleteDoctorFromHospital() {
        Hospital hospital = HospitalTestUtil.defaultHospital();
        Doctor doctor = DoctorTestUtil.defaultDoctor();

        hospital.setId(1);
        doctor.setId(2);

        Set<Doctor> doctors = new HashSet<>();
        doctors.add(doctor);
        hospital.setDoctors(doctors);

        hospitalRepository.save(hospital);
        doctorRepository.save(doctor);

        Assert.assertEquals(hospitalService.deleteDoctor(hospital, doctor), doctor);
    }
}
