package hr.fer.suljekibuljeki.healthwallet;

import hr.fer.suljekibuljeki.healthwallet.dao.DoctorRepository;
import hr.fer.suljekibuljeki.healthwallet.dao.HospitalRepository;
import hr.fer.suljekibuljeki.healthwallet.dao.PatientRepository;
import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;
import hr.fer.suljekibuljeki.healthwallet.service.impl.DoctorServiceJpa;
import hr.fer.suljekibuljeki.healthwallet.service.impl.HospitalServiceJpa;
import hr.fer.suljekibuljeki.healthwallet.service.impl.PatientServiceJpa;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class DoctorServiceTest {

    @Mock
    private DoctorRepository doctorRepository;

    @Mock
    private PatientRepository patientRepository;

    @Mock
    private HospitalRepository hospitalRepository;

    @Mock
    private PatientServiceJpa patientService;

    @Mock
    private HospitalServiceJpa hospitalService;

    @InjectMocks
    private DoctorServiceJpa doctorService;

    @Test
    public void testDoctorAddPatient() {
        Patient patient = PatientTestUtil.defaultPatient();
        Doctor doctor = DoctorTestUtil.defaultDoctor();

        patientRepository.save(patient);

        boolean result = doctorService.addPatient(patient, doctor.getEmail());

        Assert.assertEquals(result, false);
    }

    @Test
    public void testDoctorAdd() {
        Doctor doctor = DoctorTestUtil.defaultDoctor();
        doctor.setSpeciality("kirurg");
        doctor.setLicenceNumber("12345");

        when(doctorRepository.save(any(Doctor.class))).thenReturn(doctor);
        Doctor saved = doctorService.createDoctor(doctor);

        Assert.assertEquals(doctor, saved);
    }
}
