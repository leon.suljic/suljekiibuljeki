package hr.fer.suljekibuljeki.healthwallet;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

public class DocAddPatientSeleTest {

    //Tests adding patients by doctors

    @Test
    public void testAddPatient() throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "C:/Program Files (x86)/Chrome Driver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://healthwallet.herokuapp.com/login");

        WebElement elementEmail = driver.findElement(By.name("username"));
        elementEmail.sendKeys("sandra.ri4521@gmail.com");
        WebElement elementPassword = driver.findElement(By.name("password"));
        elementPassword.sendKeys("doctordoctor");
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/div/div/div/div/div/div/div/div[3]/button/span[1]")).click();

        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div[2]/div/div/div[2]/ul/a[3]/div/div")).click();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div/div/div/div/div[2]/button/span[1]")).click();

        WebElement oib = driver.findElement(By.xpath("//*[@id=\"patientOIB\"]"));
        oib.sendKeys("25332080302");
        WebElement pin = driver.findElement(By.xpath("//*[@id=\"patientPIN\"]"));
        pin.sendKeys("7569");

        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div/div/div/div/div/div/div/div[3]/button[1]")).click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        try {
            driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div/div/div/div/div[2]/button"));
            driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div/div/div/div/div[2]/button")).click();
            System.out.println("Pacijent je dodan");

        }catch (org.openqa.selenium.NoSuchElementException e){
            System.out.println("Pacijent nije dodan");
        }

        driver.close();
        driver.quit();
    }

    public void main(String[] args) throws InterruptedException {
        testAddPatient();
    }
}
