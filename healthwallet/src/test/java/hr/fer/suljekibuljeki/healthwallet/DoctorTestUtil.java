package hr.fer.suljekibuljeki.healthwallet;

import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;

public class DoctorTestUtil {
    public static Doctor defaultDoctor() {
        Doctor doctor = new Doctor();
        doctor.setEmail("defaultPatient@testing.hw");
        doctor.setPassword("defaultPassword");
        doctor.setFirstName("defName");
        doctor.setLastName("defSurname");
        return doctor;
    }
}
