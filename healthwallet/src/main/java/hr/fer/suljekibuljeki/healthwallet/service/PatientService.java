package hr.fer.suljekibuljeki.healthwallet.service;

import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.MedicalRecord;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface PatientService {
    List<Patient> listAll();
    Patient fetch(int id);
    Optional<Patient> findById(int id);
    Optional<Patient> findByPin(String pin);
    Optional<Patient> findByOib(String oib);
    Optional<Patient> getByEmail(String email);
    List<String> createPatient(Patient patient);
    List<Doctor> getDoctors(int id);
    int countByPin(String pin);
    int countByConfirmationLink(String confirmationLink);
    Patient deletePatient(int id);
    List<String> continueRegistration(Patient patient);
    Patient findByOIBAndPIN(String patientOIB, String patientPIN);
    List<String> updatePatient(Patient patient);
    Patient addSupervisor(Patient patient, Patient supervisor);
    void store(Patient patient);
    List<MedicalRecord> searchAndFilterRecords(Patient patient, String[] searchedWords, Date dateFrom, Date dateTo);
    Patient findInPatients(Set<Patient> allPatients, int patientId);
    Set<MedicalRecord> filterMedRecords(Patient patient, Date dateFrom, Date dateTo, String hospital, String department, String doctor, String diagnosis);
    int numberOfDoctors(Patient patient);
    int countSupervisedPatients(Patient patient);
    List<Patient> getAllSupervised(Patient patient);
    Set<Doctor> getLastFiveDoctors(Patient patient);
}
