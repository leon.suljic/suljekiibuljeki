package hr.fer.suljekibuljeki.healthwallet.domain;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Size;
import java.util.Date;

@MappedSuperclass
public class Person extends User {

    //================================================================================
    // Properties
    //================================================================================

    @Size(min = 11, max = 11)
    private String oib;

    private String firstName;

    private String lastName;

    private Date birthDate;

    private Sex sex;

    private String address;

    @Size(min = 5, max = 5)
    private String zipCode;

    public enum Sex {MALE, FEMALE, OTHER}

    //================================================================================
    // Accessors
    //================================================================================

    public String getOib() {
        return oib;
    }

    public void setOib(String oib) {
        this.oib = oib;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
