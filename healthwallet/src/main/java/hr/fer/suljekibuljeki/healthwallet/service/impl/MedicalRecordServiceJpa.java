package hr.fer.suljekibuljeki.healthwallet.service.impl;

import hr.fer.suljekibuljeki.healthwallet.dao.DoctorRepository;
import hr.fer.suljekibuljeki.healthwallet.dao.HospitalRepository;
import hr.fer.suljekibuljeki.healthwallet.dao.MedicalRecordRepository;
import hr.fer.suljekibuljeki.healthwallet.dao.PatientRepository;
import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;
import hr.fer.suljekibuljeki.healthwallet.domain.MedicalRecord;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;
import hr.fer.suljekibuljeki.healthwallet.service.MedicalRecordService;
import hr.fer.suljekibuljeki.healthwallet.service.RequestDeniedException;
import hr.fer.suljekibuljeki.healthwallet.service.S3Services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Service
public class MedicalRecordServiceJpa implements MedicalRecordService {

    @Autowired
    private MedicalRecordRepository medRecordRepo;

    @Autowired
    private PatientRepository patientRepo;

    @Autowired
    private DoctorRepository doctorRepo;

    @Autowired
    private HospitalRepository hospitalRepo;

    @Autowired
    private S3Services s3Services;

    @Override
    public List<MedicalRecord> listAll() {
        return sortMedRecords(medRecordRepo.findAll());
    }

    @Override
    public MedicalRecord createMedicalRecord(MedicalRecord record, Patient patient) {
        Assert.notNull(record, "Medical record object must be given!");
        record.setPatient(patient);
        return createMedicalRecord(record);
    }

    @Override
    public MedicalRecord createMedicalRecord(MedicalRecord record) {
        Assert.notNull(record, "Medical record object must be given!");
        Assert.notNull(record.getPatient(), "Patient must be given!");
        Assert.isNull(record.getRecordID(), "Medical record ID must be null and not " + record.getRecordID());
        return medRecordRepo.save(record);
    }

    @Override
    public List<MedicalRecord> getAllForPatient(Patient patient) {
        return sortMedRecords(medRecordRepo.getByPatient(patient));
    }

    public List<MedicalRecord> sortMedRecords(List<MedicalRecord> medrecords){
        return medrecords.stream().sorted(Comparator.comparing(MedicalRecord::getDate).reversed()).collect(Collectors.toList());
    }

    @Override
    public List<MedicalRecord> getAllForPatient(int patientId) {
        Optional<Patient> patient = patientRepo.findById(patientId);
        if (patient.isEmpty())
            return Collections.emptyList();
        return getAllForPatient(patient.get());
    }

    @Override
    public List<MedicalRecord> getAllForDoctor(Doctor doctor) {
        return sortMedRecords(medRecordRepo.getByDoctor(doctor));
    }

    @Override
    public List<MedicalRecord> getAllForDoctor(int doctorId) {
        Optional<Doctor> doctor = doctorRepo.findById(doctorId);
        if (doctor.isEmpty())
            return Collections.emptyList();
        return getAllForDoctor(doctor.get());
    }

    @Override
    public List<MedicalRecord> getAllForHospital(Hospital hospital) {
        return sortMedRecords(medRecordRepo.getByHospital(hospital));
    }

    @Override
    public List<MedicalRecord> getAllForHospital(int hospitalId) {
        Optional<Hospital> hospital = hospitalRepo.findById(hospitalId);
        if (hospital.isEmpty())
            return Collections.emptyList();
        return getAllForHospital(hospital.get());
    }

    @Override
    public String deleteRecord(int recordId) {
        MedicalRecord medRec = medRecordRepo.getOne(recordId);
        if (s3Services.deleteFileFromS3Bucket(medRec.getFile()).equals("Successfully deleted")){
            medRecordRepo.delete(medRecordRepo.getOne(recordId));
        } else {
            throw new RequestDeniedException("Amazon odbija surađivati");
        }
        return "Success!";
    }

    @Override
    public int countByHospital(Hospital hospital){
        return medRecordRepo.countByHospital(hospital);

	}
	@Override
    public int numberOfRecords(Patient patient){
        return getAllForPatient(patient).size();
    }

    @Override
    public int numberOfRecords(Doctor doctor){
        return getAllForDoctor(doctor).size();
    }

    @Override
    public List<MedicalRecord> getLastFiveMedRecords(Doctor doctor) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date today = Calendar.getInstance().getTime();
        List<MedicalRecord> allMed = getAllForDoctor(doctor);
        allMed.sort(Comparator.comparing(MedicalRecord::getDate));
        if (allMed.size()<6) {
            return allMed;
        } else {
            return allMed.subList((allMed.size() - 5), allMed.size() );
        }
    }

    @Override
    public List<MedicalRecord> getLastFiveMedRecords(Patient patient) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date today = Calendar.getInstance().getTime();
        List<MedicalRecord> allMed = getAllForPatient(patient);
        allMed.sort(Comparator.comparing(MedicalRecord::getDate).reversed());
        if (allMed.size()<6) {
            return allMed;
        } else {
            return allMed.subList((allMed.size() - 5), allMed.size());
        }
    }
}
