package hr.fer.suljekibuljeki.healthwallet.rest;

public class DoctorDTO {
    private String email;
    private String firstName;
    private String lastName;
    private String specialty;
    private String hospital;

    public DoctorDTO(String email, String firstName, String lastName, String specialty, String hospital) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.specialty = specialty;
        this.hospital = hospital;
}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getHospital() { return hospital; }

    public void setHospital(String hospital) { this.hospital = hospital; }

    public String getSpecialty() { return specialty; }

    public void setSpecialty(String specialty) { this.specialty = specialty; }
}
