package hr.fer.suljekibuljeki.healthwallet.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

public interface S3Services {
    URL downloadFile(String file) throws IOException, InterruptedException;
    String uploadFile(MultipartFile file);
    String deleteFileFromS3Bucket(String fileName);
    String getFileInString(URL url) throws IOException;
}
