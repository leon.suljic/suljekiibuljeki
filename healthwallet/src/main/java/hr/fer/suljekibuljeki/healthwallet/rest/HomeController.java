package hr.fer.suljekibuljeki.healthwallet.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;
import hr.fer.suljekibuljeki.healthwallet.domain.User;
import hr.fer.suljekibuljeki.healthwallet.service.LoginNameNotFoundException;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("api/home")
public class HomeController {

    @Autowired
    private HospitalController hospitalController;

    @Autowired
    private DoctorController doctorController;

    @Autowired
    private PatientController patientController;

    @GetMapping("/home")
    public String home() {
        return "<h1>Health Wallet Homepage</h>";
    }

    @GetMapping("/current-user")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Map<String, Object> current_user() {
        Map<String,Object> map = new HashMap<>();
        //provjera da li je admin
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String test = principal instanceof UserDetails ? ((UserDetails)principal).getUsername() : principal.toString();
        if(test.equalsIgnoreCase("admin")){
            map.put("role", "admin");
            map.put("user", null);
            return map;
        }

        Optional<Patient> patient = patientController.user();
        if(patient.isPresent()) {
            map.put("role", "patient");
            map.put("user", patient.get());
            return map;
        }

        Optional<Hospital> hospital = hospitalController.user();
        if(hospital.isPresent()) {
            map.put("role", "hospital");
            map.put("user", hospital.get());
            return map;
        }

        Optional<Doctor> doctor = doctorController.user();
        if(doctor.isPresent()) {
            map.put("role", "doctor");
            map.put("user", doctor.get());
            return map;
        }

        throw new LoginNameNotFoundException("user not found");
    }

    /*
    @RequestMapping(value = { "/", "/home" }, method = RequestMethod.GET)
    public String homePage(ModelMap model) {
        model.addAttribute("greeting", "Hi, Welcome to mysite");
        return "welcome";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String adminPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "admin";
    }

    @RequestMapping(value = "/db", method = RequestMethod.GET)
    public String dbaPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "dba";
    }

    @RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "accessDenied";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage() {
        return "login";
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";//You can redirect wherever you want, but generally it's a good idea to show login screen again.
    }

    private String getPrincipal(){
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }

    /*

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";//You can redirect wherever you want, but generally it's a good practice to show login screen again.
    }
*/

}
