package hr.fer.suljekibuljeki.healthwallet.rest;

import hr.fer.suljekibuljeki.healthwallet.HealthwalletApplication;
import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;
import hr.fer.suljekibuljeki.healthwallet.domain.MedicalRecord;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;
import hr.fer.suljekibuljeki.healthwallet.service.DoctorService;
import hr.fer.suljekibuljeki.healthwallet.service.HospitalService;
import hr.fer.suljekibuljeki.healthwallet.service.MedicalRecordService;
import hr.fer.suljekibuljeki.healthwallet.service.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/hospitals")
public class HospitalController {

    @Autowired
    private HospitalService hospitalService;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private MedicalRecordService medicalRecordService;

    @GetMapping("")
    @Secured("ROLE_ADMIN")
    public List<Hospital> listHospitals() {
        return hospitalService.listAll();
    }

    @GetMapping("/doctors")
    @Secured("ROLE_HOSPITAL")
    public List<Doctor> listDoctors() {
        return hospitalService.getAllDoctors(user().get().getId());
    }

    @PostMapping("/register")
    public ResponseEntity<List<String>> createHospital(@RequestBody Hospital hospital) {
        List<String> errors = new LinkedList<>();

        if (hospital.getPassword().isEmpty() || !hospital.getPassword().matches("^[a-zA-Z0-9]{8,}$"))
            errors.add("password");
        else {
            String encPass = HealthwalletApplication.pswdEncoder().encode(hospital.getPassword());
            hospital.setPassword(encPass);
        }
        if (hospital.getName().isEmpty() || !hospital.getName().matches("^[a-zšđžćčŠĐŽĆČ\\sA-Z-]+$"))
            errors.add("name");
        if (hospital.getOib().isEmpty() || !hospital.getOib().matches("^[0-9]{11}$"))
            errors.add("oib");
        if (hospital.getEmail().isEmpty() || !hospital.getEmail().matches("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,6}$"))
            errors.add("email");
        if (hospital.getAddress().isEmpty() || !hospital.getAddress().matches("^[a-zA-Z0-9šđžćčŠĐŽĆČ.\\s]+$"))
            errors.add("address");
        if (hospital.getZipCode().isEmpty() || !hospital.getZipCode().matches("^[0-9]{5}$"))
            errors.add("zipCode");

        if (!errors.isEmpty())
            return ResponseEntity.badRequest().body(errors);
        else {
            try {
                hospitalService.createHospital(hospital);
            }
            catch (Exception e) {
                e.printStackTrace();
                errors.add("email");
                return ResponseEntity.badRequest().body(errors);
            }
            return ResponseEntity.ok().build();
        }
    }

    @PostMapping("/add-doctor")
    @Secured("ROLE_HOSPITAL")
    public ResponseEntity<List<String>> createDoctor(@RequestBody Doctor doctor) {
        doctor.setEmail(doctor.getEmail().toLowerCase());
        Optional<Hospital> hospital = hospitalService.getByEmail(user().get().getEmail());
        if (hospital.isEmpty())
            return null;

        List<String> errors = new LinkedList<>();
        String confirmationLink = generateDoctorLink();
        doctor.setConfirmationLink(confirmationLink);

        if (doctor.getEmail().isEmpty() || !doctor.getEmail().matches("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,6}$"))
            errors.add("email");
        if (doctor.getFirstName().isEmpty() || !doctor.getFirstName().matches("^[a-z\\sA-ZšđžćčŠĐŽĆČ]+$"))
            errors.add("firstName");
        if (doctor.getLastName().isEmpty() || !doctor.getLastName().matches("^[a-z\\sA-ZšđžćčŠĐŽĆČ]+$"))
            errors.add("lastName");
        if (doctor.getOib().isEmpty() || !doctor.getOib().matches("^[0-9]{11}$"))
            errors.add("oib");
        if (doctor.getLicenceNumber().isEmpty() || !doctor.getLicenceNumber().matches("^[0-9]+$"))
            errors.add("licenceNumber");
        if (doctor.getSpeciality().isEmpty() || !doctor.getSpeciality().matches("^[a-z\\sA-ZšđžćčŠĐŽĆČ]+$"))
            errors.add("speciality");

        // password must be null when sent from frontend, this check enables compatibility with postman
        if (doctor.getPassword() != null) {
            String encPass = HealthwalletApplication.pswdEncoder().encode(doctor.getPassword());
            doctor.setPassword(encPass);
        }

        if (!errors.isEmpty())
            return ResponseEntity.badRequest().body(errors);
        else {
            hospitalService.createDoctor(doctor, hospital.get());
            return ResponseEntity.ok().build();
        }
    }

    /**
     * Update information for currently logged in hospital
     *
     * @param hospital - Hospital object which contains updated hospital information
     * @return List<String> - List with the names of all invalid fields in the update JSON
     */
    @PutMapping("/update-my-info")
    @Secured("ROLE_HOSPITAL")
    public ResponseEntity<List<String>> updateHospital(@RequestBody Hospital hospital) {

        Optional<Hospital> currHospital = user();
        if (currHospital.isEmpty())
            throw new RequestDeniedException("Must be signed in as hospital!");

        if (!currHospital.get().getId().equals(hospital.getId()))
            throw new RequestDeniedException("Can not change other hospital's details!");

        hospital.setEmail(hospital.getEmail().toLowerCase());

        List<String> errors = hospitalService.updateHospital(hospital);
        if (errors != null)
            return ResponseEntity.badRequest().body(errors);
        else
            return ResponseEntity.ok().build();
    }

    /**
     * Update information for doctor with doctorId
     *
     * @param doctorId - Id of doctor whose information should be updated
     * @param doctor   - Doctor object which contains updated doctor information
     * @return List<String> - List with the names of all invalid fields in the update JSON
     */
    @PutMapping("/update-doctor/{id}")
    @Secured("ROLE_HOSPITAL")
    public ResponseEntity<List<String>> updateDoctor(@PathVariable("id") int doctorId, @RequestBody Doctor doctor) {

        Optional<Doctor> dbDoctor = doctorService.findById(doctorId);
        if (dbDoctor.isEmpty())
            ResponseEntity.badRequest().header("Doctor with id " + doctorId + " does not exist!").body(new ArrayList<>());

        Optional<Hospital> currHospital = user();
        if (currHospital.isEmpty())
            return ResponseEntity.badRequest().header("Must be signed in as hospital!").body(new ArrayList<>());

        doctor.setEmail(doctor.getEmail().toLowerCase());

        Set<Doctor> currHospitalDoctors = currHospital.get().getDoctors();
        //Update doctor info only if current hospital contains that doctor
        if (!currHospitalDoctors.contains(dbDoctor.get()))
            return ResponseEntity.badRequest().header("Doctor is not from this hospital").body(new ArrayList<>());

        Doctor doc = dbDoctor.get();
        doc.setFirstName(doctor.getFirstName());
        doc.setLastName(doctor.getLastName());
        doc.setSpeciality(doctor.getSpeciality());
        doc.setBirthDate(doctor.getBirthDate());
        doc.setOib(doctor.getOib());
        doc.setAddress(doctor.getAddress());
        doc.setZipCode(doctor.getZipCode());
        doc.setSex(doctor.getSex());
        doc.setLicenceNumber(doctor.getLicenceNumber());

        List<String> errors = doctorService.updateDoctor(doc);
        if (errors != null)
            return ResponseEntity.badRequest().body(errors);
        else
            return ResponseEntity.ok().build();
    }

    @DeleteMapping("/delete-doctor/{id}")
    @Secured("ROLE_HOSPITAL")
    public Doctor deleteDoctor(@PathVariable("id") int doctorId) {
        return hospitalService.deleteDoctor(user().get().getId(), doctorId);
    }

    public Optional<Hospital> user() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails)
            username = ((UserDetails) principal).getUsername();
        else
            username = principal.toString();
        return hospitalService.getByEmail(username);
    }


    public String generateDoctorLink() {
        boolean bool = true;
        int i;
        String confirmationLink = "";
        while (bool) {
            confirmationLink = UUID.randomUUID().toString();
            bool = confirmationDoctorLinkExists(confirmationLink);
        }
        return confirmationLink;
    }

    public boolean confirmationDoctorLinkExists(String confLink) {
        if (doctorService.countByConfirmationLink(("https://healthwallet.herokuapp.com/doctors/register" + confLink).toString()) > 0) {
            return true;
        }
        return false;
    }

    /**
     * Method that deletes a hospital along with its doctors, but keeps the patients
     *
     * @param hospitalId - Id of the hospital that needs to be deleted
     * @return Hospital - Hospital object that represents the deleted hospital
     */
    @DeleteMapping("/remove/{id}")
    @Secured("ROLE_ADMIN")
    public Hospital deleteHospital(@PathVariable("id") int hospitalId) {
        Hospital hospital = hospitalService.fetch(hospitalId);
        hospitalService.deleteHospital(hospitalId);
        return hospital;
    }

    /**
     * Method that deletes the current logged in hospital, also it deletes all registered doctors by that hospital.
     *
     * @return Hospital - Hospital object that was deleted from the database
     */
    @DeleteMapping("/remove-self")
    @Secured("ROLE_HOSPITAL")
    public Hospital removeSelf() {
        Optional<Hospital> currentUser = user();
        if (currentUser.isEmpty()) {
            return null;
        }
        Hospital hospital = currentUser.get();

        int userId = hospital.getId();
        return hospitalService.deleteHospital(userId);
    }


    @GetMapping("/statistics")
    @Secured("ROLE_HOSPITAL")
    public ResponseEntity<StatsDTO> getStatistics() {
        Optional<Hospital> currentUser = user();
        if (currentUser.isEmpty()) {
            return ResponseEntity.badRequest().header("You must be signed in as hospital!").body(new StatsDTO());
        }
        Hospital hospital = currentUser.get();

        StatsDTO stats = new StatsDTO();
        List<Doctor> allDoctors = hospitalService.getAllDoctors(hospital.getId());
        List<Patient> allPatients = new ArrayList<>(allDoctors.stream().flatMap(d -> d.getPatients().stream()).collect(Collectors.toSet()));//transfer elements from stream to set to avoid duplicates
        List<MedicalRecord> allMedicalRecords = medicalRecordService.getAllForHospital(hospital);
        allMedicalRecords = allMedicalRecords.stream().filter(x -> x.getDoctor() != null).collect(Collectors.toList());
        allMedicalRecords.sort(Comparator.comparing(MedicalRecord::getDate).reversed());
        Set<Patient> last5patients = new LinkedHashSet<>(5);
        Set<Doctor> last5doctors = new LinkedHashSet<>(5);
        List<Doctor> mostActive5doctors = new ArrayList<>(5);
        for (MedicalRecord record : allMedicalRecords) {
            boolean fst = false, snd = false;
            if (last5doctors.size() < 5)
                last5doctors.add(record.getDoctor());
            else fst = true;
            if (last5patients.size() < 5)
                last5patients.add(record.getPatient());
            else snd = true;
            if (fst && snd) break;
        }

        Map<Object, Long> map = allMedicalRecords.stream().map(r -> r.getDoctor()).collect(Collectors.groupingBy(d -> d, Collectors.counting()));
        List<Map.Entry<Object, Long>> list = new ArrayList<>(map.entrySet());
        list.sort(Comparator.comparingLong(d -> ((Map.Entry<Object, Long>) d).getValue()).reversed());
        for (int l = list.size(), i = 0; i < 5 && i < l; i++)
            mostActive5doctors.add((Doctor) list.get(i).getKey());

        stats.setNumberOfDoctors(allDoctors.size());
        stats.setNumberOfPatients(allPatients.size());
        stats.setNumberOfRecords(allMedicalRecords.size());

        stats.setRecentRecords(allMedicalRecords.size() <= 5 ? allMedicalRecords : allMedicalRecords.subList(0, 5));
        stats.setRecentDoctors(new ArrayList<>(last5doctors));
        stats.setRecentPatients(new ArrayList<>(last5patients));
        stats.setMostActiveDoctors(mostActive5doctors);
        return ResponseEntity.ok(stats);
    }
}
