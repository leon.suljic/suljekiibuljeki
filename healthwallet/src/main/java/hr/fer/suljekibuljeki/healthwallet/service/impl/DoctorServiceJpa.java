package hr.fer.suljekibuljeki.healthwallet.service.impl;

import hr.fer.suljekibuljeki.healthwallet.dao.DoctorRepository;
import hr.fer.suljekibuljeki.healthwallet.dao.HospitalRepository;
import hr.fer.suljekibuljeki.healthwallet.dao.MedicalRecordRepository;
import hr.fer.suljekibuljeki.healthwallet.dao.PatientRepository;
import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;
import hr.fer.suljekibuljeki.healthwallet.domain.MedicalRecord;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;
import hr.fer.suljekibuljeki.healthwallet.rest.StatsDTO;
import hr.fer.suljekibuljeki.healthwallet.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DoctorServiceJpa implements DoctorService {

    @Autowired
    private DoctorRepository doctorRepo;

    @Autowired
    private PatientRepository patientRepo;

    @Autowired
    private HospitalRepository hospitalRepo;

    @Autowired
    private PatientService patientService;

    @Autowired
    private HospitalService hospitalService;

    @Autowired
    private MedicalRecordService medicalRecordService;

    @Autowired
    private MedicalRecordRepository medicalRecordRepository;

    @Override
    public List<Doctor> listAll() {
        return doctorRepo.findAll();
    }

    public int countByConfirmationLink(String confirmationLink) {
        return doctorRepo.countByConfirmationLink(confirmationLink);
    }
    
    @Override
    public Doctor createDoctor(Doctor doctor) {
    	Assert.notNull(doctor, "Doctor object must be given!");
    	Assert.isNull(doctor.getId(), "Doctor ID must be null and not " + doctor.getId());
    	doctor.setEmail(doctor.getEmail().toLowerCase());
    	if(doctorRepo.countByEmail(doctor.getEmail()) > 0 || patientRepo.countByEmail(doctor.getEmail()) > 0
    	    || hospitalRepo.countByEmail(doctor.getEmail()) > 0){
    		throw new RequestDeniedException("User with email " + doctor.getEmail() + " already exists!");
    	}
    	return doctorRepo.save(doctor);
    }

    @Override
    public Optional<Doctor> findById(int doctorId) {
        return doctorRepo.findById(doctorId);
    }

    @Override
    public Doctor fetch(int doctorId) {
        return findById(doctorId).orElseThrow();
    }

    @Override
    public Optional<Doctor> getByEmail(String email) {
        return listAll().stream().filter(x -> x.getEmail().equals(email)).findFirst();
    }

    @Override
    public Optional<Doctor> findByOib(String oib) {
        return listAll().stream().filter(x -> x.getOib().equals(oib)).findFirst();
    }

    @Override
    public List<Patient> listAllPatients(int doctorId) {
        Doctor doctor = fetch(doctorId);
        List<Patient> patients = new LinkedList<>();
        patients.addAll(doctor.getPatients());
        return patients;
    }

    @Override
    public Doctor registerPassword(Doctor doctor) {
        Assert.notNull(doctor, "Doctor object must be given!");

        return doctorRepo.save(doctor);
    }

    @Override
    public Boolean addPatient(Patient patient, String doctorEmail) {
        Optional<Doctor> doctor = getByEmail(doctorEmail);
        if (doctor.isEmpty()) return false;
        boolean result = doctor.get().getPatients().add(patient);
        doctorRepo.save(doctor.get());
        return result;
    }

    @Override
    public Boolean removePatient(int doctorId, int patientId) {
        Doctor doctor = fetch(doctorId);
        Set<Patient> patients = doctor.getPatients();
        boolean removed = patients.remove(patientService.fetch(patientId));
        if(removed){
            doctorRepo.save(doctor);
        }
        return removed;
    }

    @Override
    public void unbindAllPatients(int doctorId) {
        Doctor doctor = fetch(doctorId);
        Set<Patient> patients = doctor.getPatients();
        for(Patient patient : patients) {
            removePatient(doctorId, patient.getId());
        }
    }

    @Override
    public void unbindAllMedRecords(int doctorId) {
        Doctor doctor = fetch(doctorId);
        Set<Patient> patients = doctor.getPatients();
        for (MedicalRecord medRecord : medicalRecordService.getAllForDoctor(doctorId)){
            medRecord.setOldRecordDoctor(medRecord.getDoctor().getFirstName() + " " + medRecord.getDoctor().getLastName());
            medRecord.setDoctor(null);
            medicalRecordRepository.save(medRecord);
        }
    }

    @Override
    public List<String> updateDoctor(Doctor doctor){
        List<String> invalidFields = new LinkedList<>();

        //Assert.notNull(doctor, "Doctor object must be given!");
      //  Assert.notNull(doctor.getId(), "Doctor ID can not be null!");

        if (!doctor.getSpeciality().matches("^[a-z\\sA-ZšđžćčŠĐŽĆČ]+$"))
            invalidFields.add("speciality");
        if (!doctor.getFirstName().matches("^[a-z\\sA-ZšđžćčŠĐŽĆČ]+$"))
            invalidFields.add("firstName");
        if (!doctor.getLastName().matches("^[a-z\\sA-ZšđžćčŠĐŽĆČ]+$"))
            invalidFields.add("lastName");
        if (!doctor.getEmail().matches("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,6}$"))
            invalidFields.add("email");
        if (!doctor.getOib().matches("^[0-9]{11}$"))
            invalidFields.add("oib");
        if (!doctor.getAddress().matches("^[a-zA-Z0-9šđžćčŠĐŽĆČ.\\s]*$"))
            invalidFields.add("address");
        if (!doctor.getLicenceNumber().matches("^[0-9]*$"))
            invalidFields.add("licenceNumber");
        if (!doctor.getZipCode().matches("^[0-9]{5}$"))
            invalidFields.add("zipCode");
        if (doctor.getBirthDate() == null)
            invalidFields.add("birthDate");
        if (doctor.getSex() == null)
            invalidFields.add("sex");

        if (invalidFields.size() == 0) doctorRepo.save(doctor);

        return invalidFields.size() == 0 ? null : invalidFields;
    }

    @Override
    public Hospital findBindedHospital(int doctorId) {
        Doctor doctor = fetch(doctorId);
        List<Hospital> hospitals = hospitalService.listAll().stream().filter(x -> x.getDoctors().contains(doctor)).collect(Collectors.toList());
        if(hospitals.size() == 1) {
            return hospitals.get(0);
        } else if (hospitals.size() == 0) {
            return null;
        } else {
            throw new IllegalArgumentException("1 doctor can't be in 2 or more hospitals under the same email!");
        }
    }

    @Override
    public Doctor deleteDoctor(int doctorId) {
        Doctor doctor = fetch(doctorId);
        Set<Patient> patients = doctor.getPatients();
        // Unbind all medical records
        unbindAllMedRecords(doctorId);

        // Unbind all the patients
        for(Patient patient : patients) {
            removePatient(doctorId, patient.getId());
        }

        // Unbind from a hospital
        Hospital hospital = findBindedHospital(doctorId);
        if(hospital != null) {
            hospitalService.unbindDoctor(hospital.getId(), doctorId);
        }

        doctorRepo.delete(doctor);
        return doctor;
    }

    @Override
    public Doctor deleteDoctorAlternate(Doctor doctor) {
        // Unbind all the patients
        Set<Patient> patients = doctor.getPatients();
        unbindAllMedRecords(doctor.getId());
        for(Patient patient : patients) {
            removePatient(doctor.getId(), patient.getId());
        }

        // Unbind from a hospital
        Hospital hospital = findBindedHospital(doctor.getId());
        if(hospital != null) {
            hospitalService.unbindDoctor(hospital.getId(), doctor.getId());
        }

        doctorRepo.delete(doctor);
        return doctor;
    }

    @Override
    public void store(Doctor doctor) {
            doctorRepo.save(doctor);
    }

    @Override
    public int numberOfPatients(Doctor doctor){
        return listAllPatients(doctor.getId()).size();
    }

    @Override
    public Set<Patient> getLastFivePatients (Doctor doctor){
        List<MedicalRecord> allMedicalRecords = medicalRecordService.getAllForDoctor(doctor);
        allMedicalRecords.sort(Comparator.comparing(MedicalRecord::getDate).reversed());
        Set<Patient> last5patients = new LinkedHashSet<>(5);
        for (MedicalRecord record : allMedicalRecords) {
            boolean fst = false, snd = false;
            if (last5patients.size() < 6) {
                last5patients.add(record.getPatient());
            }
        }
        return last5patients;

    }
}
