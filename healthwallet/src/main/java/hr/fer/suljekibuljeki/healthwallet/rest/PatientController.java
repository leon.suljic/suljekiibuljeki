package hr.fer.suljekibuljeki.healthwallet.rest;

import hr.fer.suljekibuljeki.healthwallet.HealthwalletApplication;
import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.MedicalRecord;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;
import hr.fer.suljekibuljeki.healthwallet.email.SendEmailTLS;

import hr.fer.suljekibuljeki.healthwallet.service.DoctorService;
import hr.fer.suljekibuljeki.healthwallet.service.MedicalRecordService;
import hr.fer.suljekibuljeki.healthwallet.service.PatientService;
import hr.fer.suljekibuljeki.healthwallet.service.RequestDeniedException;

import hr.fer.suljekibuljeki.healthwallet.service.*;
import hr.fer.suljekibuljeki.healthwallet.service.uploadexceptions.StorageException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Controller class that communicates with the front end through REST API.
 * Accepts requests and sends responses.
 * <p>
 * All requests are done through the /api/patients mapping.
 */
@RestController
@RequestMapping("api/patients")

public class PatientController {

    /**
     * PatientService type that represents the business logic of patients in the application
     */
    @Autowired
    private PatientService patientService;

    /**
     * DoctorService type that represents the business logic of doctors in the application
     */
    @Autowired
    private DoctorService doctorService;

    /**
     * MedicalRecordService type that represents the business logic of medical records in the application
     */
    @Autowired
    private MedicalRecordService medicalRecordService;


    @Autowired
    private S3Services s3Services;

    /**
     * Get all patients in the database
     *
     * @return List - list of all patients in the database
     */
    @GetMapping("")
    @Secured("ROLE_ADMIN")
    public List<Patient> listPatients() {
        return patientService.listAll();
    }

    /**
     * Create a Patient object from JSON, encrypt his password and add him to the database
     *
     * @param patient - Patient object representing a patient
     * @return Patient - Patient object with an encrypted password
     */
    @PostMapping("/register")
    public ResponseEntity<List<String>> createPatient(@RequestBody Patient patient) {
        List<String> errors = new LinkedList<>();
        patient.setEmail(patient.getEmail().toLowerCase());

        if (patient.getPassword().isEmpty() || !patient.getPassword().matches("^[a-zA-Z0-9]{8,}$")) {
            errors.add("password"); // Forbidden characters in password
            return ResponseEntity.badRequest().body(errors);
        } else {
            // Password is ok, encode it
            String encPass = HealthwalletApplication.pswdEncoder().encode(patient.getPassword());
            patient.setPassword(encPass);
        }

        String confirmationLink = generateConfirmLink();
        patient.setConfirmationLink(confirmationLink);

        errors.addAll(patientService.createPatient(patient));

        if (!errors.isEmpty())
            return ResponseEntity.badRequest().body(errors);
        else
            return ResponseEntity.ok().build();
    }

    /**
     * Update information for currently logged in patient
     *
     * @param patient - Patient object which contains updated patient information
     * @return List<String> - List with the names of all invalid fields in the update JSON
     */
    @PutMapping("/update-my-info")
    @Secured("ROLE_PATIENT")
    public ResponseEntity<List<String>> updatePatient(@RequestBody Patient patient) {
        patient.setEmail(patient.getEmail().toLowerCase());
        Optional<Patient> currPatient = user();
        if (currPatient.isEmpty())
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).header("Must be signed in as patient!").body(Collections.emptyList());

        Optional<Patient> dbPatient = patientService.getByEmail(patient.getEmail());
        if (dbPatient.isEmpty())
            return ResponseEntity.badRequest().header("Invalid email!").body(Collections.emptyList());

        if (!currPatient.get().equals(dbPatient.get().getSupervisor())) {
            if (!currPatient.get().getId().equals(patient.getId()))
                return ResponseEntity.badRequest().header("Can not change other patient's details!").body(Collections.emptyList());
            if (currPatient.get().getSupervisor() != null)
                return ResponseEntity.badRequest().header("Supervised patient can not change his details!").body(Collections.emptyList());
        }

        List<String> errors = patientService.updatePatient(patient);
        if (errors != null)
            return ResponseEntity.badRequest().body(errors);
        else
            return ResponseEntity.ok().build();
    }

    @PostMapping("/register/continue/{confLink}")
    public ResponseEntity<List<String>> continueRegistration(@PathVariable("confLink") String confLink, @RequestBody Patient patient) {
        List<String> errors = new ArrayList<>();
        patient.setEmail(patient.getEmail().toLowerCase());

        Optional<Patient> databasePatient = patientService.getByEmail(patient.getEmail());
        if (databasePatient.isEmpty()) {
            errors.add("email");
            return ResponseEntity.badRequest().header("Invalid email").body(errors);
        }

        if (patient.getPassword().isEmpty() || !HealthwalletApplication.pswdEncoder().matches(patient.getPassword(), databasePatient.get().getPassword())){
            errors.add("password");
            return ResponseEntity.badRequest().header("Invalid password").body(errors);
        }

        if (confLink.isEmpty() || !confLink.equals(databasePatient.get().getConfirmationLink())) {
            errors.add("email");
            return ResponseEntity.badRequest().header("Invalid confirmation link").body(errors);
        }

        Patient dbpatient = databasePatient.get();

        patient.setConfirmationLink("");
        String oib = patient.getOib() != null ? patient.getOib() : dbpatient.getOib();
        String pin = generatePin(oib);
        patient.setPin(pin);

        dbpatient.setConfirmationLink("");
        dbpatient.setOib(patient.getOib());
        dbpatient.setAddress(patient.getAddress());
        dbpatient.setZipCode(patient.getZipCode());
        dbpatient.setHealthCardNumber(patient.getHealthCardNumber());
        dbpatient.setBirthDate(patient.getBirthDate());
        dbpatient.setSex(patient.getSex());
        dbpatient.setConfirmationLink(patient.getConfirmationLink());
        dbpatient.setPin(patient.getPin());

        errors.addAll(patientService.continueRegistration(dbpatient));
        if (!errors.isEmpty())
            return ResponseEntity.badRequest().body(errors);

        String subject = "Vaš novi PIN!";
        Runnable runnable = () -> {
            SendEmailTLS.sendEmail(subject, patient.getEmail(), dbpatient, null, null, "pin");
        };
        Thread thread = new Thread(runnable);
        thread.start();

        return ResponseEntity.ok().build();
    }


    /**
     * Get all the medical records that are linked with the current logged in user (patient)
     *
     * @return List - list of all medical records associated with the current user
     */
    @GetMapping("/medrecords")
    @Secured("ROLE_PATIENT")
    public List<MedicalRecord> listMedicalRecords(@RequestParam(value = "email", required = false) String patientEmail) {
        Optional<Patient> user = getValidUser(patientEmail);
        if (user.isEmpty())
            return Collections.emptyList();
        return medicalRecordService.getAllForPatient(user.get());
    }

    /**
     * Create a medical record from JSON and send it to the database
     *
     * @return MedicalRecord - medical record object
     */
    @PostMapping("/medrecords/upload")
    @Secured("ROLE_PATIENT")
    public MedicalRecord addMedicalRecord(@RequestBody MultipartFile file, @RequestParam("date") Date date, @RequestParam("departmentName") String departmentName
            , @RequestParam("doctor") String doctor, @RequestParam("hospital") String hospital, @RequestParam("diagnosis") String diagnosis, @RequestParam(value = "patientEmail", required = false) String patientEmail) throws IOException {
        Optional<Patient> user = getValidUser(patientEmail);
        if (user.isEmpty())
            return null;

        if (patientEmail == null && user.get().getSupervisor() != null)
            throw new RequestDeniedException("Supervised patient can't upload old medical records!");

        if (file.isEmpty())
            throw new StorageException("File is empty!");

        String fileURL = s3Services.uploadFile(file);
        String[] fields = fileURL.split("/");

        MedicalRecord medRec = new MedicalRecord(fields[fields.length - 1], date, departmentName, hospital, doctor, user().get(), diagnosis);
        return medicalRecordService.createMedicalRecord(medRec, user.get());
    }


    @GetMapping("/medrecords/{file}/download")
    @Secured("ROLE_PATIENT")
    public URL downloadRecord(@PathVariable("file") String file, @RequestParam(value = "email", required = false) String patientEmail) throws IOException, InterruptedException {
        Optional<Patient> user = getValidUser(patientEmail);
        if (user.isEmpty())
            return null;
        for (MedicalRecord medRec : medicalRecordService.getAllForPatient(user.get().getId())) {
            if (medRec.getFile().equals(file)) {
                return s3Services.downloadFile(file);
            }
        }
        throw new RequestDeniedException("Ovaj nalaz ne pripada ovom pacijentu");
    }

    @DeleteMapping("/medrecords/{id}/delete")
    @Secured("ROLE_PATIENT")
    public String removeRecord(@PathVariable("id") int id, @RequestParam(value = "email", required = false) String patientEmail){
        Optional<Patient> user = getValidUser(patientEmail);
        if (user.isEmpty())
            throw new RequestDeniedException("User nije ulogiran!");
        medicalRecordService.deleteRecord(id);
        return "Success";
    }

    /**
     * Get all Doctor objects with basic info for the given logged in user (patient)
     *
     * @return List - list of all DoctorDTO objects that are associated with the current logged in user (patient)
     */
    @GetMapping("/doctors")
    @Secured("ROLE_PATIENT")
    public List<DoctorDTO> listDoctors(@RequestParam(value = "email", required = false) String patientEmail) {
        Optional<Patient> user = getValidUser(patientEmail);
        if (user.isEmpty())
            return Collections.emptyList();

        return patientService.getDoctors(user.get().getId()).stream().map(x -> {
            String email = x.getEmail();
            String firstName = x.getFirstName();
            String lastName = x.getLastName();
            String specialty = x.getSpeciality();
            String hospital = doctorService.findBindedHospital(x.getId()).getName();
            return new DoctorDTO(email, firstName, lastName, specialty, hospital) ;
        }).collect(Collectors.toList());
    }

    @PostMapping("/doctors/remove")
    @Secured("ROLE_PATIENT")
    public boolean removeDoctor(@RequestParam("email") String doctorEmail, @RequestParam(value = "patientEmail", required = false) String patientEmail) {
        Optional<Patient> user = getValidUser(patientEmail);
        if (user.isEmpty())
            return false;
        Optional<Doctor> doctor = doctorService.getByEmail(doctorEmail);
        if (doctor.isEmpty())
            throw new RequestDeniedException("Doctor with email " + doctorEmail + " does not exist!");
        return doctorService.removePatient(doctor.get().getId(), user.get().getId());
    }

    /**
     * Check for the current user login info and return the Patient object if there exists a logged in patient
     *
     * @return Optional - Optional object that may or may not contain a Patient object
     */
    public Optional<Patient> user() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails)
            username = ((UserDetails) principal).getUsername();
        else
            username = principal.toString();
        return patientService.getByEmail(username);
    }

    /**
     * Generate a 4 digit PIN number that does not exist for some patient in the database
     *
     * @return String - string representation of a 4 digit PIN number
     */
    public String generatePin(String oib) {
        if (oib == null)
            return null;
        boolean bool = true;
        String pin = null;
        while (bool) {
            Random rand = new Random();
            pin = String.format("%04d", rand.nextInt(10000));
            bool = pinExists(pin, oib);
        }
        return pin;
    }

    /**
     * Method that generates a confirmation link for a patient and also checks if the generated link is already in use
     *
     * @return String - String representation of a generated confirmation link
     */
    public String generateConfirmLink() {
        boolean bool = true;
        int i;
        String confirmationLink = "";
        while (bool) {
            confirmationLink = UUID.randomUUID().toString().replace("-", "");
            bool = confirmationLinkExists(confirmationLink);
        }
        return confirmationLink;
    }

    /**
     * Method that checks if a PIN number is linked to some patient in the database
     *
     * @param pin - String that represents a 4 digit PIN number
     * @return boolean - true/false depending on if a PIN number exists for a patient in the database
     */
    public boolean pinExists(String pin, String oib) {
        List<Patient> patients = patientService.listAll().stream().filter(x -> oib.equals(x.getOib())).collect(Collectors.toList());
        for (Patient p : patients) {
            if (pin.equals(p.getPin()))
                return true;
        }
        return false;
    }

    /**
     * Method that checks if the generated confirmation link for registration exists already
     *
     * @param confLink - String representation a confirmation link which the patient needs
     * @return boolean - true/false depending on if the confirmation link exists
     */
    public boolean confirmationLinkExists(String confLink) {
        if (patientService.countByConfirmationLink(confLink) > 0) {
            return true;
        }
        return false;
    }

    private Optional<Patient> getValidUser(String patientEmail) {
        Optional<Patient> user = user();
        if (user.isEmpty())
            return Optional.empty();
        if (patientEmail != null) {
            Optional<Patient> patient = patientService.getByEmail(patientEmail);
            if (patient.isEmpty())
                throw new RequestDeniedException("Invalid patient email");
            user = patient;
        }
        return user;
    }

    /**
     * Method that binds a new patient for supervision under an existing patient
     *
     * @param emailSupervisor - String that represents the email of a supervisor patient
     * @return Patient - Patient object under which the new patient that is supervised was added
     */
    @PostMapping("/add-supervisor")
    @Secured("ROLE_PATIENT")
    public Patient addSupervisor(@RequestParam("email") String emailSupervisor) {
        emailSupervisor = emailSupervisor.toLowerCase();
        Optional<Patient> currentUser = user();
        if (currentUser.isEmpty()) {
            throw new RequestDeniedException("Must be signed in as patient!");
        }
        Patient patient = currentUser.get();
        if (patient.getSupervisor() != null)
            throw new RequestDeniedException("Already supervised patient can not add another supervisor!");
        if (patient.getEmail().equals(emailSupervisor))
            throw new RequestDeniedException("A patient can not be a supervisor to himself.");
        Optional<Patient> supervisor = patientService.getByEmail(emailSupervisor);
        if (supervisor.isEmpty())
            throw new RequestDeniedException("User with email " + emailSupervisor + " does not exist!");
        if (supervisor.get().getSupervisor() != null)
            throw new RequestDeniedException("Can not be supervised by an already supervised account!");
        return patientService.addSupervisor(patient, supervisor.get());
    }

    @PostMapping("/remove-supervised")
    @Secured("ROLE_PATIENT")
    public Patient removeSupervisor(@RequestParam("email") String emailSupervised) {
        emailSupervised = emailSupervised.toLowerCase();
        Optional<Patient> currentUser = user();
        if (currentUser.isEmpty()) {
            return null;
        }
        Patient patient = currentUser.get();
        Optional<Patient> supervisedPatient = patientService.getByEmail(emailSupervised);
        if (supervisedPatient.isEmpty()) {
            throw new RequestDeniedException("Patient with that e-mail does not exist.");
        }
        Patient supervised = supervisedPatient.get();
        if (supervised.getEmail().equals(emailSupervised) && supervised.getSupervisor() != null && supervised.getSupervisor().equals(patient)) {
            supervised.setSupervisor(null);
            patientService.store(supervised);
            return patient;
        }
        throw new RequestDeniedException("You can not remove a patient that you don't supervise!");
    }

    @GetMapping("/get-supervised")
    @Secured("ROLE_PATIENT")
    public List<Patient> getSupervisedPatients(@RequestParam(value = "id", required = false) Integer patientId) {
        Optional<Patient> currentUser = user();
        if (currentUser.isEmpty()) {
            return Collections.emptyList();
        }

        List<Patient> supervised = patientService.getAllSupervised(currentUser.get());
        if (patientId != null)
            supervised = supervised.stream().filter(x -> patientId.equals(x.getId())).collect(Collectors.toList());
        return supervised;
    }

    /**
     * Method that deletes the patient from the database that is identified by the id parameter
     *
     * @param patientId - id of the patient that is to be deleted
     * @return Patient - Patient object that was deleted
     */
    @DeleteMapping("/remove/{id}")
    @Secured("ROLE_ADMIN")
    public Patient deletePatient(@PathVariable("id") int patientId) {
        return patientService.deletePatient(patientId);
    }

    /**
     * Method that deletes the current logged in patient, but first it unbinds the patients from all binded doctors
     *
     * @return Patient - Patient object that was deleted from the database
     */
    @DeleteMapping("/remove-self")
    @Secured("ROLE_PATIENT")
    public Patient removeSelf() {
        Optional<Patient> currentUser = user();
        if (currentUser.isEmpty()) {
            return null;
        }
        Patient patient = currentUser.get();
        if (patient.getSupervisor() != null) {
            throw new RequestDeniedException("Supervised patient can not delete his account");
        }
        int userId = patient.getId();
        return patientService.deletePatient(userId);
    }

    /*
    @GetMapping("/filer")
    @Secured("ROLE_PATIENT")
    public Set<MedicalRecord> filterForPat(@RequestParam("dateFrom") Date dateFrom, @RequestParam("dateTo") Date dateTo, @RequestParam("hospital") String hospital, @RequestParam("department") String department, @RequestParam("doctor") String doctor, @RequestParam("diagnosis") String diagnosis) {
        Optional<Patient> user = user();
        if (user.isEmpty())
            throw new RequestDeniedException("Must be signed in as patient!");

        return patientService.filterMedRecords(user.get(), dateFrom, dateTo, hospital, department, doctor, diagnosis);
    }
    */

    /**
     * Get all medical records that match date filter and searched words
     *
     * @param string - searched string
     * @param dateFrom - date from which medical records are eligible to be searched
     * @param dateTo - date to which medical records are eligible to be searched
     * @return Set - Set of all current patient's medical records which match given parameters
     */
    @GetMapping("/search")
    @Secured("ROLE_PATIENT")
    public List<MedicalRecord> patientSearch(@RequestParam("search") String string, @RequestParam(value = "dateFrom", required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<Date> dateFrom, @RequestParam(value = "dateTo", required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<Date> dateTo, @RequestParam(value = "patientEmail", required = false) String patientEmail) {
        Optional<Patient> user = getValidUser(patientEmail);
        if (user.isEmpty())
            return Collections.emptyList();

        //Array of searched words that contain 2 or more chars
        String[] searchedWords = Arrays.stream(string.toLowerCase().split("( )+")).filter(w -> w.length() >= 2).toArray(String[]::new);

        Date startDate = dateFrom.isPresent() ? dateFrom.get() : null;
        Date endDate = dateTo.isPresent() ? dateTo.get() : null;
        return patientService.searchAndFilterRecords(user.get(), searchedWords, startDate, endDate);
    }

    @DeleteMapping("/delete-supervised")
    @Secured("ROLE_PATIENT")
    public Patient deleteSupervisedAccount(@RequestParam("email") String emailSupervised) {
        emailSupervised = emailSupervised.toLowerCase();
        Optional<Patient> currentUser = user();
        if (currentUser.isEmpty()) {
            return null;
        }
        Patient patient = currentUser.get();
        Optional<Patient> supervisedPatient = patientService.getByEmail(emailSupervised);
        if (supervisedPatient.isEmpty()) {
            throw new RequestDeniedException("Patient with that e-mail does not exist.");
        }
        Patient supervised = supervisedPatient.get();
        if (supervised.getEmail().equals(emailSupervised) && supervised.getSupervisor() != null && supervised.getSupervisor().equals(patient)) {
            int userId = supervised.getId();
            return patientService.deletePatient(userId);
        }

        throw new RequestDeniedException("You can not delete an account that you don't supervise!");

    }

    @GetMapping("/statistics")
    @Secured("ROLE_PATIENT")
    public ResponseEntity<StatsDTO> getStatistics(){
        Optional<Patient> user = user();
        Patient patient = user.get();
        if (user.isEmpty())
            return ResponseEntity.badRequest().header("User is empty").body(new StatsDTO());

        StatsDTO statsDTO = new StatsDTO();
        statsDTO.setNumberOfDoctors(patientService.numberOfDoctors(patient));
        statsDTO.setNumberOfRecords(medicalRecordService.numberOfRecords(patient));
        statsDTO.setRecentRecords(medicalRecordService.getLastFiveMedRecords(patient));
        statsDTO.setRecentDoctors(new ArrayList<Doctor>(patientService.getLastFiveDoctors(patient)));
        statsDTO.setNumberOfPatients(patientService.countSupervisedPatients(patient));
        return ResponseEntity.ok(statsDTO);

    }

}
