package hr.fer.suljekibuljeki.healthwallet.service;

import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface HospitalService {
    List<Hospital> listAll();
    Optional<Hospital> findById(int id);
    Hospital fetch(int id);
    Hospital createHospital(Hospital hospital);
    Doctor createDoctor(Doctor doctor, Hospital hospital);
    Doctor createDoctor(Doctor doctor, int hospitalId);
    Optional<Hospital> getByEmail(String email);
    List<Doctor> getAllDoctors(int hospitalId);
    List<String> updateHospital(Hospital hospital);
    Doctor unbindDoctor(int hospitalId, int doctorId);
    void store(Hospital hospital1);
    Hospital deleteHospital(int hospitalId);
    Doctor deleteDoctor(int hospitalId, int doctorId);
    Doctor deleteDoctor(Hospital hospital, Doctor doctor);
}
