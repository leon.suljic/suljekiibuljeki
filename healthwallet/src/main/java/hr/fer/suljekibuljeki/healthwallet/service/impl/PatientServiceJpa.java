package hr.fer.suljekibuljeki.healthwallet.service.impl;

import hr.fer.suljekibuljeki.healthwallet.dao.DoctorRepository;
import hr.fer.suljekibuljeki.healthwallet.dao.HospitalRepository;
import hr.fer.suljekibuljeki.healthwallet.dao.PatientRepository;
import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.MedicalRecord;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;
import hr.fer.suljekibuljeki.healthwallet.email.SendEmailTLS;
import hr.fer.suljekibuljeki.healthwallet.service.DoctorService;
import hr.fer.suljekibuljeki.healthwallet.service.MedicalRecordService;
import hr.fer.suljekibuljeki.healthwallet.service.PatientService;
import hr.fer.suljekibuljeki.healthwallet.service.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PatientServiceJpa implements PatientService {

    @Autowired
    private PatientRepository patientRepo;

    @Autowired
    private HospitalRepository hospitalRepo;

    @Autowired
    private DoctorRepository doctorRepo;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private MedicalRecordService medicalRecordService;

    @Override
    public List<Patient> listAll() {
        return patientRepo.findAll();
    }

    @Override
    public Patient fetch(int id) {
        return findById(id).orElseThrow();
    }

    @Override
    public Optional<Patient> findById(int id) {
        return patientRepo.findById(id);
    }

    @Override
    public Optional<Patient> findByPin(String pin) {
        return listAll().stream().filter(x -> x.getPin().equals(pin)).findFirst();
    }

    @Override
    public Optional<Patient> findByOib(String oib) {
        return listAll().stream().filter(x -> x.getOib().equals(oib)).findFirst();
    }

    @Override
    public Optional<Patient> getByEmail(String email) {
        return listAll().stream().filter(x -> x.getEmail().equals(email)).findFirst();
    }

    @Override
    public int countByPin(String pin) {
        return patientRepo.countByPin(pin);
    }

    @Override
    public int countByConfirmationLink(String confirmationLink) {
        return patientRepo.countByConfirmationLink(confirmationLink);
    }

    @Override
    public List<String> createPatient(Patient patient) {
        List<String> invalidFields = new LinkedList<>();

        Assert.notNull(patient, "Patient object must be given");
        Assert.isNull(patient.getId(), "Patient ID must be null, not " + patient.getId());

        patient.setEmail(patient.getEmail().toLowerCase());
        if (patientRepo.countByEmail(patient.getEmail()) > 0 || hospitalRepo.countByEmail(patient.getEmail()) > 0 || doctorRepo.countByEmail(patient.getEmail()) > 0) {
            invalidFields.add("email");
        }

        if (patient.getFirstName().isEmpty() || !patient.getFirstName().matches("^[a-z\\sšđžćčŠĐŽĆČA-Z-]+$"))
            invalidFields.add("firstName");
        if (patient.getLastName().isEmpty() || !patient.getLastName().matches("^[a-z\\sšđžćčŠĐŽĆČA-Z-]+$"))
            invalidFields.add("lastName");
        if (patient.getEmail().isEmpty() || !patient.getEmail().matches("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,6}$"))
            invalidFields.add("email");

        if (invalidFields.size() == 0) {
            patientRepo.save(patient); // All fields are ok, save patient
        } else {
            return invalidFields; // Return early
        }

        //Creating a new thread for sending the email
        String subject = "HealthWallet Vam želi dobrodošlicu!";
        Runnable runnable = () -> {
            SendEmailTLS.sendEmail(subject, patient.getEmail(), patient, null, null, "registerPatient");
        };
        Thread thread = new Thread(runnable);
        thread.start();
        return Collections.emptyList();
    }

    @Override
    public List<String> continueRegistration(Patient patient) {
        List<String> invalidFields = new LinkedList<>();

        Assert.notNull(patient, "Patient object must be given");
        Assert.notNull(patient.getId(), "Patient ID can not be null!");

        if (patient.getOib().isEmpty() || !patient.getOib().matches("^[0-9]{11}$"))
            invalidFields.add("oib");
        if (patient.getAddress().isEmpty() || !patient.getAddress().matches("^[a-zA-Z0-9šđžćčŠĐŽĆČ.\\s]+$"))
            invalidFields.add("address");
        if (patient.getHealthCardNumber().isEmpty() || !patient.getHealthCardNumber().matches("^[0-9]{9}$"))
            invalidFields.add("healthCardNumber");
        if (patient.getZipCode().isEmpty() || !patient.getZipCode().matches("^[0-9]{5}$"))
            invalidFields.add("zipCode");
        if (patient.getBirthDate() == null)
            invalidFields.add("birthDate");
        if (patient.getSex() == null)
            invalidFields.add("sex");

        if (invalidFields.size() == 0) patientRepo.save(patient);

        return invalidFields;
    }

    @Override
    public Patient findByOIBAndPIN(String patientOIB, String patientPIN) {
        if (patientRepo.countByPin(patientPIN) == 0) {
            throw new RequestDeniedException("There is no patient with this PIN!"); // TODO: Implement new type of exception for this
        }

        if (patientRepo.countByOib(patientOIB) == 0) {
            throw new RequestDeniedException("There is no patient with this OIB!"); // This also needs implementing
        }

        Patient patient = patientRepo.findByOibAndPin(patientOIB, patientPIN);

        return patient;
    }

    @Override
    public List<String> updatePatient(Patient patient) {
        List<String> invalidFields = new LinkedList<>();

        Assert.notNull(patient, "Patient object must be given!");
        Assert.notNull(patient.getId(), "Patient ID can not be null!");

        // Check invalid fields
        if (!patient.getFirstName().matches("^[a-z\\sA-ZšđžćčŠĐŽĆČ]+$"))
            invalidFields.add("firstName");
        if (!patient.getLastName().matches("^[a-z\\sA-ZšđžćčŠĐŽĆČ]+$"))
            invalidFields.add("lastName");
        if (!patient.getEmail().matches("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,6}$"))
            invalidFields.add("email");
        if (!patient.getOib().matches("^[0-9]{11}$"))
            invalidFields.add("oib");
        if (!patient.getAddress().matches("^[a-zA-Z0-9šđžćčŠĐŽĆČ.\\s]+$"))
            invalidFields.add("address");
        if (!patient.getHealthCardNumber().matches("^[0-9]{9}$"))
            invalidFields.add("healthCardNumber");
        if (!patient.getZipCode().matches("^[0-9]{5}$"))
            invalidFields.add("zipCode");
        if (patient.getBirthDate() == null)
            invalidFields.add("birthDate");
        if (patient.getSex() == null)
            invalidFields.add("sex");

        if (invalidFields.size() == 0) patientRepo.save(patient);

        return invalidFields.size() == 0 ? null : invalidFields;
    }

    @Override
    public List<Doctor> getDoctors(int id) {
        Patient patient = fetch(id);
        return doctorService.listAll().stream().filter(x -> x.getPatients().contains(patient)).collect(Collectors.toList());
    }

    @Override
    public Patient deletePatient(int id) {
        Patient patient = fetch(id);
        deleteForAllDoctors(patient);
        deleteFromSupervised(patient);
        deleteMedicalRecordsForPatient(patient);
        patientRepo.delete(patient);
        return patient;
    }

    private Patient deleteForAllDoctors(Patient patient) {
        List<Doctor> doctors = getDoctors(patient.getId());
        for (Doctor doctor : doctors) {
            doctorService.removePatient(doctor.getId(), patient.getId());
        }
        return patient;
    }

    private void deleteFromSupervised(Patient patient) {
        findAllSupervised(patient).stream().forEach(x -> {
            x.setSupervisor(null);
            patientRepo.save(x);
        });
    }

    private void deleteMedicalRecordsForPatient(Patient patient) {
        medicalRecordService.getAllForPatient(patient).forEach(x -> medicalRecordService.deleteRecord(x.getRecordID()));
    }

    @Override
    public Patient addSupervisor(Patient patient, Patient supervisor) { ;
        patient.setSupervisor(supervisor);
        return patientRepo.save(patient);
    }

    public List<Patient> findAllSupervised(Patient patient) {
        List<Patient> supervised = listAll().stream().filter(x -> patient.equals(x.getSupervisor())).collect(Collectors.toList());
        return supervised;
    }

    @Override
    public List<MedicalRecord> searchAndFilterRecords(Patient patient, String[] searchedWords, Date dateFrom, Date dateTo) {
        List<MedicalRecord> allMedRecords = medicalRecordService.getAllForPatient(patient);
        TreeSet<MedicalRecord> foundMedRecords = new TreeSet<>(Comparator.comparing(MedicalRecord::getRecordID));

        for (MedicalRecord med : allMedRecords) {
            //filter
            Date date = med.getDate();
            if ((dateFrom != null) && !(date.compareTo(dateFrom) >= 0))
                continue;
            else if ((dateTo != null) && !(date.compareTo(dateTo) <= 0))
                continue;
            //search
            int wordsFound = 0;
            for (String word : searchedWords) {
                if (med.getOldRecordDoctor() != null) {
                    if ((med.getOldRecordDoctor().toLowerCase().contains(word)) || (med.getOldRecordHospital().toLowerCase().contains(word))
                            || (med.getDepartmentName().toLowerCase().contains(word)) || (med.getFile().toLowerCase().contains(word)) || (med.getDiagnosis().toLowerCase().contains(word))) {
                        wordsFound++;
                    }
                } else {
                    if ((med.getFile().toLowerCase().contains(word)) || (med.getDepartmentName().toLowerCase().contains(word))
                            || (med.getHospital().getName().toLowerCase().contains(word)) || (med.getDoctor().getFirstName().toLowerCase().contains(word))
                            || (med.getDoctor().getLastName().toLowerCase().contains(word)) || (med.getDiagnosis().toLowerCase().contains(word))) {
                        wordsFound++;
                    }
                }
            }
            if (wordsFound == searchedWords.length)
                foundMedRecords.add(med);
        }
        return medicalRecordService.sortMedRecords(foundMedRecords.stream().collect(Collectors.toList()));
    }

    @Override
    public Set<MedicalRecord> filterMedRecords(Patient patient, Date dateFrom, Date dateTo, String hospital, String department, String doctor, String diagnosis) {
        List<MedicalRecord> allForPatient = medicalRecordService.getAllForPatient(patient);
        TreeSet<MedicalRecord> foundMedRecords = new TreeSet<>(Comparator.comparing(MedicalRecord::getRecordID));
        for (MedicalRecord record : allForPatient) {
            Date date = record.getDate();
            String docName = record.getDoctor().getFirstName() + " " + record.getDoctor().getLastName();
            if ((dateFrom != null) && !(date.compareTo(dateFrom) >= 0))
                continue;
            else if ((dateTo != null) && !(date.compareTo(dateTo) <= 0))
                continue;
            else if (!record.getDepartmentName().isEmpty() && !record.getDepartmentName().equals(department))
                continue;
            else if (!record.getDiagnosis().isEmpty() && !record.getDiagnosis().equals(diagnosis))
                continue;
            else if (!docName.isEmpty() && docName.equals(doctor))
                continue;
            else
                foundMedRecords.add(record);
        }
        return foundMedRecords;
    }

    @Override
    public Patient findInPatients(Set<Patient> allPatients, int patientId) {
        Patient patient = null;
        if (allPatients.contains(fetch(patientId))) {
            patient = fetch(patientId);
        }
        return patient;
    }

    @Override
    public void store(Patient patient) {
        patientRepo.save(patient);
    }

    @Override
    public int numberOfDoctors(Patient patient){
        return getDoctors(patient.getId()).size();
    }

    @Override
    public int countSupervisedPatients(Patient patient) {
        return patientRepo.countBySupervisor(patient);
    }

    @Override
    public List<Patient> getAllSupervised(Patient patient) {
        return patientRepo.getBySupervisor(patient);
    }

    @Override
    public Set<Doctor> getLastFiveDoctors(Patient patient){
        List<MedicalRecord> allMedicalRecords = medicalRecordService.getAllForPatient(patient);
        allMedicalRecords.sort(Comparator.comparing(MedicalRecord::getDate).reversed());
        Set<Doctor> last5doctors = new LinkedHashSet<>(5);
        for (MedicalRecord record : allMedicalRecords) {
            if (last5doctors.size() < 6) {
                if (record.getDoctor() != null)
                    last5doctors.add(record.getDoctor());
            }
        }
        return last5doctors;

    }


}
