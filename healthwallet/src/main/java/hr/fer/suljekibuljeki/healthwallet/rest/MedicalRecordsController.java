package hr.fer.suljekibuljeki.healthwallet.rest;

import hr.fer.suljekibuljeki.healthwallet.service.*;

import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;

import hr.fer.suljekibuljeki.healthwallet.domain.MedicalRecord;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/medRed")
public class MedicalRecordsController {

    @Autowired
    private PatientService patientService;

    @Autowired
    private MedicalRecordService medicalRecordService;

    @Autowired
    private PatientController patientController;

    @GetMapping("/")
    @Secured("ROLE_ADMIN")
    public List<MedicalRecord> listMedRecords() {
        return medicalRecordService.listAll();
    }


}
