package hr.fer.suljekibuljeki.healthwallet.rest;

import hr.fer.suljekibuljeki.healthwallet.HealthwalletApplication;
import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;
import hr.fer.suljekibuljeki.healthwallet.domain.MedicalRecord;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;
import hr.fer.suljekibuljeki.healthwallet.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * Controller class that communicates with the front end through REST API.
 * Accepts requests and sends responses.
 * <p>
 * All requests are done through the /api/doctors mapping.
 */
@RestController
@RequestMapping("api/doctors")
public class DoctorController {

    /**
     * DoctorService type representing the business logic of doctors in the application
     */
    @Autowired
    private DoctorService doctorService;

    /**
     * PatientService type representing the business logic of patients in the application
     */
    @Autowired
    private PatientService patientService;

    @Autowired
    private HospitalService hospitalService;

    /**
     * MedicalRecordService type representing the business logic of medical records in the application
     */
    @Autowired
    private MedicalRecordService medicalRecordService;

    @Autowired
    private S3Services s3Services;

    /**
     * Get all doctors from the database
     *
     * @return List - list of all doctors
     */
    @GetMapping("")
    @Secured("ROLE_ADMIN")
    public List<Doctor> listDoctors() {
        return doctorService.listAll();
    }

    @GetMapping("/patients")
    @Secured("ROLE_DOCTOR")
    public List<Patient> listPatients() {
        return doctorService.listAllPatients(user().get().getId());
    }

    /**
     * Get all medical records that are attributed to a certain patient
     *
     * @param patientId - ID property of a patient whose medical records we need to get
     * @return List - list of all medical records that pertain to a certain patient
     */
    @GetMapping("/{id}/records")
    @Secured("ROLE_DOCTOR")
    public List<MedicalRecord> getMedicalRecords(@PathVariable("id") int patientId) {
        return new ArrayList<>(medicalRecordService.getAllForPatient(patientId));
    }

    @PostMapping("/{id}/records/upload")
    @Secured("ROLE_DOCTOR")
    public MedicalRecord uploadMedicalRecord(@PathVariable("id") int patientId, @RequestParam("file") MultipartFile file,
                                             @RequestParam("date") Date date, @RequestParam("departmentName") String departmentName, @RequestParam("diagnosis") String diagnosis) {
        Optional<Doctor> user = user();


        if (user.isEmpty())
            return null;

        Set<Patient> allPatients = user.get().getPatients();
        Patient patient = patientService.findInPatients(allPatients, patientId);
        if (patient == null)
            throw new RequestDeniedException("Ne postoji pacijent s tim id!");

        //patient = patientService.findById(patientId);

        String fileURL = s3Services.uploadFile(file);
        String[] fields = fileURL.split("/");

        Hospital hospital = doctorService.findBindedHospital(user.get().getId());
        MedicalRecord medRec = new MedicalRecord(fields[fields.length - 1], date, departmentName, hospital, user.get(), patient, diagnosis);
        return medicalRecordService.createMedicalRecord(medRec);
    }

    @GetMapping("/{id}/records/{file}/download")
    @Secured("ROLE_DOCTOR")
    public URL downloadRecord(@PathVariable("file") String file, @PathVariable("id") int id) throws IOException, InterruptedException {
        Optional<Doctor> user = user();

        if (user.isEmpty())
            return null;

        Doctor doctor = user.get();
        Set<Patient> allPatients = user.get().getPatients();
        Patient patient = patientService.findInPatients(allPatients, id);

        if (patient == null)
            throw new RequestDeniedException("Poslani id ne odgovara niti jednom pacijentu doktora");

        for (MedicalRecord medrec : medicalRecordService.getAllForPatient(id)) {
            if (medrec.getFile().equals(file)) {
                return s3Services.downloadFile(file);
            }
        }
        throw new RequestDeniedException("Nalaz ne pripada odabranom pacijentu");
    }

    @DeleteMapping("/{patientId}/records/{recordId}/delete")
    @Secured("ROLE_DOCTOR")
    public String deleteRecord(@PathVariable("patientId") int patientId, @PathVariable("recordId") int recordId) {
        Optional<Doctor> user = user();

        if (user.isEmpty())
            return null;

        Doctor doctor = user.get();
        Set<Patient> allPatients = user.get().getPatients();
        Patient patient = patientService.findInPatients(allPatients, patientId);
        if (patient == null)
            throw new RequestDeniedException("Poslani patientId ne odgovara niti jednom pacijentu doktora");

        medicalRecordService.deleteRecord(recordId);

        return "Obrisano!";
    }


    /**
     * Get a container (Optional) that can contain the Doctor object of a logged in user
     *
     * @return Optional - Optional data type that may or may not contain the Doctor that is logged in (depends if someone is logged in)
     */
    public Optional<Doctor> user() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails)
            username = ((UserDetails) principal).getUsername();
        else
            username = principal.toString();
        return doctorService.getByEmail(username);
    }

    @PostMapping("/register/{confLink}")
    public ResponseEntity<List<String>> registerPassword(@PathVariable("confLink") String confirmationLink, @RequestBody Doctor doctor) {
        doctor.setEmail(doctor.getEmail().toLowerCase());
        List<String> errors = new LinkedList<>();
        Optional<Doctor> databaseDoctor = doctorService.getByEmail(doctor.getEmail());
        if (databaseDoctor.isEmpty()){
            errors.add("email");
            return ResponseEntity.badRequest().header("Invalid email").body(errors);
        }

        Doctor dbdoctor = databaseDoctor.get();

        if (confirmationLink.equals("") || dbdoctor.getConfirmationLink() == null || !dbdoctor.getConfirmationLink().equals(confirmationLink))
            throw new RequestDeniedException("Confirmation link invalid");

        if (doctor.getPassword().isEmpty() || !doctor.getPassword().matches("^[a-zA-Z0-9]{8,}$")) {
            errors.add("password");
            return ResponseEntity.badRequest().header("Invalid password").body(errors);
        }
        String encPass = HealthwalletApplication.pswdEncoder().encode(doctor.getPassword());
        dbdoctor.setPassword(encPass);
        dbdoctor.setConfirmationLink("");

        doctorService.registerPassword(dbdoctor);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/add-patient/")
    @Secured("ROLE_DOCTOR")
    public Patient addPatient(@RequestParam("patientOIB") String patientOIB, @RequestParam("patientPIN") String patientPIN) {
        Patient patient = patientService.findByOIBAndPIN(patientOIB, patientPIN);
        doctorService.addPatient(patient, user().get().getEmail());
        return patient;
    }

    @PostMapping("/unbind-patient/{id}")
    @Secured("ROLE_DOCTOR")
    public Patient unbindPatient(@PathVariable("id") int patientId) {
        int doctorId = user().get().getId();
        doctorService.removePatient(doctorId, patientId);
        return patientService.fetch(patientId);
    }

    @DeleteMapping("/remove-self")
    @Secured("ROLE_DOCTOR")
    public Doctor removeSelf() {
        Doctor doctor = user().get();
        int doctorId = doctor.getId();
        doctorService.deleteDoctor(doctorId);
        return doctor;
    }

    @DeleteMapping("/remove/{id}")
    @Secured("ROLE_ADMIN")
    public Doctor removeDoctor(@PathVariable("id") Integer doctorId) {
        if (doctorId == null)
            throw new RequestDeniedException("Doctor ID must be provided");
        Doctor doctor = doctorService.fetch(doctorId);
        doctorService.deleteDoctor(doctorId);
        return doctor;
    }

    /**
     * Get all medical records of patient with patientId that match date filter and searched words
     *
     * @param patientId - ID of patient which medical records are being searched
     * @param string - searched string
     * @param dateFrom - date from which medical records are eligible to be searched
     * @param dateTo - date to which medical records are eligible to be searched
     * @return Set - Set of all current patient's medical records which match given parameters
     */
    @GetMapping("/search/{id}")
    @Secured("ROLE_DOCTOR")
    public List<MedicalRecord> doctorSearch(@PathVariable("id") int patientId, @RequestParam("search") String string, @RequestParam(value = "dateFrom", required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<Date> dateFrom, @RequestParam(value = "dateTo", required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<Date> dateTo) {
        Optional<Doctor> user = user();
        if (user.isEmpty())
            throw new RequestDeniedException("Must be signed in as doctor!");

        Optional<Patient> patient = patientService.findById(patientId);
        if (patient.isEmpty())
            throw new RequestDeniedException("Given ID does not match any patient!");

        Set<Patient> setOfPatients = user.get().getPatients();
        if (!setOfPatients.contains(patient.get()))
            throw new RequestDeniedException("Given ID does not match any current doctor's patient!");

        //Array of searched words that contain 2 or more chars
        String[] searchedWords = Arrays.stream(string.toLowerCase().split("( )+")).filter(w -> w.length() >= 2).toArray(String[]::new);

        Date startDate = dateFrom.isPresent() ? dateFrom.get() : null;
        Date endDate = dateTo.isPresent() ? dateTo.get() : null;
        return patientService.searchAndFilterRecords(patient.get(), searchedWords, startDate, endDate);
    }

    @GetMapping("/statistics")
    @Secured("ROLE_DOCTOR")
    public ResponseEntity<StatsDTO> getStatistics(){
        Optional<Doctor> user = user();
        Doctor doctor = user.get();
        if (user.isEmpty()) {
            return ResponseEntity.badRequest().header("User is empty").body(new StatsDTO());
        }
        StatsDTO statsDTO = new StatsDTO();
        statsDTO.setNumberOfPatients(doctorService.listAllPatients(doctor.getId()).size());
        statsDTO.setNumberOfRecords(medicalRecordService.numberOfRecords(doctor));
        statsDTO.setRecentRecords(medicalRecordService.getLastFiveMedRecords(doctor));
        statsDTO.setRecentPatients(new ArrayList<Patient>(doctorService.getLastFivePatients(doctor)));
        return ResponseEntity.ok(statsDTO);
    }

    /*
    @GetMapping("/filer/{id}")
    @Secured("ROLE_DOCTOR")
    public Set<MedicalRecord> filterForPat(@PathVariable("id") int patientId, @RequestParam("dateFrom") Date dateFrom, @RequestParam("dateTo") Date dateTo, @RequestParam("hospital") String hospital, @RequestParam("department") String department, @RequestParam("doctor") String doctor, @RequestParam("diagnosis") String diagnosis) {
        Optional<Doctor> user = user();
        if (user.isEmpty())
            throw new RequestDeniedException("Must be signed in as doctor!");

        Optional<Patient> optPat = patientService.findById(patientId);
        if (optPat.isEmpty())


        if (!user.get().getPatients().contains(optPat.get()))
            throw new RequestDeniedException("This doctor does not contain patient with id " + patientId + "!");
        
        return patientService.filterMedRecords(optPat.get(), dateFrom, dateTo, hospital, department, doctor, diagnosis);
    }
     */
}
