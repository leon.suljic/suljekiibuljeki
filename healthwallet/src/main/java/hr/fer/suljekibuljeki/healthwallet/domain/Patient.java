package hr.fer.suljekibuljeki.healthwallet.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

@Entity
public class Patient extends Person {

    //================================================================================
    // Properties
    //================================================================================

    @Size(min = 9, max = 9)
    private String healthCardNumber;

    @ManyToOne
    private Patient supervisor;

    private String pin;


    //================================================================================
    // Accessors
    //================================================================================

    public String getHealthCardNumber() {
        return healthCardNumber;
    }

    public void setHealthCardNumber(String healthCardNumber) {
        this.healthCardNumber = healthCardNumber;
    }

    public Patient getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Patient supervisor) {
        this.supervisor = supervisor;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    @Override
    public boolean equals(Object o) {
        if(o == this) return true;
        if(!(o instanceof Patient)) return false;
        Patient p = (Patient) o;
        return p.getEmail().equals(this.getEmail());
    }
}
