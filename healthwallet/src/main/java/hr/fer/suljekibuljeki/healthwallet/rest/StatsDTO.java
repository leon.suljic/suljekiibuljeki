package hr.fer.suljekibuljeki.healthwallet.rest;

import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.MedicalRecord;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;

import java.util.List;

public class StatsDTO {

    private int numberOfRecords;
    private int numberOfDoctors;
    private int numberOfPatients;

    private List<MedicalRecord> recentRecords;
    private List<Doctor> recentDoctors;
    private List<Patient> recentPatients;

    private List<Doctor> mostActiveDoctors;

    public StatsDTO(){}

    public int getNumberOfRecords() {
        return numberOfRecords;
    }

    public void setNumberOfRecords(int numberOfRecords) {
        this.numberOfRecords = numberOfRecords;
    }

    public int getNumberOfDoctors() {
        return numberOfDoctors;
    }

    public void setNumberOfDoctors(int numberOfDoctors) {
        this.numberOfDoctors = numberOfDoctors;
    }

    public int getNumberOfPatients() {
        return numberOfPatients;
    }

    public void setNumberOfPatients(int numberOfPatients) {
        this.numberOfPatients = numberOfPatients;
    }

    public List<MedicalRecord> getRecentRecords() {
        return recentRecords;
    }

    public void setRecentRecords(List<MedicalRecord> recentRecords) {
        this.recentRecords = recentRecords;
    }

    public List<Doctor> getRecentDoctors() {
        return recentDoctors;
    }

    public void setRecentDoctors(List<Doctor> recentDoctors) {
        this.recentDoctors = recentDoctors;
    }

    public List<Patient> getRecentPatients() {
        return recentPatients;
    }

    public void setRecentPatients(List<Patient> recentPatients) {
        this.recentPatients = recentPatients;
    }

    public List<Doctor> getMostActiveDoctors() {
        return mostActiveDoctors;
    }

    public void setMostActiveDoctors(List<Doctor> mostActiveDoctors) {
        this.mostActiveDoctors = mostActiveDoctors;
    }
}
