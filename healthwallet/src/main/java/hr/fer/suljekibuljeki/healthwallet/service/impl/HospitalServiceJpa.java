package hr.fer.suljekibuljeki.healthwallet.service.impl;

import hr.fer.suljekibuljeki.healthwallet.dao.DoctorRepository;
import hr.fer.suljekibuljeki.healthwallet.dao.HospitalRepository;
import hr.fer.suljekibuljeki.healthwallet.dao.MedicalRecordRepository;
import hr.fer.suljekibuljeki.healthwallet.dao.PatientRepository;
import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;
import hr.fer.suljekibuljeki.healthwallet.domain.MedicalRecord;
import hr.fer.suljekibuljeki.healthwallet.email.SendEmailTLS;
import hr.fer.suljekibuljeki.healthwallet.service.DoctorService;
import hr.fer.suljekibuljeki.healthwallet.service.HospitalService;
import hr.fer.suljekibuljeki.healthwallet.service.MedicalRecordService;
import hr.fer.suljekibuljeki.healthwallet.service.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;

@Service
public class HospitalServiceJpa implements HospitalService {

    @Autowired
    private HospitalRepository hospitalRepo;

    @Autowired
    private DoctorRepository doctorRepo;

    @Autowired
    private PatientRepository patientRepo;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private MedicalRecordService medRecordService;

    @Autowired
    private MedicalRecordRepository medicalRecordRepository;

    @Override
    public List<Hospital> listAll() {
        return hospitalRepo.findAll();
    }

    @Override
    public Optional<Hospital> findById(int id) {
        return hospitalRepo.findById(id);
    }

    @Override
    public Hospital fetch(int id) {
        return findById(id).orElseThrow();
    }

    @Override
    public Hospital createHospital(Hospital hospital) {
        Assert.notNull(hospital, "Hospital object must be given!");
        Assert.isNull(hospital.getId(), "Hospital ID must be null and not " + hospital.getId());
        hospital.setEmail(hospital.getEmail().toLowerCase());
        if(hospitalRepo.countByEmail(hospital.getEmail()) > 0 || doctorRepo.countByEmail(hospital.getEmail()) > 0
            || patientRepo.countByEmail(hospital.getEmail()) > 0){
            throw new RequestDeniedException("Hospital with email " + hospital.getEmail() + " already exists!");
        }
        return hospitalRepo.save(hospital);
    }

    @Override
    public Doctor createDoctor(Doctor doctor, Hospital hospital) {
        Assert.notNull(hospital, "Hospital must not be null!");
        doctor = doctorService.createDoctor(doctor);
        hospital.getDoctors().add(doctor);
        hospitalRepo.save(hospital);

        //Creating a new thread for sending the email
        String subject = "HealthWallet Vam želi dobrodošlicu!";
        Doctor finalDoctor = doctor;
        Runnable runnable = () -> {
            SendEmailTLS.sendEmail(subject, finalDoctor.getEmail(), null, finalDoctor, hospital, "registerDoctor");
        };
        Thread thread = new Thread(runnable);
        thread.start();

        return doctor;
    }

    @Override
    public Doctor createDoctor(Doctor doctor, int hospitalId) {
        return createDoctor(doctor, fetch(hospitalId));
    }

    @Override
    public List<String> updateHospital(Hospital hospital){
        List<String> invalidFields = new LinkedList<>();

        Assert.notNull(hospital, "Hospital object must be given!");
        Assert.notNull(hospital.getId(), "Hospital ID can not be null!");

        if (!hospital.getName().matches("^[a-z\\sA-ZšđžćčŠĐŽĆČ]+$"))
            invalidFields.add("name");
        if (!hospital.getEmail().matches("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,6}$"))
            invalidFields.add("email");
        if (hospital.getOib().isEmpty() || !hospital.getOib().matches("^[0-9]{11}$"))
            invalidFields.add("oib");
        if (!hospital.getAddress().matches("^[a-zA-Z0-9šđžćčŠĐŽĆČ.\\s]+$"))
            invalidFields.add("address");
        if (!hospital.getZipCode().matches("^[0-9]{5}$"))
            invalidFields.add("zipCode");

        if (invalidFields.size() == 0) hospitalRepo.save(hospital);

        return invalidFields.size() == 0 ? null : invalidFields;
    }

    @Override
    public Optional<Hospital> getByEmail(String email) {
        return listAll().stream().filter(x -> x.getEmail().equals(email)).findFirst();
    }

    @Override
    public List<Doctor> getAllDoctors(int hospitalId) {
        Hospital hospital = fetch(hospitalId);
        List<Doctor> doctors = new LinkedList<>();
        doctors.addAll(hospital.getDoctors());
        return doctors;
    }

    @Override
    public Doctor unbindDoctor(int hospitalId, int doctorId) {
        Hospital hospital = fetch(hospitalId);
        Doctor doctor = doctorService.fetch(doctorId);
        if(hospital.getDoctors().contains(doctor)) {
            hospital.getDoctors().remove(doctor);
            hospitalRepo.save(hospital);
        } else {
            throw new IllegalArgumentException("The selected hospital doesn't contain the selected doctor!");
        }
        return doctor;
    }

    @Override
    public void store(Hospital hospital) {
        hospitalRepo.save(hospital);
    }

    @Override
    public Hospital deleteHospital(int hospitalId) {
        Hospital hospital = fetch(hospitalId);
        Set<Doctor> doctors = hospital.getDoctors();
        Set<Doctor> docCopy = Set.copyOf(doctors);

        // Unbind doctors from hospital
        for(Doctor doctor : doctors) {
            doctorService.unbindAllPatients(doctor.getId());
        }

        for(Doctor doctor : docCopy) {

            doctorService.deleteDoctor(doctor.getId());
        }
        doctors.clear();

        List<MedicalRecord> medicalRecords = medRecordService.getAllForHospital(hospitalId);
        for (MedicalRecord medRecord : medicalRecords){
            medRecord.setOldRecordHospital(medRecord.getHospital().getName());
            medRecord.setHospital(null);
            medicalRecordRepository.save(medRecord);
        }

        hospitalRepo.save(hospital);
        // Unbind patients from doctors
        // Delete doctors

        hospitalRepo.delete(hospital);
        return hospital;
    }

    @Override
    public Doctor deleteDoctor(int hospitalId, int doctorId) {
        Hospital hospital = fetch(hospitalId);
        Doctor doctor = doctorService.fetch(doctorId);

        Set<Doctor> doctors = hospital.getDoctors();
        if(!doctors.contains(doctor)) {
            throw new RequestDeniedException("Doctor that wants to be deleted doesn't exist in this hospital!");
        }

        doctorService.deleteDoctor(doctorId);
        return doctor;
    }

    @Override
    public Doctor deleteDoctor(Hospital hospital, Doctor doctor) {
        Set<Doctor> doctors = hospital.getDoctors();
        if(!doctors.contains(doctor)) {
            throw new RequestDeniedException("Doctor that wants to be deleted doesn't exist in this hospital!");
        }

        doctorService.deleteDoctorAlternate(doctor);
        return doctor;
    }
}
