package hr.fer.suljekibuljeki.healthwallet;

import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;
import hr.fer.suljekibuljeki.healthwallet.service.DoctorService;
import hr.fer.suljekibuljeki.healthwallet.service.HospitalService;
import hr.fer.suljekibuljeki.healthwallet.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;


public class DataInitializer {

    private PasswordEncoder passwordEncoder;

    public DataInitializer(PasswordEncoder encoder)
    {
        this.passwordEncoder = encoder;
        addHospitals();
    }

    @Autowired
    private PatientService patientService;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private HospitalService hospitalService;

    private void addHospitals()
    {
        Hospital hospital = new Hospital();
        hospital.setName("Sv. Duh");
        hospital.setEmail("admin@svduh.hr");
        hospital.setPassword(passwordEncoder.encode("svduh"));
        hospitalService.createHospital(hospital);

        hospital = new Hospital();
        hospital.setName("KBC Zagreb");
        hospital.setEmail("admin@kbczagreb.hr");
        hospital.setPassword(passwordEncoder.encode("kbczagreb"));
        hospitalService.createHospital(hospital);

    }
}
