package hr.fer.suljekibuljeki.healthwallet.rest;


import hr.fer.suljekibuljeki.healthwallet.HealthwalletApplication;
import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;
import hr.fer.suljekibuljeki.healthwallet.email.SendEmailTLS;
import hr.fer.suljekibuljeki.healthwallet.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@RestController
@RequestMapping("api/forgotten-password")

public class ForgottenPasswordController {
    @Autowired
    PatientService patientService;

    @Autowired
    DoctorService doctorService;

    @Autowired
    HospitalService hospitalService;


    @PostMapping("/send-code")
    public void sendEmailCode(@RequestParam("email") String email){
        String subject = "Zaboravljena lozinka";
        Optional<Patient> patient = patientService.getByEmail(email);
        if(patient.isPresent()) {
            Patient patient1 = patient.get();
            String code = generatePassCode();
            patient1.setForgottenPassCode(code);
            patientService.store(patient1);

            Runnable runnable = () -> {
                SendEmailTLS.sendEmail(subject, patient1.getEmail(), patient1, null, null, "forgottenPatient");
            };
            Thread thread = new Thread(runnable);
            thread.start();
            return;
        }

        Optional<Doctor> doctor = doctorService.getByEmail(email);
        if(doctor.isPresent()) {
            Doctor doctor1 = doctor.get();
            String code = generatePassCode();
            doctor1.setForgottenPassCode(code);
            doctorService.store(doctor1);

            Runnable runnable = () -> {
                SendEmailTLS.sendEmail(subject, doctor1.getEmail(), null, doctor1, null, "forgottenDoctor");
            };
            Thread thread = new Thread(runnable);
            thread.start();
            return;
        }

        Optional<Hospital> hospital = hospitalService.getByEmail(email);
        if(hospital.isPresent()) {
            Hospital hospital1 = hospital.get();
            String code = generatePassCode();
            hospital1.setForgottenPassCode(code);
            hospitalService.store(hospital1);

            Runnable runnable = () -> {
                SendEmailTLS.sendEmail(subject, hospital1.getEmail(), null, null, hospital1, "forgottenHospital");
            };
            Thread thread = new Thread(runnable);
            thread.start();
            return;
        }

        throw new LoginNameNotFoundException("User with email: " + email + " hasn't been found!");

    }

    @PostMapping("/change-password")
    public ResponseEntity<List<String>> changePassword(@RequestParam("oldPassword") String oldPassword, @RequestParam("newPassword1") String newPassword1, @RequestParam("newPassword2") String newPassword2) {

        List<String> errors = new ArrayList<>();

        if(!newPassword1.matches("^[a-zA-Z0-9]{8,}$")) {
            errors.add("newPassword");
            return ResponseEntity.badRequest().header("New password is not long enough or contains forbidden characters!").body(errors);
        }

        if (!newPassword1.equals(newPassword2)) {
            errors.add("newPassword");
            return ResponseEntity.badRequest().header("New passwords do not match!").body(errors);
        }
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails)
            username = ((UserDetails) principal).getUsername();
        else
            username = principal.toString();

        Optional<Patient> optPat = patientService.getByEmail(username);
        if (optPat.isPresent()){
            Patient patient = optPat.get();
            if (!HealthwalletApplication.pswdEncoder().matches(oldPassword, patient.getPassword())) {
                errors.add("oldPassword");
                return ResponseEntity.badRequest().header("Wrong old password!").body(errors);
            }
            patient.setPassword(HealthwalletApplication.pswdEncoder().encode(newPassword1));
            patientService.store(patient);
            return ResponseEntity.ok().build();
        }

        Optional<Doctor> optDoc = doctorService.getByEmail(username);
        if (optDoc.isPresent()){
            Doctor doctor = optDoc.get();
            if (!HealthwalletApplication.pswdEncoder().matches(oldPassword, doctor.getPassword())) {
                errors.add("oldPassword");
                return ResponseEntity.badRequest().header("Wrong old password!").body(errors);
            }
            doctor.setPassword(HealthwalletApplication.pswdEncoder().encode(newPassword1));
            doctorService.store(doctor);
            return ResponseEntity.ok().build();
        }

        Optional<Hospital> optHos = hospitalService.getByEmail(username);
        if (optHos.isPresent()){
            Hospital hospital = optHos.get();
            if (!HealthwalletApplication.pswdEncoder().matches(oldPassword, hospital.getPassword())) {
                errors.add("oldPassword");
                return ResponseEntity.badRequest().header("Wrong old password!").body(errors);
            }
            hospital.setPassword(HealthwalletApplication.pswdEncoder().encode(newPassword1));
            hospitalService.store(hospital);
            return ResponseEntity.ok().build();
        }
        errors.add("login");
        return ResponseEntity.badRequest().header("User has to be logged in to perform this action!").body(errors);
    }

    @PostMapping("/reset-password")
    public void resetPassword(@RequestParam("email") String email, @RequestParam("passcode") String passCode,
                       @RequestParam("newpassword") String password){

        String encPass = HealthwalletApplication.pswdEncoder().encode(password);

        Optional<Patient> patient = patientService.getByEmail(email);
        if(patient.isPresent()) {
            Patient patient1 = patient.get();
            if (passCode.equals(patient1.getForgottenPassCode())){
                patient1.setPassword(encPass);
                patient1.setForgottenPassCode(null);
                patientService.store(patient1);
                return;
            } else
                throw new RequestDeniedException("Wrong login data");
        }

        Optional<Doctor> doctor = doctorService.getByEmail(email);
        if(doctor.isPresent()) {
            Doctor doctor1 = doctor.get();
            if (passCode.equals(doctor1.getForgottenPassCode())){
                doctor1.setPassword(encPass);
                doctor1.setForgottenPassCode(null);
                doctorService.store(doctor1);
                return;
            } else
                throw new RequestDeniedException("Wrong login data");
        }

        Optional<Hospital> hospital = hospitalService.getByEmail(email);
        if(hospital.isPresent()) {
            Hospital hospital1 = hospital.get();
            if (passCode.equals(hospital1.getForgottenPassCode())){
                hospital1.setPassword(encPass);
                hospital1.setForgottenPassCode(null);
                hospitalService.store(hospital1);
                return;
            } else
                throw new RequestDeniedException("Wrong login data");
        }
        throw new LoginNameNotFoundException("User not found");
    }


    String generatePassCode(){
        Random rnd = new Random();
        int number = rnd.nextInt(999999);

        return String.format("%06d", number);
    }

}
