package hr.fer.suljekibuljeki.healthwallet.service;

import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;

import java.util.List;
import java.util.Set;
import java.util.Optional;

public interface DoctorService {
    List<Doctor> listAll();
    Doctor createDoctor(Doctor doctor);
    Optional<Doctor> findById(int doctorId);
    Doctor fetch(int doctorId);
    Optional<Doctor> getByEmail(String email);
    Optional<Doctor> findByOib(String oib);
    List<Patient> listAllPatients(int doctorId);
    Boolean addPatient(Patient patient, String doctorEmail);
    Boolean removePatient(int doctorId, int patientId);
    int countByConfirmationLink(String confirmationLink);
    Doctor registerPassword(Doctor doctor);
    List<String> updateDoctor(Doctor doctor);
    Hospital findBindedHospital(int doctorId);
    Doctor deleteDoctor(int doctorId);
    void store(Doctor doctor);
    void unbindAllPatients(int doctorId);
    void unbindAllMedRecords(int doctorId);
    Doctor deleteDoctorAlternate(Doctor doctor);
    int numberOfPatients(Doctor doctor);
    Set<Patient> getLastFivePatients (Doctor doctor);
}
