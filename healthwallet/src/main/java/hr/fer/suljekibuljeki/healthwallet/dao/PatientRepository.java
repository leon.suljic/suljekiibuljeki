package hr.fer.suljekibuljeki.healthwallet.dao;

import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PatientRepository extends JpaRepository<Patient, Integer> {
    int countByEmail(String email);
    int countByPin(String pin);
    int countByOib(String oib);
    int countBySupervisor(Patient patient);
    int countByConfirmationLink(String confirmationLink);
    Patient findByOibAndPin(String oib, String pin);
    List<Patient> getBySupervisor(Patient patient);
}
