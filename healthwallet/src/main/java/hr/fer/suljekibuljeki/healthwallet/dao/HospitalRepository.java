package hr.fer.suljekibuljeki.healthwallet.dao;

import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalRepository extends JpaRepository<Hospital, Integer> {
    int countByEmail(String email);
}

