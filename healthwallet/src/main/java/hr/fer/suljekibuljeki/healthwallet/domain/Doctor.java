package hr.fer.suljekibuljeki.healthwallet.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
public class Doctor extends Person {

    //================================================================================
    // Properties
    //================================================================================

    private String licenceNumber;

    @NotNull
    private String speciality;

    @ManyToMany
    private Set<Patient> patients;

    //================================================================================
    // Accessors
    //================================================================================

    public String getLicenceNumber() {
        return licenceNumber;
    }

    public void setLicenceNumber(String licenceNumber) {
        this.licenceNumber = licenceNumber;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public Set<Patient> getPatients() {
        return patients;
    }

    public void setPatients(Set<Patient> patients) {
        this.patients = patients;
    }
}
