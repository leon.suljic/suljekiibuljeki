package hr.fer.suljekibuljeki.healthwallet.dao;

import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DoctorRepository extends JpaRepository<Doctor, Integer> {
	int countByEmail(String email);
	int countByLicenceNumber(String licenceNumber);
    int countByConfirmationLink(String confirmationLink);
}
