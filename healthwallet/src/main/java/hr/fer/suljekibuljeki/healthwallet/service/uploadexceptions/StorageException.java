package hr.fer.suljekibuljeki.healthwallet.service.uploadexceptions;

public class StorageException extends RuntimeException {

	public StorageException(String message) {
		super(message);
	}

	public StorageException(String message, Throwable cause) {
		super(message, cause);
	}
}
