package hr.fer.suljekibuljeki.healthwallet.dao;
import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;
import hr.fer.suljekibuljeki.healthwallet.domain.MedicalRecord;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MedicalRecordRepository extends JpaRepository<MedicalRecord, Integer>  {
    int countByHospital(Hospital hospital);
    int countByPatient(Patient patient);
    int countByDoctor(Doctor doctor);
    List<MedicalRecord> getByHospital(Hospital hospital);
    List<MedicalRecord> getByPatient(Patient patient);
    List<MedicalRecord> getByDoctor(Doctor doctor);
}

