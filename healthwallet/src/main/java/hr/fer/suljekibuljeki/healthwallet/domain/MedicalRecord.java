package hr.fer.suljekibuljeki.healthwallet.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
public class MedicalRecord {

    //================================================================================
    // Properties
    //================================================================================

    @Id
    @GeneratedValue
    private Integer recordID;
    private String file;
    private Date date;
    private String departmentName;
    private String oldRecordDoctor;
    private String oldRecordHospital;
    private String diagnosis;

    @OneToOne
    private Hospital hospital;
    @OneToOne
    private Doctor doctor;
    @OneToOne
    private Patient patient;

    //================================================================================
    // Constructor
    //================================================================================

    public MedicalRecord(String file, Date date, String departmentName, Hospital hospital, Doctor doctor, Patient patient, String diagnosis) {
        this.file = file;
        this.date = date;
        this.departmentName = departmentName;
        this.hospital = hospital;
        this.doctor = doctor;
        this.patient = patient;
        this.diagnosis = diagnosis;
    }

    public MedicalRecord(String file, Date date, String departmentName, String hospital, String doctor, Patient patient, String diagnosis) {
        this.file = file;
        this.date = date;
        this.departmentName = departmentName;
        this.oldRecordHospital = hospital;
        this.oldRecordDoctor = doctor;
        this.patient = patient;
        this.diagnosis = diagnosis;
    }


    public MedicalRecord() {
    }

    //================================================================================
    // Accessors
    //================================================================================

    public Integer getRecordID() {
        return recordID;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getOldRecordHospital() {
        return oldRecordHospital;
    }

    public void setOldRecordHospital(String oldRecordHospital) {
        this.oldRecordHospital = oldRecordHospital;
    }

    public String getOldRecordDoctor() {
        return oldRecordDoctor;
    }

    public void setOldRecordDoctor(String oldRecordDoctor) {
        this.oldRecordDoctor = oldRecordDoctor;
    }

    public String getDiagnosis() { return diagnosis; }

    public void setDiagnosis(String diagnosis) { this.diagnosis = diagnosis; }
}
