package hr.fer.suljekibuljeki.healthwallet.email;


import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;
import hr.fer.suljekibuljeki.healthwallet.service.RequestDeniedException;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

public class SendEmailTLS {

    public static void sendEmail(String subject, String email, Patient patient, Doctor doctor, Hospital hospital, String type) {
        String html = "";

        final String username = "noreplyhealthwallet@gmail.com";
        final String password = "Suljekibuljeki123";

        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("noreplyhealthwallet@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            message.setSubject(subject);


            if (type.equals("registerPatient")) {
                html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                        "<html style=\"width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\">\n" +
                        " <head> \n" +
                        "  <meta charset=\"UTF-8\"> \n" +
                        "  <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\"> \n" +
                        "  <meta name=\"x-apple-disable-message-reformatting\"> \n" +
                        "  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> \n" +
                        "  <meta content=\"telephone=no\" name=\"format-detection\"> \n" +
                        "  <title>New email template 2020-01-14</title> \n" +
                        "  <!--[if (mso 16)]>\n" +
                        "    <style type=\"text/css\">\n" +
                        "    a {text-decoration: none;}\n" +
                        "    </style>\n" +
                        "    <![endif]--> \n" +
                        "  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> \n" +
                        "  <style type=\"text/css\">\n" +
                        "@media only screen and (max-width:600px) {p, ul li, ol li, a { font-size:16px!important; line-height:150%!important } h1 { font-size:30px!important; text-align:center; line-height:120%!important } h2 { font-size:26px!important; text-align:center; line-height:120%!important } h3 { font-size:20px!important; text-align:center; line-height:120%!important } h1 a { font-size:30px!important } h2 a { font-size:26px!important } h3 a { font-size:20px!important } .es-menu td a { font-size:16px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:16px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class=\"gmail-fix\"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:inline-block!important } a.es-button { font-size:20px!important; display:inline-block!important; border-width:6px 25px 6px 25px!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } .es-desk-menu-hidden { display:table-cell!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }\n" +
                        "#outlook a {\n" +
                        "\tpadding:0;\n" +
                        "}\n" +
                        ".ExternalClass {\n" +
                        "\twidth:100%;\n" +
                        "}\n" +
                        ".ExternalClass,\n" +
                        ".ExternalClass p,\n" +
                        ".ExternalClass span,\n" +
                        ".ExternalClass font,\n" +
                        ".ExternalClass td,\n" +
                        ".ExternalClass div {\n" +
                        "\tline-height:100%;\n" +
                        "}\n" +
                        ".es-button {\n" +
                        "\tmso-style-priority:100!important;\n" +
                        "\ttext-decoration:none!important;\n" +
                        "}\n" +
                        "a[x-apple-data-detectors] {\n" +
                        "\tcolor:inherit!important;\n" +
                        "\ttext-decoration:none!important;\n" +
                        "\tfont-size:inherit!important;\n" +
                        "\tfont-family:inherit!important;\n" +
                        "\tfont-weight:inherit!important;\n" +
                        "\tline-height:inherit!important;\n" +
                        "}\n" +
                        ".es-desk-hidden {\n" +
                        "\tdisplay:none;\n" +
                        "\tfloat:left;\n" +
                        "\toverflow:hidden;\n" +
                        "\twidth:0;\n" +
                        "\tmax-height:0;\n" +
                        "\tline-height:0;\n" +
                        "\tmso-hide:all;\n" +
                        "}\n" +
                        "</style> \n" +
                        " </head> \n" +
                        " <body style=\"width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\"> \n" +
                        "  <div class=\"es-wrapper-color\" style=\"background-color:#FFFFFF;\"> \n" +
                        "   <!--[if gte mso 9]>\n" +
                        "\t\t\t<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\n" +
                        "\t\t\t\t<v:fill type=\"tile\" color=\"#ffffff\"></v:fill>\n" +
                        "\t\t\t</v:background>\n" +
                        "\t\t<![endif]--> \n" +
                        "   <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;\"> \n" +
                        "     <tr style=\"border-collapse:collapse;\"> \n" +
                        "      <td valign=\"top\" style=\"padding:0;Margin:0;\"> \n" +
                        "       <table class=\"es-content\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;\"> \n" +
                        "         <tr style=\"border-collapse:collapse;\"> \n" +
                        "          <td align=\"center\" style=\"padding:0;Margin:0;\"> \n" +
                        "           <table class=\"es-content-body\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;border-left:1px solid transparent;border-right:1px solid transparent;border-top:1px solid transparent;border-bottom:1px solid transparent;\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#ffffff\" align=\"center\"> \n" +
                        "             <tr style=\"border-collapse:collapse;\"> \n" +
                        "              <td align=\"left\" style=\"padding:0;Margin:0;padding-left:20px;padding-right:20px;\"> \n" +
                        "               <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \n" +
                        "                 <tr style=\"border-collapse:collapse;\"> \n" +
                        "                  <td width=\"558\" valign=\"top\" align=\"center\" style=\"padding:0;Margin:0;\"> \n" +
                        "                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \n" +
                        "                     <tr style=\"border-collapse:collapse;\"> \n" +
                        "                      <td align=\"center\" style=\"padding:0;Margin:0;\"><img class=\"adapt-img\" src=\"https://ekighy.stripocdn.email/content/guids/916c3cd1-eae1-4658-a21f-47bf738f4fd1/images/39151579001121676.png\" alt style=\"display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;\" width=\"558\"></td> \n" +
                        "                     </tr> \n" +
                        "                   </table></td> \n" +
                        "                 </tr> \n" +
                        "               </table></td> \n" +
                        "             </tr> \n" +
                        "             <tr style=\"border-collapse:collapse;\"> \n" +
                        "              <td align=\"left\" style=\"Margin:0;padding-top:20px;padding-bottom:40px;padding-left:40px;padding-right:40px;\"> \n" +
                        "               <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \n" +
                        "                 <tr style=\"border-collapse:collapse;\"> \n" +
                        "                  <td width=\"518\" align=\"left\" style=\"padding:0;Margin:0;\"> \n" +
                        "                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \n" +
                        "                     <tr style=\"border-collapse:collapse;\"> \n" +
                        "                      <td class=\"es-m-txt-c\" align=\"center\" style=\"padding:0;Margin:0;\"><h2 style=\"Margin:0;line-height:29px;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:24px;font-style:normal;font-weight:normal;color:#33CCFF;\">Lijep pozdrav!</h2></td> \n" +
                        "                     </tr> \n" +
                        "                     <tr style=\"border-collapse:collapse;\"> \n" +
                        "                      <td class=\"es-m-txt-c\" align=\"center\" style=\"padding:0;Margin:0;padding-top:15px;\">" + patient.getFirstName() + ","
                        + " kliknite na sljedeći link kako bi potvrdili Vašu registraciju:" + "<br></td> \n" +
                        "                     </tr> \n" +
                        "                     <tr style=\"border-collapse:collapse;\"> \n" +
                        "                      <td align=\"center\" style=\"Margin:0;padding-left:10px;padding-right:10px;padding-bottom:15px;padding-top:20px;\"><span class=\"es-button-border\" style=\"border-style:solid;border-color:#474745;background:#474745;border-width:0px;display:inline-block;border-radius:20px;width:auto;\"><a href=\"" + "https://healthwallet.herokuapp.com/register/patients/" + patient.getConfirmationLink() + "\" class=\"es-button\" target=\"_blank\" style=\"mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size:16px;color:#EFEFEF;border-style:solid;border-color:#474745;border-width:6px 25px 6px 25px;display:inline-block;background:#474745;border-radius:20px;font-weight:normal;font-style:normal;line-height:19px;width:auto;text-align:center;\">Potvrdite registraciju</a></span></td> \n" +
                        "                     </tr> \n" +
                        "                   </table></td> \n" +
                        "                 </tr> \n" +
                        "               </table></td> \n" +
                        "             </tr> \n" +
                        "             <tr style=\"border-collapse:collapse;\"> \n" +
                        "              <td align=\"left\" style=\"padding:0;Margin:0;padding-left:20px;padding-right:20px;\"> \n" +
                        "               <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \n" +
                        "                 <tr style=\"border-collapse:collapse;\"> \n" +
                        "                  <td width=\"558\" valign=\"top\" align=\"center\" style=\"padding:0;Margin:0;\"> \n" +
                        "                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \n" +
                        "                     <tr style=\"border-collapse:collapse;\"> \n" +
                        "                      <td align=\"center\" style=\"padding:0;Margin:0;\"><p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;\">Hvala što koristite HealthWallet!<br></p></td> \n" +
                        "                     </tr> \n" +
                        "                   </table></td> \n" +
                        "                 </tr> \n" +
                        "               </table></td> \n" +
                        "             </tr> \n" +
                        "           </table></td> \n" +
                        "         </tr> \n" +
                        "       </table></td> \n" +
                        "     </tr> \n" +
                        "   </table> \n" +
                        "  </div> \n" +
                        "  <div style=\"position:absolute;left:-9999px;top:-9999px;margin:0px;\"></div>  \n" +
                        " </body>\n" +
                        "</html>";
            } else if (type.equals("registerDoctor")) {
                html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                        "<html style=\"width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\">\n" +
                        " <head> \n" +
                        "  <meta charset=\"UTF-8\"> \n" +
                        "  <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\"> \n" +
                        "  <meta name=\"x-apple-disable-message-reformatting\"> \n" +
                        "  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> \n" +
                        "  <meta content=\"telephone=no\" name=\"format-detection\"> \n" +
                        "  <title>New email template 2020-01-14</title> \n" +
                        "  <!--[if (mso 16)]>\n" +
                        "    <style type=\"text/css\">\n" +
                        "    a {text-decoration: none;}\n" +
                        "    </style>\n" +
                        "    <![endif]--> \n" +
                        "  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> \n" +
                        "  <style type=\"text/css\">\n" +
                        "@media only screen and (max-width:600px) {p, ul li, ol li, a { font-size:16px!important; line-height:150%!important } h1 { font-size:30px!important; text-align:center; line-height:120%!important } h2 { font-size:26px!important; text-align:center; line-height:120%!important } h3 { font-size:20px!important; text-align:center; line-height:120%!important } h1 a { font-size:30px!important } h2 a { font-size:26px!important } h3 a { font-size:20px!important } .es-menu td a { font-size:16px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:16px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class=\"gmail-fix\"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:inline-block!important } a.es-button { font-size:20px!important; display:inline-block!important; border-width:6px 25px 6px 25px!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } .es-desk-menu-hidden { display:table-cell!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }\n" +
                        "#outlook a {\n" +
                        "\tpadding:0;\n" +
                        "}\n" +
                        ".ExternalClass {\n" +
                        "\twidth:100%;\n" +
                        "}\n" +
                        ".ExternalClass,\n" +
                        ".ExternalClass p,\n" +
                        ".ExternalClass span,\n" +
                        ".ExternalClass font,\n" +
                        ".ExternalClass td,\n" +
                        ".ExternalClass div {\n" +
                        "\tline-height:100%;\n" +
                        "}\n" +
                        ".es-button {\n" +
                        "\tmso-style-priority:100!important;\n" +
                        "\ttext-decoration:none!important;\n" +
                        "}\n" +
                        "a[x-apple-data-detectors] {\n" +
                        "\tcolor:inherit!important;\n" +
                        "\ttext-decoration:none!important;\n" +
                        "\tfont-size:inherit!important;\n" +
                        "\tfont-family:inherit!important;\n" +
                        "\tfont-weight:inherit!important;\n" +
                        "\tline-height:inherit!important;\n" +
                        "}\n" +
                        ".es-desk-hidden {\n" +
                        "\tdisplay:none;\n" +
                        "\tfloat:left;\n" +
                        "\toverflow:hidden;\n" +
                        "\twidth:0;\n" +
                        "\tmax-height:0;\n" +
                        "\tline-height:0;\n" +
                        "\tmso-hide:all;\n" +
                        "}\n" +
                        "</style> \n" +
                        " </head> \n" +
                        " <body style=\"width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\"> \n" +
                        "  <div class=\"es-wrapper-color\" style=\"background-color:#FFFFFF;\"> \n" +
                        "   <!--[if gte mso 9]>\n" +
                        "\t\t\t<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\n" +
                        "\t\t\t\t<v:fill type=\"tile\" color=\"#ffffff\"></v:fill>\n" +
                        "\t\t\t</v:background>\n" +
                        "\t\t<![endif]--> \n" +
                        "   <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;\"> \n" +
                        "     <tr style=\"border-collapse:collapse;\"> \n" +
                        "      <td valign=\"top\" style=\"padding:0;Margin:0;\"> \n" +
                        "       <table class=\"es-content\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;\"> \n" +
                        "         <tr style=\"border-collapse:collapse;\"> \n" +
                        "          <td align=\"center\" style=\"padding:0;Margin:0;\"> \n" +
                        "           <table class=\"es-content-body\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;border-left:1px solid transparent;border-right:1px solid transparent;border-top:1px solid transparent;border-bottom:1px solid transparent;\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#ffffff\" align=\"center\"> \n" +
                        "             <tr style=\"border-collapse:collapse;\"> \n" +
                        "              <td align=\"left\" style=\"padding:0;Margin:0;padding-left:20px;padding-right:20px;\"> \n" +
                        "               <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \n" +
                        "                 <tr style=\"border-collapse:collapse;\"> \n" +
                        "                  <td width=\"558\" valign=\"top\" align=\"center\" style=\"padding:0;Margin:0;\"> \n" +
                        "                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \n" +
                        "                     <tr style=\"border-collapse:collapse;\"> \n" +
                        "                      <td align=\"center\" style=\"padding:0;Margin:0;\"><img class=\"adapt-img\" src=\"https://ekighy.stripocdn.email/content/guids/916c3cd1-eae1-4658-a21f-47bf738f4fd1/images/39151579001121676.png\" alt style=\"display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;\" width=\"558\"></td> \n" +
                        "                     </tr> \n" +
                        "                   </table></td> \n" +
                        "                 </tr> \n" +
                        "               </table></td> \n" +
                        "             </tr> \n" +
                        "             <tr style=\"border-collapse:collapse;\"> \n" +
                        "              <td align=\"left\" style=\"Margin:0;padding-top:20px;padding-bottom:40px;padding-left:40px;padding-right:40px;\"> \n" +
                        "               <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \n" +
                        "                 <tr style=\"border-collapse:collapse;\"> \n" +
                        "                  <td width=\"518\" align=\"left\" style=\"padding:0;Margin:0;\"> \n" +
                        "                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \n" +
                        "                     <tr style=\"border-collapse:collapse;\"> \n" +
                        "                      <td class=\"es-m-txt-c\" align=\"center\" style=\"padding:0;Margin:0;\"><h2 style=\"Margin:0;line-height:29px;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:24px;font-style:normal;font-weight:normal;color:#33CCFF;\">Lijep pozdrav!</h2></td> \n" +
                        "                     </tr> \n" +
                        "                     <tr style=\"border-collapse:collapse;\"> \n" +
                        "                      <td class=\"es-m-txt-c\" align=\"center\" style=\"padding:0;Margin:0;padding-top:15px;\">" + doctor.getFirstName() + " " + doctor.getLastName()
                        + ", bolnica " + hospital.getName() + " Vas je postavila za doktora u HealthWallet aplikaciji. Kliknite ovdje " +
                        "kako biste nastavili registraciju i postavili svoju lozinku:" + "<br></td> \n" +
                        "                     </tr> \n" +
                        "                     <tr style=\"border-collapse:collapse;\"> \n" +
                        "                      <td align=\"center\" style=\"Margin:0;padding-left:10px;padding-right:10px;padding-bottom:15px;padding-top:20px;\"><span class=\"es-button-border\" style=\"border-style:solid;border-color:#474745;background:#474745;border-width:0px;display:inline-block;border-radius:20px;width:auto;\"><a href=\"" + "https://healthwallet.herokuapp.com/register/doctor/" + doctor.getConfirmationLink() + "\" class=\"es-button\" target=\"_blank\" style=\"mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size:16px;color:#EFEFEF;border-style:solid;border-color:#474745;border-width:6px 25px 6px 25px;display:inline-block;background:#474745;border-radius:20px;font-weight:normal;font-style:normal;line-height:19px;width:auto;text-align:center;\">Confirm Email</a></span></td> \n" +
                        "                     </tr> \n" +
                        "                   </table></td> \n" +
                        "                 </tr> \n" +
                        "               </table></td> \n" +
                        "             </tr> \n" +
                        "             <tr style=\"border-collapse:collapse;\"> \n" +
                        "              <td align=\"left\" style=\"padding:0;Margin:0;padding-left:20px;padding-right:20px;\"> \n" +
                        "               <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \n" +
                        "                 <tr style=\"border-collapse:collapse;\"> \n" +
                        "                  <td width=\"558\" valign=\"top\" align=\"center\" style=\"padding:0;Margin:0;\"> \n" +
                        "                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \n" +
                        "                     <tr style=\"border-collapse:collapse;\"> \n" +
                        "                      <td align=\"center\" style=\"padding:0;Margin:0;\"><p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;\">Hvala što koristite HealthWallet!<br></p></td> \n" +
                        "                     </tr> \n" +
                        "                   </table></td> \n" +
                        "                 </tr> \n" +
                        "               </table></td> \n" +
                        "             </tr> \n" +
                        "           </table></td> \n" +
                        "         </tr> \n" +
                        "       </table></td> \n" +
                        "     </tr> \n" +
                        "   </table> \n" +
                        "  </div> \n" +
                        "  <div style=\"position:absolute;left:-9999px;top:-9999px;margin:0px;\"></div>  \n" +
                        " </body>\n" +
                        "</html>";
            } else if (type.equals("pin")) {
                html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                        "<html>\n" +
                        "\n" +
                        "<head>\n" +
                        "    <meta charset=\"UTF-8\">\n" +
                        "    <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\">\n" +
                        "    <meta name=\"x-apple-disable-message-reformatting\">\n" +
                        "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                        "    <meta content=\"telephone=no\" name=\"format-detection\">\n" +
                        "    <title></title>\n" +
                        "    <!--[if (mso 16)]>\n" +
                        "    <style type=\"text/css\">\n" +
                        "    a {text-decoration: none;}\n" +
                        "    </style>\n" +
                        "    <![endif]-->\n" +
                        "    <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->\n" +
                        "</head>\n" +
                        "\n" +
                        "<body>\n" +
                        "    <div class=\"es-wrapper-color\">\n" +
                        "        <!--[if gte mso 9]>\n" +
                        "\t\t\t<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\n" +
                        "\t\t\t\t<v:fill type=\"tile\" color=\"#ffffff\"></v:fill>\n" +
                        "\t\t\t</v:background>\n" +
                        "\t\t<![endif]-->\n" +
                        "        <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "            <tbody>\n" +
                        "                <tr>\n" +
                        "                    <td class=\"esd-email-paddings\" valign=\"top\">\n" +
                        "                        <table class=\"es-content esd-footer-popover\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">\n" +
                        "                            <tbody>\n" +
                        "                                <tr>\n" +
                        "                                    <td class=\"esd-stripe\" align=\"center\">\n" +
                        "                                        <table class=\"es-content-body\" style=\"border-left:1px solid transparent;border-right:1px solid transparent;border-top:1px solid transparent;border-bottom:1px solid transparent;\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#ffffff\" align=\"center\">\n" +
                        "                                            <tbody>\n" +
                        "                                                <tr>\n" +
                        "                                                    <td class=\"esd-structure es-p20r es-p20l\" align=\"left\">\n" +
                        "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                            <tbody>\n" +
                        "                                                                <tr>\n" +
                        "                                                                    <td class=\"esd-container-frame\" width=\"558\" valign=\"top\" align=\"center\">\n" +
                        "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                                            <tbody>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                    <td class=\"esd-block-image\" align=\"center\"><a target=\"_blank\"><img class=\"adapt-img\" src=\"https://ekighy.stripocdn.email/content/guids/916c3cd1-eae1-4658-a21f-47bf738f4fd1/images/39151579001121676.png\" alt style=\"display: block;\" width=\"558\"></a></td>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                            </tbody>\n" +
                        "                                                                        </table>\n" +
                        "                                                                    </td>\n" +
                        "                                                                </tr>\n" +
                        "                                                            </tbody>\n" +
                        "                                                        </table>\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>\n" +
                        "                                                <tr>\n" +
                        "                                                    <td class=\"esd-structure es-p20t es-p40b es-p40r es-p40l\" esd-custom-block-id=\"8537\" align=\"left\">\n" +
                        "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                            <tbody>\n" +
                        "                                                                <tr>\n" +
                        "                                                                    <td class=\"esd-container-frame\" width=\"518\" align=\"left\">\n" +
                        "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                                            <tbody>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                    <td class=\"esd-block-text es-m-txt-c\" align=\"center\">\n" +
                        "                                                                                        <h2 style=\"color: #33ccff;\">Lijep pozdrav!</h2>\n" +
                        "                                                                                    </td>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                    <td class=\"esd-block-text es-m-txt-c es-p15t\" align=\"center\">" + patient.getFirstName() + ", "
                        + "Vaš jedinstveni pin je: " + patient.getPin() + ". Pokažite ovaj PIN Vašem liječniku pri prvom dolasku." + "<br></td>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                            </tbody>\n" +
                        "                                                                        </table>\n" +
                        "                                                                    </td>\n" +
                        "                                                                </tr>\n" +
                        "                                                            </tbody>\n" +
                        "                                                        </table>\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>\n" +
                        "                                                <tr>\n" +
                        "                                                    <td class=\"esd-structure es-p20r es-p20l\" align=\"left\">\n" +
                        "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                            <tbody>\n" +
                        "                                                                <tr>\n" +
                        "                                                                    <td class=\"esd-container-frame\" width=\"558\" valign=\"top\" align=\"center\">\n" +
                        "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                                            <tbody>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                    <td class=\"esd-block-text\" align=\"center\">\n" +
                        "                                                                                        <p>Hvala što koristite HealthWallet!<br></p>\n" +
                        "                                                                                    </td>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                            </tbody>\n" +
                        "                                                                        </table>\n" +
                        "                                                                    </td>\n" +
                        "                                                                </tr>\n" +
                        "                                                            </tbody>\n" +
                        "                                                        </table>\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>\n" +
                        "                                            </tbody>\n" +
                        "                                        </table>\n" +
                        "                                    </td>\n" +
                        "                                </tr>\n" +
                        "                            </tbody>\n" +
                        "                        </table>\n" +
                        "                    </td>\n" +
                        "                </tr>\n" +
                        "            </tbody>\n" +
                        "        </table>\n" +
                        "    </div>\n" +
                        "    <div style=\"position: absolute; left: -9999px; top: -9999px; margin: 0px;\"></div>\n" +
                        "</body>\n" +
                        "\n" +
                        "</html>";
            } else if (type.equals("forgottenPatient")) {
                html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                        "<html>\n" +
                        "\n" +
                        "<head>\n" +
                        "    <meta charset=\"UTF-8\">\n" +
                        "    <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\">\n" +
                        "    <meta name=\"x-apple-disable-message-reformatting\">\n" +
                        "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                        "    <meta content=\"telephone=no\" name=\"format-detection\">\n" +
                        "    <title></title>\n" +
                        "    <!--[if (mso 16)]>\n" +
                        "    <style type=\"text/css\">\n" +
                        "    a {text-decoration: none;}\n" +
                        "    </style>\n" +
                        "    <![endif]-->\n" +
                        "    <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->\n" +
                        "</head>\n" +
                        "\n" +
                        "<body>\n" +
                        "    <div class=\"es-wrapper-color\">\n" +
                        "        <!--[if gte mso 9]>\n" +
                        "\t\t\t<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\n" +
                        "\t\t\t\t<v:fill type=\"tile\" color=\"#ffffff\"></v:fill>\n" +
                        "\t\t\t</v:background>\n" +
                        "\t\t<![endif]-->\n" +
                        "        <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "            <tbody>\n" +
                        "                <tr>\n" +
                        "                    <td class=\"esd-email-paddings\" valign=\"top\">\n" +
                        "                        <table class=\"es-content esd-footer-popover\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">\n" +
                        "                            <tbody>\n" +
                        "                                <tr>\n" +
                        "                                    <td class=\"esd-stripe\" align=\"center\">\n" +
                        "                                        <table class=\"es-content-body\" style=\"border-left:1px solid transparent;border-right:1px solid transparent;border-top:1px solid transparent;border-bottom:1px solid transparent;\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#ffffff\" align=\"center\">\n" +
                        "                                            <tbody>\n" +
                        "                                                <tr>\n" +
                        "                                                    <td class=\"esd-structure es-p20r es-p20l\" align=\"left\">\n" +
                        "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                            <tbody>\n" +
                        "                                                                <tr>\n" +
                        "                                                                    <td class=\"esd-container-frame\" width=\"558\" valign=\"top\" align=\"center\">\n" +
                        "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                                            <tbody>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                    <td class=\"esd-block-image\" align=\"center\"><a target=\"_blank\"><img class=\"adapt-img\" src=\"https://ekighy.stripocdn.email/content/guids/916c3cd1-eae1-4658-a21f-47bf738f4fd1/images/39151579001121676.png\" alt style=\"display: block;\" width=\"558\"></a></td>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                            </tbody>\n" +
                        "                                                                        </table>\n" +
                        "                                                                    </td>\n" +
                        "                                                                </tr>\n" +
                        "                                                            </tbody>\n" +
                        "                                                        </table>\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>\n" +
                        "                                                <tr>\n" +
                        "                                                    <td class=\"esd-structure es-p20t es-p40b es-p40r es-p40l\" esd-custom-block-id=\"8537\" align=\"left\">\n" +
                        "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                            <tbody>\n" +
                        "                                                                <tr>\n" +
                        "                                                                    <td class=\"esd-container-frame\" width=\"518\" align=\"left\">\n" +
                        "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"10\">\n" +
                        "                                                                            <tbody>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                    <td class=\"esd-block-text es-m-txt-c\" align=\"center\">\n" +
                        "                                                                                        <h2 style=\"color: #33ccff;\">Lijep pozdrav!</h2>\n" +
                        "                                                                                    </td>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                    <td class=\"esd-block-text es-m-txt-c es-p15t\" align=\"center\">" + patient.getFirstName() + ", "
                        + "Vaš zahtjev za promjenom lozinke je odobren. Molimo Vas da unesete ovaj kod u predviđeno polje na HealthWallet aplikaciji " +
                        "kako bi postavili novu lozinku: <br></td>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                            </tbody>\n" +
                        "                                                                        </table>\n" +
                        "                                                                    </td>\n" +
                        "                                                                </tr>\n" +
                        "                                                            </tbody>\n" +
                        "                                                        </table>\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>\n" +
                        "                                                <tr>\n" +
                        "                                                    <td class=\"esd-structure es-p20r es-p20l\" align=\"left\">\n" +
                        "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                            <tbody>\n" +
                        "                                                                <tr>\n" +
                        "                                                                    <td class=\"esd-container-frame\" width=\"558\" valign=\"top\" align=\"center\">\n" +
                        "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"10\">\n" +
                        "                                                                            <tbody>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                   <td class=\"esd-block-text es-m-txt-c es-p15t\" align=\"center\">" +
                        "                                                                                          <strong style=\"font-size:35px;\">" + patient.getForgottenPassCode() + "</strong><br></td>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                            </tbody>\n" +
                        "                                                                        </table>\n" +
                        "                                                                    </td>\n" +
                        "                                                                </tr>\n" +
                        "                                                            </tbody>\n" +
                        "                                                        </table>\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>\n" +
                        "                                                <tr>\n" +
                        "                                                    <td class=\"esd-structure es-p20r es-p20l\" align=\"left\">\n" +
                        "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                            <tbody>\n" +
                        "                                                                <tr>\n" +
                        "                                                                    <td class=\"esd-container-frame\" width=\"558\" valign=\"top\" align=\"center\">\n" +
                        "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                                            <tbody>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                    <td class=\"esd-block-text\" align=\"center\">\n" +
                        "                                                                                        <p>Hvala što koristite HealthWallet!<br></p>\n" +
                        "                                                                                    </td>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                            </tbody>\n" +
                        "                                                                        </table>\n" +
                        "                                                                    </td>\n" +
                        "                                                                </tr>\n" +
                        "                                                            </tbody>\n" +
                        "                                                        </table>\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>\n" +
                        "                                            </tbody>\n" +
                        "                                        </table>\n" +
                        "                                    </td>\n" +
                        "                                </tr>\n" +
                        "                            </tbody>\n" +
                        "                        </table>\n" +
                        "                    </td>\n" +
                        "                </tr>\n" +
                        "            </tbody>\n" +
                        "        </table>\n" +
                        "    </div>\n" +
                        "    <div style=\"position: absolute; left: -9999px; top: -9999px; margin: 0px;\"></div>\n" +
                        "</body>\n" +
                        "\n" +
                        "</html>";
            } else if (type.equals("forgottenDoctor")) {
                html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                        "<html>\n" +
                        "\n" +
                        "<head>\n" +
                        "    <meta charset=\"UTF-8\">\n" +
                        "    <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\">\n" +
                        "    <meta name=\"x-apple-disable-message-reformatting\">\n" +
                        "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                        "    <meta content=\"telephone=no\" name=\"format-detection\">\n" +
                        "    <title></title>\n" +
                        "    <!--[if (mso 16)]>\n" +
                        "    <style type=\"text/css\">\n" +
                        "    a {text-decoration: none;}\n" +
                        "    </style>\n" +
                        "    <![endif]-->\n" +
                        "    <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->\n" +
                        "</head>\n" +
                        "\n" +
                        "<body>\n" +
                        "    <div class=\"es-wrapper-color\">\n" +
                        "        <!--[if gte mso 9]>\n" +
                        "\t\t\t<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\n" +
                        "\t\t\t\t<v:fill type=\"tile\" color=\"#ffffff\"></v:fill>\n" +
                        "\t\t\t</v:background>\n" +
                        "\t\t<![endif]-->\n" +
                        "        <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "            <tbody>\n" +
                        "                <tr>\n" +
                        "                    <td class=\"esd-email-paddings\" valign=\"top\">\n" +
                        "                        <table class=\"es-content esd-footer-popover\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">\n" +
                        "                            <tbody>\n" +
                        "                                <tr>\n" +
                        "                                    <td class=\"esd-stripe\" align=\"center\">\n" +
                        "                                        <table class=\"es-content-body\" style=\"border-left:1px solid transparent;border-right:1px solid transparent;border-top:1px solid transparent;border-bottom:1px solid transparent;\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#ffffff\" align=\"center\">\n" +
                        "                                            <tbody>\n" +
                        "                                                <tr>\n" +
                        "                                                    <td class=\"esd-structure es-p20r es-p20l\" align=\"left\">\n" +
                        "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                            <tbody>\n" +
                        "                                                                <tr>\n" +
                        "                                                                    <td class=\"esd-container-frame\" width=\"558\" valign=\"top\" align=\"center\">\n" +
                        "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                                            <tbody>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                    <td class=\"esd-block-image\" align=\"center\"><a target=\"_blank\"><img class=\"adapt-img\" src=\"https://ekighy.stripocdn.email/content/guids/916c3cd1-eae1-4658-a21f-47bf738f4fd1/images/39151579001121676.png\" alt style=\"display: block;\" width=\"558\"></a></td>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                            </tbody>\n" +
                        "                                                                        </table>\n" +
                        "                                                                    </td>\n" +
                        "                                                                </tr>\n" +
                        "                                                            </tbody>\n" +
                        "                                                        </table>\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>\n" +
                        "                                                <tr>\n" +
                        "                                                    <td class=\"esd-structure es-p20t es-p40b es-p40r es-p40l\" esd-custom-block-id=\"8537\" align=\"left\">\n" +
                        "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                            <tbody>\n" +
                        "                                                                <tr>\n" +
                        "                                                                    <td class=\"esd-container-frame\" width=\"518\" align=\"left\">\n" +
                        "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"10\">\n" +
                        "                                                                            <tbody>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                    <td class=\"esd-block-text es-m-txt-c\" align=\"center\">\n" +
                        "                                                                                        <h2 style=\"color: #33ccff;\">Lijep pozdrav!</h2>\n" +
                        "                                                                                    </td>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                    <td class=\"esd-block-text es-m-txt-c es-p15t\" align=\"center\">" + doctor.getFirstName() + " " + doctor.getLastName() + ", "
                        + "Vaš zahtjev za promjenom lozinke je odobren. Molimo Vas da unesete ovaj kod u predviđeno polje na HealthWallet aplikaciji " +
                        "kako bi postavili novu lozinku:<br></td>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                            </tbody>\n" +
                        "                                                                        </table>\n" +
                        "                                                                    </td>\n" +
                        "                                                                </tr>\n" +
                        "                                                            </tbody>\n" +
                        "                                                        </table>\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>\n" +
                        "                                                <tr>\n" +
                        "                                                    <td class=\"esd-structure es-p20r es-p20l\" align=\"left\">\n" +
                        "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                            <tbody>\n" +
                        "                                                                <tr>\n" +
                        "                                                                    <td class=\"esd-container-frame\" width=\"558\" valign=\"top\" align=\"center\">\n" +
                        "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"10\">\n" +
                        "                                                                            <tbody>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                   <td class=\"esd-block-text es-m-txt-c es-p15t\" align=\"center\">" +
                        "                                                                                          <strong style=\"font-size:35px;\">" + doctor.getForgottenPassCode() + "</strong><br></td>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                            </tbody>\n" +
                        "                                                                        </table>\n" +
                        "                                                                    </td>\n" +
                        "                                                                </tr>\n" +
                        "                                                            </tbody>\n" +
                        "                                                        </table>\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>\n" +
                        "                                                <tr>\n" +
                        "                                                    <td class=\"esd-structure es-p20r es-p20l\" align=\"left\">\n" +
                        "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                            <tbody>\n" +
                        "                                                                <tr>\n" +
                        "                                                                    <td class=\"esd-container-frame\" width=\"558\" valign=\"top\" align=\"center\">\n" +
                        "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                                            <tbody>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                    <td class=\"esd-block-text\" align=\"center\">\n" +
                        "                                                                                        <p>Hvala što koristite HealthWallet!<br></p>\n" +
                        "                                                                                    </td>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                            </tbody>\n" +
                        "                                                                        </table>\n" +
                        "                                                                    </td>\n" +
                        "                                                                </tr>\n" +
                        "                                                            </tbody>\n" +
                        "                                                        </table>\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>\n" +
                        "                                            </tbody>\n" +
                        "                                        </table>\n" +
                        "                                    </td>\n" +
                        "                                </tr>\n" +
                        "                            </tbody>\n" +
                        "                        </table>\n" +
                        "                    </td>\n" +
                        "                </tr>\n" +
                        "            </tbody>\n" +
                        "        </table>\n" +
                        "    </div>\n" +
                        "    <div style=\"position: absolute; left: -9999px; top: -9999px; margin: 0px;\"></div>\n" +
                        "</body>\n" +
                        "\n" +
                        "</html>";
            } else if (type.equals("forgottenHospital")) {
                html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                        "<html>\n" +
                        "\n" +
                        "<head>\n" +
                        "    <meta charset=\"UTF-8\">\n" +
                        "    <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\">\n" +
                        "    <meta name=\"x-apple-disable-message-reformatting\">\n" +
                        "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                        "    <meta content=\"telephone=no\" name=\"format-detection\">\n" +
                        "    <title></title>\n" +
                        "    <!--[if (mso 16)]>\n" +
                        "    <style type=\"text/css\">\n" +
                        "    a {text-decoration: none;}\n" +
                        "    </style>\n" +
                        "    <![endif]-->\n" +
                        "    <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->\n" +
                        "</head>\n" +
                        "\n" +
                        "<body>\n" +
                        "    <div class=\"es-wrapper-color\">\n" +
                        "        <!--[if gte mso 9]>\n" +
                        "\t\t\t<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\n" +
                        "\t\t\t\t<v:fill type=\"tile\" color=\"#ffffff\"></v:fill>\n" +
                        "\t\t\t</v:background>\n" +
                        "\t\t<![endif]-->\n" +
                        "        <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "            <tbody>\n" +
                        "                <tr>\n" +
                        "                    <td class=\"esd-email-paddings\" valign=\"top\">\n" +
                        "                        <table class=\"es-content esd-footer-popover\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">\n" +
                        "                            <tbody>\n" +
                        "                                <tr>\n" +
                        "                                    <td class=\"esd-stripe\" align=\"center\">\n" +
                        "                                        <table class=\"es-content-body\" style=\"border-left:1px solid transparent;border-right:1px solid transparent;border-top:1px solid transparent;border-bottom:1px solid transparent;\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#ffffff\" align=\"center\">\n" +
                        "                                            <tbody>\n" +
                        "                                                <tr>\n" +
                        "                                                    <td class=\"esd-structure es-p20r es-p20l\" align=\"left\">\n" +
                        "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                            <tbody>\n" +
                        "                                                                <tr>\n" +
                        "                                                                    <td class=\"esd-container-frame\" width=\"558\" valign=\"top\" align=\"center\">\n" +
                        "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                                            <tbody>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                    <td class=\"esd-block-image\" align=\"center\"><a target=\"_blank\"><img class=\"adapt-img\" src=\"https://ekighy.stripocdn.email/content/guids/916c3cd1-eae1-4658-a21f-47bf738f4fd1/images/39151579001121676.png\" alt style=\"display: block;\" width=\"558\"></a></td>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                            </tbody>\n" +
                        "                                                                        </table>\n" +
                        "                                                                    </td>\n" +
                        "                                                                </tr>\n" +
                        "                                                            </tbody>\n" +
                        "                                                        </table>\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>\n" +
                        "                                                <tr>\n" +
                        "                                                    <td class=\"esd-structure es-p20t es-p40b es-p40r es-p40l\" esd-custom-block-id=\"8537\" align=\"left\">\n" +
                        "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                            <tbody>\n" +
                        "                                                                <tr>\n" +
                        "                                                                    <td class=\"esd-container-frame\" width=\"518\" align=\"left\">\n" +
                        "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"10\">\n" +
                        "                                                                            <tbody>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                    <td class=\"esd-block-text es-m-txt-c\" align=\"center\">\n" +
                        "                                                                                        <h2 style=\"color: #33ccff;\">Lijep pozdrav!</h2>\n" +
                        "                                                                                    </td>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                    <td class=\"esd-block-text es-m-txt-c es-p15t\" align=\"center\">" + hospital.getName() + ", "
                        + "Vaš zahtjev za promjenom lozinke je odobren. Molimo Vas da unesete ovaj kod u predviđeno polje na HealthWallet aplikaciji " +
                        "kako bi postavili novu lozinku: <br></td>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                            </tbody>\n" +
                        "                                                                        </table>\n" +
                        "                                                                    </td>\n" +
                        "                                                                </tr>\n" +
                        "                                                            </tbody>\n" +
                        "                                                        </table>\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>\n" +
                        "                                                <tr>\n" +
                        "                                                    <td class=\"esd-structure es-p20r es-p20l\" align=\"left\">\n" +
                        "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                            <tbody>\n" +
                        "                                                                <tr>\n" +
                        "                                                                    <td class=\"esd-container-frame\" width=\"558\" valign=\"top\" align=\"center\">\n" +
                        "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"10\">\n" +
                        "                                                                            <tbody>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                   <td class=\"esd-block-text es-m-txt-c es-p15t\" align=\"center\">" +
                        "                                                                                          <strong style=\"font-size:35px;\">" + hospital.getForgottenPassCode() + "</strong><br></td>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                            </tbody>\n" +
                        "                                                                        </table>\n" +
                        "                                                                    </td>\n" +
                        "                                                                </tr>\n" +
                        "                                                            </tbody>\n" +
                        "                                                        </table>\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>\n" +
                        "                                                <tr>\n" +
                        "                                                    <td class=\"esd-structure es-p20r es-p20l\" align=\"left\">\n" +
                        "                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                            <tbody>\n" +
                        "                                                                <tr>\n" +
                        "                                                                    <td class=\"esd-container-frame\" width=\"558\" valign=\"top\" align=\"center\">\n" +
                        "                                                                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                                                                            <tbody>\n" +
                        "                                                                                <tr>\n" +
                        "                                                                                    <td class=\"esd-block-text\" align=\"center\">\n" +
                        "                                                                                        <p>Hvala što koristite HealthWallet!<br></p>\n" +
                        "                                                                                    </td>\n" +
                        "                                                                                </tr>\n" +
                        "                                                                            </tbody>\n" +
                        "                                                                        </table>\n" +
                        "                                                                    </td>\n" +
                        "                                                                </tr>\n" +
                        "                                                            </tbody>\n" +
                        "                                                        </table>\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>\n" +
                        "                                            </tbody>\n" +
                        "                                        </table>\n" +
                        "                                    </td>\n" +
                        "                                </tr>\n" +
                        "                            </tbody>\n" +
                        "                        </table>\n" +
                        "                    </td>\n" +
                        "                </tr>\n" +
                        "            </tbody>\n" +
                        "        </table>\n" +
                        "    </div>\n" +
                        "    <div style=\"position: absolute; left: -9999px; top: -9999px; margin: 0px;\"></div>\n" +
                        "</body>\n" +
                        "\n" +
                        "</html>";
            } else throw new RequestDeniedException("Mail of type" + type + " does not exist!");

            message.setContent(html, "text/html; charset=utf-8");
            Transport.send(message);

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

}

