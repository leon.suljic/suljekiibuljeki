package hr.fer.suljekibuljeki.healthwallet.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@MappedSuperclass
public class User {

    //================================================================================
    // Properties
    //================================================================================

    @Id
    @GeneratedValue
    private Integer id;

    @Column(unique = true)
    @NotNull
    private String email;

    private String password;

    private String confirmationLink;

    private String forgottenPassCode;

    //================================================================================
    // Accessors
    //================================================================================

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        if(this.id == null) {
            this.id = id;
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmationLink() { return confirmationLink; }

    public void setConfirmationLink(String confirmationLink) { this.confirmationLink = confirmationLink; }

    public String getForgottenPassCode(){ return forgottenPassCode;}

    public void setForgottenPassCode(String forgottenPassCode) { this.forgottenPassCode = forgottenPassCode;}
}
