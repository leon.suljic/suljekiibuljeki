package hr.fer.suljekibuljeki.healthwallet.rest;

import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;
import hr.fer.suljekibuljeki.healthwallet.service.DoctorService;
import hr.fer.suljekibuljeki.healthwallet.service.HospitalService;
import hr.fer.suljekibuljeki.healthwallet.service.LoginNameNotFoundException;
import hr.fer.suljekibuljeki.healthwallet.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static org.springframework.security.core.authority.AuthorityUtils.commaSeparatedStringToAuthorityList;

@Service
public class WalletUserDetailService implements UserDetailsService {

    @Value("${healthwallet.admin.password}")
    private String adminPasswordHash;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private PatientService patientService;

    @Autowired
    private HospitalService hospitalService;

    @Override
    public UserDetails loadUserByUsername(String username) {
        username=username.toLowerCase();

        if(username.equals("admin")) {
            return new User(username, adminPasswordHash, commaSeparatedStringToAuthorityList("ROLE_ADMIN"));
        }

        Optional<Patient> patient = patientService.getByEmail(username);
        if(patient.isPresent()) {
            return new User(username, patient.get().getPassword(), commaSeparatedStringToAuthorityList("ROLE_PATIENT"));
        }

        Optional<Doctor> doctor = doctorService.getByEmail(username);
        if(doctor.isPresent()) {
            return new User(username, doctor.get().getPassword(), commaSeparatedStringToAuthorityList("ROLE_DOCTOR"));
        }

        Optional<Hospital> hospital = hospitalService.getByEmail(username);
        if(hospital.isPresent()) {
            return new User(username, hospital.get().getPassword(), commaSeparatedStringToAuthorityList("ROLE_HOSPITAL"));
        }

        throw new LoginNameNotFoundException("User with email: " + username + " hasn't been found!");
    }

//    private List<GrantedAuthority> authorities(String username)
//    {
//        if (username.equals("admin"))
//            return commaSeparatedStringToAuthorityList("ROLE_ADMIN");
//        Optional<Hospital> hospital = hospitalService.getByEmail(username);
//        if (hospital.isPresent())
//            return  commaSeparatedStringToAuthorityList("ROLE_HOSPITAL");
//        Optional<Doctor> doctor = doctorService.getByEmail(username);
//        if (doctor.isPresent())
//            return  commaSeparatedStringToAuthorityList("ROLE_DOCTOR");
//        Optional<Patient> patient = patientService.getByEmail(username);
//        if (patient.isPresent()) {
//            return commaSeparatedStringToAuthorityList("ROLE_PATIENT");
//        }
//        return null;
//    }
}
