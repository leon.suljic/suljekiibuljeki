package hr.fer.suljekibuljeki.healthwallet.domain;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Hospital extends User {

    //================================================================================
    // Properties
    //================================================================================

    @NotNull
    private String name;

    @Size(min = 11, max = 11)
    private String oib;

    private String address;

    private String zipCode;

    @OneToMany
    private Set<Doctor> doctors;

    //================================================================================
    // Constructors
    //================================================================================

    public Hospital()
    {
        doctors = new HashSet<>();
    }

    //================================================================================
    // Accessors
    //================================================================================

    public String getOib() {
        return oib;
    }

    public void setOib(String oib) {
        this.oib = oib;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Set<Doctor> getDoctors() {
        return doctors;
    }

    public void setDoctors(Set<Doctor> doctors) {
        this.doctors = doctors;
    }
}
