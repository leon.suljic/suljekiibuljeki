package hr.fer.suljekibuljeki.healthwallet.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public interface StorageService {

    void store(MultipartFile filePath) throws IOException;

    void loadAll();

    Path load(String filename);

    void deleteAll();

    File sendFileToFrontend(String filename);
}