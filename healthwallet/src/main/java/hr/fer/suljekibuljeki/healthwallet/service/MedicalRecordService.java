package hr.fer.suljekibuljeki.healthwallet.service;

import hr.fer.suljekibuljeki.healthwallet.domain.Doctor;
import hr.fer.suljekibuljeki.healthwallet.domain.Hospital;
import hr.fer.suljekibuljeki.healthwallet.domain.MedicalRecord;
import hr.fer.suljekibuljeki.healthwallet.domain.Patient;

import java.util.List;

public interface MedicalRecordService {
    List<MedicalRecord> listAll();
    List<MedicalRecord> sortMedRecords(List<MedicalRecord> medrecords);
    MedicalRecord createMedicalRecord(MedicalRecord record, Patient patient);
    MedicalRecord createMedicalRecord(MedicalRecord record);
    List<MedicalRecord> getAllForPatient(Patient patient);
    List<MedicalRecord> getAllForPatient(int patientId);
    List<MedicalRecord> getAllForDoctor(Doctor doctor);
    List<MedicalRecord> getAllForDoctor(int doctorId);
    List<MedicalRecord> getAllForHospital(Hospital hospital);
    List<MedicalRecord> getAllForHospital(int hospitalId);
    String deleteRecord(int recordId);
    int countByHospital(Hospital hospital);
    int numberOfRecords(Patient patient);
    int numberOfRecords(Doctor doctor);
    List<MedicalRecord> getLastFiveMedRecords(Doctor doctor);
    List<MedicalRecord> getLastFiveMedRecords(Patient patient);
}
