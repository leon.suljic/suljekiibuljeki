import React from "react";
// creates a beautiful scrollbar
import "perfect-scrollbar/css/perfect-scrollbar.css";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import PatientRegisterCard from "../views/Login/PatientRegisterCard";
import HospitalRegisterCard from "../views/Login/HospitalRegisterCard";
import DoctorPasswordSetCard from "../views/Login/DoctorPasswordSetCard";
import PatientConfirmRegistrationCard from "../views/Login/PatientConfirmRegistrationCard";
import LoginCard from "../views/Login/LoginCard";
import ResetPasswordCard from "../views/Login/ResetPasswordCard";

import styles from "assets/jss/material-dashboard-react/layouts/homeStyle.js";

import PropTypes from "prop-types";

const useStyles = makeStyles(styles);

export default function Register(props) {

  const {
    login,
    register,
    patient,
    hospital,
    doctor,
    confirmation,
    onRegisterPatient,
    onRegisterHospital,
      forgottenPassword,
      updateUser,
    ...rest
  } = props;

  // styles
  const classes = useStyles();
  // ref to help us initialize PerfectScrollbar on windows devices
  const mainPanel = React.createRef();
  // states and functions
  const [isHospital, setIsHospital] = React.useState(hospital);
  const [isPatient, setIsPatient] = React.useState(patient);
  const [isDoctor, setIsDoctor] = React.useState(doctor);


  const patientRegister = () => {

    if (confirmation)
      return (
          <PatientConfirmRegistrationCard
              onComplete={() => {
                  props.onLogin();
                  updateUser();
              }}
              patientCode={props.match.params.patientCode}
              {...rest}
          />
      );

    return (
      <div className={classes.card}>
        <PatientRegisterCard
            onChangeType={() => {
              setIsHospital(true);
              setIsPatient(false);
            }}
            {...rest}
        />
      </div>);
  };

  return (

      <div className={classes.centerBlock}>
          {login ?
              <div className={classes.card}>
                <LoginCard
                    {...rest}
                />
              </div> : null}
          {register && isPatient ?
              patientRegister()
               : null }
          {register && isHospital ?
                <div className={classes.card}>
                  <HospitalRegisterCard
                      onChangeType={() => {
                        setIsHospital(false);
                        setIsPatient(true);
                      }}
                      {...rest}
                  />
                </div> : null}
          {register && isDoctor ?
              <div className={classes.card}>
                <DoctorPasswordSetCard
                    docCode={props.match.params.docCode}
                    {...rest}
                />
              </div> : null}

            {forgottenPassword ?
                <div className={classes.card}>
                    <ResetPasswordCard
                        {...rest}
                    />
                </div> : null }
      </div>
  );
}

Register.propTypes = {
  patient: PropTypes.bool,
  hospital: PropTypes.bool,
  doctor: PropTypes.bool,
  login: PropTypes.bool,
  register: PropTypes.bool,
  confirmation: PropTypes.bool,
  forgottenPassword: PropTypes.bool
};
