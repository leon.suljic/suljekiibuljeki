import React from "react";
import {Route} from "react-router-dom";
// creates a beautiful scrollbar
import "perfect-scrollbar/css/perfect-scrollbar.css";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import HomeHeader from "components/Navbars/HomeHeader.js";
import Footer from "components/Footer/Footer.js";
import Sidebar from "components/Sidebar/Sidebar.js";
import FixedPlugin from "components/FixedPlugin/FixedPlugin.js";
import Dashboard from "./Dashboard";
import Button from "components/CustomButtons/Button.js";
import Header from "../components/Navbars/Navbar";
import Register from "./Register";
import Upload from "../components/FileUpload/Upload/Upload";
import routes from "PatientDashboardRoutes.js";

import styles from "assets/jss/material-dashboard-react/layouts/homeStyle.js";

import bgImage from "assets/img/sidebar-2.jpg";
import logo from "assets/img/reactlogo.png";
import LoginCard from "../views/Login/LoginCard";
import healthWalletLogo from "assets/images/healthwalletlogonobuffer.png"
import Card from "../components/Card/Card";
import CardBody from "../components/Card/CardBody";


const useStyles = makeStyles(styles);

export default function Home(props) {
  const {
    isLoggedIn,
    setIsLoggedIn,
    setUsername,
    updateUser,
    ...rest
  } = props;
  // styles
  const classes = useStyles();
  // ref to help us initialize PerfectScrollbar on windows devices
  const mainPanel = React.createRef();
  // states and functions
  const [image, setImage] = React.useState(bgImage);
  const [color, setColor] = React.useState("blue");
  const [fixedClasses, setFixedClasses] = React.useState("dropdown show");
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const [isLoggingIn, setIsLoggingIn] = React.useState(false);
  function onLogin() {
    setIsLoggedIn(true);
    props.history.push('/dashboard');
  };
  const onLogout = () => {
    setIsLoggedIn(false);
    props.history.push('/');
  };
  const handleImageClick = image => {
    setImage(image);
  };
  const handleColorClick = color => {
    setColor(color);
  };
  const onLoginButtonClick = event => {
    props.history.push('/login');
  };
  const onRegisterButtonClick = event => {
    props.history.push('/register');
  };
  const handleFixedClick = () => {
    if (fixedClasses === "dropdown") {
      setFixedClasses("dropdown show");
    } else {
      setFixedClasses("dropdown");
    }
  };
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const resizeFunction = () => {
    if (window.innerWidth >= 960) {
      setMobileOpen(false);
    }
  };

  const passwordReset = () => {
    props.history.push('/forgotten-password');
  };

  const openHomePage = () => {
    props.history.push('/home');
  };

  function loginAsUser(email, password, res) {

    const body = `username=${email}&password=${password}`;
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: body
    };
    fetch('/api/login', options)
        .then(response => {
          if (response.status === 200) {
            if (res !== null)
              res(true);
          } else {
            if (res !== null)
              res(false);
          }
        });
  }


  return (
      <div className={classes.wrapper}>
        <div className={classes.mainPanel} ref={mainPanel}>
          <HomeHeader
              routes={routes}
              handleDrawerToggle={handleDrawerToggle}
              onLoginClick={onLoginButtonClick}
              onRegisterClick={onRegisterButtonClick}
              {...rest}
          />

          <div>
            <div className={classes.centerPanel}>
              <div className={classes.centerBlock}>

              {isLoggedIn ?
                  <Route
                      exact path="/(login|register)"
                      render={(props) => props.history.push('/dashboard')}
                  /> : null}

              <Route
                  exact path="/login"
                  render={(props) => <Register
                      login
                      onLogin={onLogin}
                      loginAsUser={loginAsUser}
                      onLogout={onLogout}
                      passwordReset={passwordReset}
                  />}
              />
              <Route
                  path="/home"
                  render={(props) =>
                    <div>
                      <img className={classes.image} src={healthWalletLogo} alt="Health Wallet"/>
                      <h4 style={{
                        textAllign: 'center'
                      }}>Alat za pohranu i organizaciju svih Vaših zdravstvenih podataka</h4>
                    </div>
                  }
              />

              <Route
                  exact path="/register"
                  render={(props) => <Register
                      register
                      patient
                      onLogin={onLogin}
                      loginAsUser={loginAsUser}
                      updateUser={updateUser}
                  />}
              />

              <Route
                  path="/register/doctor/:docCode"
                  render={(props) => <Register
                      register
                      doctor
                      onLogin={onLogin}
                      loginAsUser={loginAsUser}
                      updateUser={updateUser}
                      {...props}
                  />}
              />

              <Route
                  path="/register/patients/:patientCode"
                  render={(props) => <Register
                      register
                      patient
                      confirmation
                      onLogin={onLogin}
                      loginAsUser={loginAsUser}
                      updateUser={updateUser}
                      {...props}
                  />}
              />

              <Route
                  path="/forgotten-password"
                  render={(props) => <Register
                      forgottenPassword
                      onLogin={onLogin}
                      loginAsUser={loginAsUser}
                  />}
              />

            </div>
            </div>
          </div>
          <div className={classes.footer}>
            <Footer
              onClickHome={openHomePage}
            />
          </div>
        </div>
      </div>
  );
}
