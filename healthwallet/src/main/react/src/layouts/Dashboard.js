import React from "react";
import ReactLoading from 'react-loading';

import { Switch, Route, Redirect } from "react-router-dom";
// creates a beautiful scrollbar
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footer/Footer.js";
import Sidebar from "components/Sidebar/Sidebar.js";
import FixedPlugin from "components/FixedPlugin/FixedPlugin.js";

import patientRoutes from "PatientDashboardRoutes.js";
import doctorRoutes from "DoctorDashboardRoutes";
import hospitalRoutes from "HospitalDashboardRoutes";

import styles from "assets/jss/material-dashboard-react/layouts/adminStyle.js";

import bgImage from "assets/img/heart-beat.jpg";
import logo from "assets/images/healthWalletLogoIcon.png";
import TableList from "../views/TableList/TableList";
import Upload from "../components/FileUpload/Upload/Upload";
import Card from "../components/Card/Card";
import CardBody from "../components/Card/CardBody";
import UploadCard from "../views/FileUpload/UploadCard";
import Home from "./Home";
import PatientsList from "../views/TableList/PatientsList";

let ps;

const useStyles = makeStyles(styles);

const useStateWithLocalStorage = localStorageKey => {
  const [value, setValue] = React.useState(
      localStorage.getItem(localStorageKey) || ''
  );
  React.useEffect(() => {
    localStorage.setItem(localStorageKey, value);
  }, [value]);
  return [value, setValue];
};

export default function Dashboard(props) {

  const {
    setIsLoggedIn,
    isLoggedIn,
    isLoading,
    user,
    updateUser,
    ...rest
  } = props;

  // styles
  const classes = useStyles();
  // ref to help us initialize PerfectScrollbar on windows devices
  const mainPanel = React.createRef();
  // states and functions
  const [image, setImage] = React.useState(bgImage);
  const [color, setColor] = React.useState("blue");
  const [fixedClasses, setFixedClasses] = React.useState("dropdown show");
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [loggedIn, setLoggedIn] = React.useState(isLoggedIn);
  const [hasLoggedOut, setHasLoggedOut] = React.useState(false);
  const [routes, setRoutes] = React.useState([]);
  const [patient, setPatientVal] = React.useState(null);
  const [editRestriction, setEditRestriction] = React.useState(false);
  const [statistics, setStatistics] = React.useState([]);

  const [patientSavedData, setPatientSavedData] = useStateWithLocalStorage('currentPatient');

  const handleImageClick = image => {
    setImage(image);
  };
  const handleColorClick = color => {
    setColor(color);
  };

  React.useEffect(() => {
    if (patientSavedData !== null && patientSavedData !== '')
      setPatient(JSON.parse(patientSavedData));
  }, [patientSavedData]);

  function setPatient(pat) {
    setPatientVal(pat);
    setPatientSavedData(JSON.stringify(pat));
    refreshRoutes();
  }

  React.useEffect(() => getStatistics(), [user, ]);

  function getStatistics() {
    if (user == null || user.role == null)
      return;
    let path = '';
    if (user.role === 'patient')
      path = '/api/patients/statistics';
    if (user.role === 'doctor')
      path = '/api/doctors/statistics';
    if (user.role === 'hospital')
      path = '/api/hospitals/statistics';

    fetch(path, {method: 'GET'})
        .then(res => {
          if (res.status >= 200 && res.status < 300) {
            return res.json();
          } else {
            return '';
          }
        })
        .then(data => {
          setStatistics(data)
        });
  }

  const switchRoutes = (
      <Switch>
        {routes.map((prop, key) => {
          if (prop.requirePatient && patient === null)
          {
            return null;
          }
          if (prop.layout === "/dashboard") {
              return (
                  <Route
                      path={prop.layout + prop.path}
                      key={key}
                      render={(props) => <prop.component {...props}
                                                         user={user}
                                                         patient={patient}
                                                         setPatient={setPatient}
                                                         editRestriction={editRestriction}
                                                         updateUser={updateUser}
                                                         statistics={statistics}/>}
                  />
              );
          }
          return null;
        })}
        <Redirect exact from="/dashboard" to="/dashboard/dash"/>
      </Switch>
  );

  const handleFixedClick = () => {
    if (fixedClasses === "dropdown") {
      setFixedClasses("dropdown show");
    } else {
      setFixedClasses("dropdown");
    }
  };
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const getRoute = () => {
    return window.location.pathname !== "/admin/maps";
  };
  const resizeFunction = () => {
    if (window.innerWidth >= 960) {
      setMobileOpen(false);
    }
  };

  function refreshRoutes() {

    const newRoutes = [];
    let routes = null;
    if (user === null)
      return;

    let restriction = false;

    if (user.role === 'patient') {
      routes = patientRoutes;
      restriction = user.user.supervisor !== null;
      setEditRestriction(restriction);
    }
    if (user.role === 'doctor')
      routes = doctorRoutes;
    if (user.role === 'hospital')
      routes = hospitalRoutes;
    if (routes === null)
      return;
    routes.map((r) => {
      if (!r.requireSupervised || statistics.numberOfPatients > 0) {
        if (patient === null && !r.requirePatient) {
          if (!restriction || r.path !== '/upload')
            newRoutes.push(r);
        }
        if (patient !== null && !r.hiddenWhilePatient) {
          newRoutes.push(r);
        }
      }
    });
    setRoutes(newRoutes);
  }

  React.useEffect(() => {
    refreshRoutes()
  }, [user, statistics]);

  React.useEffect(() => {
    const {pathname} = props.location;
    let path = pathname.toString();
    if (path.endsWith('/'))
      path = path.substring(0, path.length - 1);
    if (path.endsWith('/dash') || path.endsWith('/user') || path.endsWith('/dashboard'))
      setPatient(null);
  }, []);

  function onSubmitLogout() {
    setPatient(null);
    props.logout();
  }

  function redirectToHome() {
    props.history.push('/home');
  }

  // initialize and destroy the PerfectScrollbar plugin
  React.useEffect(() => {
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(mainPanel.current, {
        suppressScrollX: true,
        suppressScrollY: false
      });
      document.body.style.overflow = "hidden";
    }
    window.addEventListener("resize", resizeFunction);
    // Specify how to clean up after this effect:
    return function cleanup() {
      if (navigator.platform.indexOf("Win") > -1) {
        ps.destroy();
      }
      window.removeEventListener("resize", resizeFunction);
    };
  }, [mainPanel]);
  return (
      <div className={classes.wrapper}>
        <Sidebar
            routes={routes}
            logoText={"Health Wallet"}
            logo={logo}
            image={image}
            handleDrawerToggle={handleDrawerToggle}
            onLogout={onSubmitLogout}
            user={user}
            patient={patient}
            setPatient={setPatient}
            open={mobileOpen}
            color={color}
            {...rest}
        />
        <div className={classes.mainPanel} ref={mainPanel}>
          <Navbar
              routes={routes}
              handleDrawerToggle={handleDrawerToggle}
              onLogout={onSubmitLogout}
              user={user}
              setPatient={setPatient}
              {...rest}
          />

          {isLoading ?
              <div className={classes.centerPanel}>
                <ReactLoading type="spinningBubbles" color="gray" height={'5%'} width={'5%'} />
              </div> :
              <div className={classes.content}>
                <div className={classes.container}>
                  {switchRoutes}
                </div>
              </div>
          }

          {getRoute() ? <Footer onClickHome={redirectToHome}/> : null}
        </div>
      </div>
  );
}
