/*!

=========================================================
* Material PatientDashboard React - v1.8.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from 'react';
import { Redirect } from "react-router-dom";

// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import People from "@material-ui/icons/People";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import BubbleChart from "@material-ui/icons/BubbleChart";
import LocationOn from "@material-ui/icons/LocationOn";
import Notifications from "@material-ui/icons/Notifications";
import CloudUpload from "@material-ui/icons/CloudUpload";
import Unarchive from "@material-ui/icons/Unarchive";
import Language from "@material-ui/icons/Language";
import CloseIcon from '@material-ui/icons/Close';
// core components/views for Admin layout
import DashboardPage from "views/Dashboard/DoctorDashboard.js";
import TableList from "views/TableList/TableList.js";
import Typography from "views/Typography/Typography.js";
import Icons from "views/Icons/Icons.js";
import Maps from "views/Maps/Maps.js";
import NotificationsPage from "views/Notifications/Notifications.js";
import UpgradeToPro from "views/UpgradeToPro/UpgradeToPro.js";
import LoginCard from "./views/Login/LoginCard";
import UploadCard from "./views/FileUpload/UploadCard";
import MedicalRecordsList from "./views/TableList/MedicalRecordsList";
import PatientProfile from "./views/UserProfile/PatientProfileDoctor";
import PatientsList from "./views/TableList/PatientsList";
import DoctorProfileCard from "./views/UserProfile/DoctorProfileCard";

const dashboardRoutes = [
  {
    path: "/dash",
    name: "Dashboard",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/dashboard",
    hiddenWhilePatient: true
  },
  {
    path: "/user",
    name: "Korisnički račun",
    icon: Person,
    component: DoctorProfileCard,
    layout: "/dashboard",
    hiddenWhilePatient: true
  },
  {
    path: "/patients",
    name: "Pacijenti",
    icon: People,
    component: PatientsList,
    layout: "/dashboard",
    hiddenWhilePatient: true
  },
  {
    path: "/about-patient",
    name: "O pacijentu",
    icon: Person,
    component: PatientProfile,
    layout: "/dashboard",
    requirePatient: true
  },
  {
    path: "/records",
    name: "Nalazi",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    component: MedicalRecordsList,
    layout: "/dashboard",
    requirePatient: true
  },
  {
    path: "/upload",
    name: "Dodavanje nalaza",
    icon: CloudUpload,
    component: UploadCard,
    layout: "/dashboard",
    requirePatient: true
  },
  {
    path: "/close-record",
    name: "Zatvori karton",
    icon: CloseIcon,
    component: CloseRecord ,
    layout: "/dashboard",
    requirePatient: true
  }
];

function CloseRecord(props) {

  React.useEffect(() => {
    console.log('Patient set to null');
    props.setPatient(null)
    props.history.push('/dashboard/patients');
  }, []);

  return (
      <div>
      </div>
  );
}

export default dashboardRoutes;
