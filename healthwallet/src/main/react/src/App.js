import React from "react";
import { createBrowserHistory } from "history";
import {Router, Route, Switch, Redirect, BrowserRouter} from "react-router-dom";

import './App.css';
import Home from "./layouts/Home";
import Dashboard from "./layouts/Dashboard";
import PrivateRoute from "./PrivateRoute";

const useStateWithLocalStorage = localStorageKey => {
    const [value, setValue] = React.useState(
        localStorage.getItem(localStorageKey) || ''
    );
    React.useEffect(() => {
        localStorage.setItem(localStorageKey, value);
    }, [value]);
    return [value, setValue];
};

function App() {

  const hist = createBrowserHistory();

  const [isLoggedIn, setIsLoggedIn] = React.useState(false);
  const [user, setUser] = React.useState(null);
  const [isLoading, setIsLoading] = React.useState(true);

  const [userSaved, setUserSaved] = useStateWithLocalStorage('user');

  function getUser() {
      setIsLoading(true);
      fetch('/api/home/current-user')
          .then(response => {
              if (response.status === 202 || response.status === 200) {
                  setIsLoggedIn(true);
                  return response.json();
              } else {
                  setIsLoggedIn(false);
                  return [];
              }
          })
          .then(data => {
              setIsLoading(false);
              setUser(data);
          });
  }

  function refreshUser() {
      fetch('/api/home/current-user')
          .then(response => {
              if (response.status === 202 || response.status === 200) {
                  setIsLoggedIn(true);
                  return response.json();
              } else {
                  setIsLoggedIn(false);
                  return [];
              }
          })
          .then(data => {
              setUser(data);
          });
  }

  function logout() {
      fetch('/api/logout')
          .then(response => {
              setIsLoggedIn(false);
          });
      return <Redirect to="/home"/>
  }

  function checkLoggedIn() {
      return isLoggedIn;
  };

  React.useEffect(() => {
      if (user != null && user.user != null) {
          if (userSaved == null) {
              localStorage.clear();
              setUserSaved(user);
          } else if (userSaved.user != null && userSaved.user.id !== user.user.id)
              localStorage.clear();
      }
  }, [user]);

    React.useEffect(() => {
        getUser()
    }, [isLoggedIn]);

    React.useEffect(() => {
        getUser()
    }, []);

    return (
    <BrowserRouter history={hist}>
      <Switch>
        <Route
            path="/(home|login|register|forgotten-password)"
            render={(props) => <Home {...props}
                                     isLoggedIn={isLoggedIn}
                                     setIsLoggedIn={setIsLoggedIn}
                                     setUser={setUser}
                                     updateUser={refreshUser}

            />}
        />
        <PrivateRoute
            path="/dashboard"
            isLoggedIn={checkLoggedIn}
            isLoading={isLoading}
            logout={logout}
            component={Dashboard}
            user={user}
            updateUser={refreshUser}
            />}
        />
        <Redirect from="/" to="/home" />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
