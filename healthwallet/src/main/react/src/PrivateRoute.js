// This is used to determine if a user is authenticated and
// if they are allowed to visit the page they navigated to.

// If they are: they proceed to the page
// If not: they are redirected to the login page.
import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import Home from "./layouts/Home";
import ReactLoading from "react-loading";

const PrivateRoute = ({ component: Component, isLoggedIn, isLoading, ...rest }) => {

    const loggedIn = isLoggedIn();

    return (

        <div>
            {isLoading ?
                <div style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: '100vh'
                }}>
                    <ReactLoading type="spinningBubbles" color="gray" height={'5%'} width={'5%'} />
                </div> :
                <Route
                    {...rest}
                    render={props =>
                        !loggedIn ?
                            (
                                <Redirect to='/login' />
                            ) : (
                                <Component
                                    {...rest} {...props} />)
                    }
                />
            }
        </div>
    )
}

export default PrivateRoute