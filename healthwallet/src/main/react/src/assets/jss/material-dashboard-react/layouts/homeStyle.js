import {
  drawerWidth,
  transition,
  container
} from "assets/jss/material-dashboard-react.js";

const appStyle = theme => ({
  wrapper: {
    position: "relative",
    top: "0",
    height: "100vh",
  },
  mainPanel: {
    overflow: "auto",
    position: "absolute",
    float: "center",
    ...transition,
    width: "100vw",
    minHeight : '100vh'
  },
  centerPanel: {
    display: 'flex',
    minHeight : 'calc(100vh - 80px)',
    paddingTop: 0,
    paddingLeft: 50,
    paddingRight: 50,
    paddingBottom: 10,
    maxWidth: '100vw'
  },
  centerBlock: {
    paddingTop: '55px',
    paddingBottom: '70px',
    margin: 'auto',
    maxWidth: '100vw'
  },
  content: {
    marginTop: "70px",
    padding: "30px 15px",
    minHeight: "calc(100vh - 100px)"
  },
  card: {
    minWidth: '250px'
  },
  header: {
    height: '80px',
    width: '100vw',
    position: 'absolute',
    top:0,
    paddingRight: '100px'
  },
  footer: {
    width: '100vw',
    position: 'absolute',
    bottom:0
  },
  container,
  map: {
    marginTop: "70px"
  },
  image: {
    flex: 1,
    maxheight: 'calc(40vh)',
    maxWidth: 'calc(100vw - 100px)',
    resizeMode: 'contain'
  }
});

export default appStyle;
