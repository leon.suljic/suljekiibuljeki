/*!

=========================================================
* Material PatientDashboard React - v1.8.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import People from "@material-ui/icons/People";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import BubbleChart from "@material-ui/icons/BubbleChart";
import LocationOn from "@material-ui/icons/LocationOn";
import Notifications from "@material-ui/icons/Notifications";
import CloudUpload from "@material-ui/icons/CloudUpload";
import Unarchive from "@material-ui/icons/Unarchive";
import Language from "@material-ui/icons/Language";
import BusinessIcon from "@material-ui/icons/Business";
// core components/views for Admin layout
import DashboardPage from "views/Dashboard/HospitalDashboard.js";
import PatientProfile from "views/UserProfile/PatientProfile.js";
import DoctorsListView from "./views/TableList/DoctorsListHospitalView.js";
import HospitalProfile from "./views/UserProfile/HospitalProfile";

const dashboardRoutes = [
  {
    path: "/dash",
    name: "Dashboard",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/dashboard"
  },
  {
    path: "/doctors",
    name: "Doktori",
    icon: People,
    component: DoctorsListView,
    layout: "/dashboard"
  },
  {
    path: "/user",
    name: "Bolnica",
    icon: BusinessIcon,
    component: HospitalProfile,
    layout: "/dashboard"
  }
];

export default dashboardRoutes;
