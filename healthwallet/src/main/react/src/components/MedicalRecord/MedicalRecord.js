import React from "react";
import {PDFReader, MobilePDFReader} from "react-read-pdf";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridContainer from "components/Grid/GridContainer.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";

import ReactLoading from "react-loading";
import AlertDialog from "../Alert/Alert";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};



const useStyles = makeStyles(styles);

export default function MedicalRecord(props) {

  const classes = useStyles();

  const {
    file,
    onClose,
    onDelete,
    editRestriction,
      downloadLink,
      fileName,
    ...rest
  } = props;

  const [isLoading, setIsLoading] = React.useState(true);
  const [isAlertOpen, onSetIsAlertOpen] = React.useState(false);

  const [invalidPdf, setInvalidPdf] = React.useState(false);

  function onLoad(totalPage,title,otherObj) {
    setIsLoading(false); // doesn't work
  }

  React.useEffect(() => {
      if (fileName != null && !fileName.endsWith('.pdf'))
          setInvalidPdf(true);
  }, [file, fileName]);

  function downloadFile(){
    fetch(downloadLink)
        .then(response => {
          response.blob()
              .then(blob => {
                let url = window.URL.createObjectURL(blob);
                let a = document.createElement('a');
                a.href = url;
                a.target = '_blank';
                a.download = fileName;
                a.click();})
        });
  }

    const deleteAlert = () => {
        return (
            <AlertDialog
                title='Obiši nalaz'
                message='Obrisani nalaz bit će nepovratno izgubljen'
                open={isAlertOpen}
                setOpen={onSetIsAlertOpen}
                onOk={onDelete}
                onCancel={() => {}}
                okButton='Obriši'
                cancelButton='Odustani'
                okButtonColor='rose'
            />
        );
    };

  return (

      <Card medicalRecord>
          {deleteAlert()}
        <CardHeader color="primary">
          <div style={{
            flexDirection: "row",
            display: "flex",
          }}>
          <h4 className={classes.cardTitleWhite}
          style={{
            verticalAlign: "middle"
          }}>Nalaz</h4>

          </div>
        </CardHeader>
        <CardBody>
            <div>
            { onClose != null ?
                <Button
                    onClick={onClose}
                >Zatvori</Button> : null }
            { onClose != null && !isLoading ?
                <Button
                    onClick={downloadFile}
                >Preuzmi</Button> : null }
            { onDelete != null && !editRestriction() ?
                <Button style={{
                    float: 'right'
                }}
                    color='rose'
                    onClick={() => {onSetIsAlertOpen(true)}}
                >Obriši</Button> : null }
            </div>
          <div>
            <GridContainer>

              </GridContainer>
          </div>
        <div>
        { isLoading ?
            <div style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              height: '100vh'
            }}>
              <ReactLoading type="spinningBubbles" color="gray" height={'5%'} width={'5%'} />
            </div>
        : null}
        </div>

        <div style={{
          position: 'relative',
          minWidth: '30vw',
          maxWidth: '90vw',
          minHeight: '80vh',
          maxHeight: '90vh'
        }}>
          { file !== null && !invalidPdf ?
              <div>
                  { isLoading ?
                    <PDFReader
                        url={file}
                        onDocumentComplete={onLoad}
                    /> : null}
                  <MobilePDFReader
                      showAllPage={true}
                      isShowHeader={false}
                      isShowFooter={false}
                      scale='auto'
                      url={file}
                  />
              </div>: null
          }
        </div>
            {invalidPdf ?
                <h2>Dogodila se greška prilikom prikazivanja nalaza</h2> : null
            }
        </CardBody>
      </Card>
 );
}


