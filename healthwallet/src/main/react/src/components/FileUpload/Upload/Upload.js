import React, { Component } from "react";

import Dropzone from "../Dropzone/Dropzone";
import "./Upload.css";
import Progress from "../Progress/Progress";
import Button from "components/CustomButtons/Button.js";

import successIcon from "assets/icons/successIcon.svg";
import errorIcon from "assets/icons/errorIcon.svg";

import GridContainer from "../../Grid/GridContainer";
import GridItem from "../../Grid/GridItem";

import CustomInput from "../../CustomInput/CustomInput";
import DatePicker from 'react-datepicker'
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import hr from 'date-fns/locale/hr';
import "react-datepicker/dist/react-datepicker.css";
import 'react-day-picker/lib/style.css';

import dateFnsFormat from 'date-fns/format';


import {
  grayColor,
} from "assets/jss/material-dashboard-react.js";


class Upload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: null,
      date: new Date(),
      department: null,
      doctor: null,
      hospital: '',
      diagnosis: null,
      uploading: false,
      uploadProgress: {},
      successfullUploaded: false
    };
    registerLocale('hr', hr) //TODO PICKER
    this.onFilesAdded = this.onFilesAdded.bind(this);
    this.uploadFiles = this.uploadFiles.bind(this);
    this.sendRequest = this.sendRequest.bind(this);
    this.renderActions = this.renderActions.bind(this);
    this.onChange = this.onChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.setMedFile = this.props.setMedFile;
    this.onWrongFormat = this.props.onWrongFormat;
  }

  onFilesAdded(files) {

    this.setState(prevState => ({
      files: prevState.file = files[0]
    }));

    this.props.setMedFile(files[0]);
  }

   onChange(event) {
    const {name, value} = event.target;
    this.setState({ [name]: value,});
  }

  handleDateChange(date) {
    this.setState({ date: date });
  }

  async uploadFile() {

    this.setState({ uploadProgress: {}, uploading: true });

    if (this.state.file !== null) {

      const file = this.state.file;

      this.sendRequest(file);
    }
  }

  async uploadFiles() {
    this.setState({ uploadProgress: {}, uploading: true });
    const promises = [];
    this.state.files.forEach(file => {
      promises.push(this.sendRequest(file));
    });
    try {
      await Promise.all(promises);

      this.setState({ successfullUploaded: true, uploading: false });
    } catch (e) {
      // Not Production ready! Do some error handling here instead...
      this.setState({ successfullUploaded: true, uploading: false });
    }
  }

  sendRequest(file) {
    return new Promise((resolve, reject) => {

      const isDoctor = this.props.user.role === 'doctor';

      const req = new XMLHttpRequest();

      const formData = new FormData();
      formData.append('file', file);
      formData.append('date', this.state.date);
      formData.append('departmentName', this.state.department);
      formData.append('diagnosis', this.state.diagnosis);

      if (!isDoctor) {
        formData.append('doctor', this.state.doctor);
        formData.append('hospital', this.state.hospital);
      }


      // req.upload.addEventListener("progress", event => {
      //   if (event.lengthComputable) {
      //     const copy = { ...this.state.uploadProgress };
      //     copy[file.name] = {
      //       state: "pending",
      //       percentage: (event.loaded / event.total) * 100
      //     };
      //     this.setState({ uploadProgress: copy });
      //   }
      // });

      // req.upload.addEventListener("load", event => {
      //   const copy = { ...this.state.uploadProgress };
      //   copy[file.name] = { state: "done", percentage: 100 };
      //   this.setState({ uploadProgress: copy });
      //   resolve(req.response);
      // });

      req.upload.addEventListener("error", event => {
        const copy = { ...this.state.uploadProgress };
        copy[file.name] = { state: "error", percentage: 0 };
        this.setState({ uploadProgress: copy });
        reject(req.response);
      });


      req.onreadystatechange = (e) => {
        if (req.readyState === 4) {
          this.uploadCompete()
        }
      };

      let path = '/patients/medrecords';
      if (isDoctor && this.props.patient != null)
        path = '/doctors/' + this.props.patient.id + '/records';
      path = "/api" + path + "/upload";
      if (this.props.user.role === 'patient' && this.props.patient != null)
        path = path + '/?patientEmail=' + this.props.patient.email;

      req.open("POST", path);
      req.send(formData);
    });
  }

  uploadCompete(e) {
    const copy = this.state.uploadProgress;
    copy[this.state.file.name] = {state: "done", percentage: 100};
    this.setState({successfullUploaded: true, uploadProgress: copy});
  }

  renderProgress(file) {
    const uploadProgress = this.state.uploadProgress[file.name];
    if (this.state.uploading || this.state.successfullUploaded) {
      return (
        <div className="ProgressWrapper">
          <Progress progress={uploadProgress ? uploadProgress.percentage : 0} />
          <img
            className="CheckIcon"
            alt="done"
            src={successIcon}
            style={{
              opacity:
                uploadProgress && uploadProgress.state === "done" ? 0.5 : 0
            }}
          />
        </div>
      );
    }
  }

  renderActions() {
    return (
      <div>
        {this.state.file != null && !this.state.successfullUploaded && !this.state.uploading?
            <Button
                onClick={() => {
                  this.setState({file: null, files: [], successfullUploaded: false});
                  this.setMedFile(null);
                  window.scrollTo(0, 0);
                }}
                disabled={this.state.uploading}
            >
              Poništi
            </Button> : null}
        {this.state.file != null && this.state.successfullUploaded?
            <Button
                onClick={() => {
                  this.setMedFile(null);
                  this.setState({
                    file: null,
                    date: new Date(),
                    department: null,
                    doctor: null,
                    hospital: '',
                    diagnosis: null,
                    uploading: false,
                    uploadProgress: {},
                    successfullUploaded: false
                  });
                  window.scrollTo(0, 0);
                }}
            >
              Dodaj novi nalaz
            </Button> : null}
        {!this.state.successfullUploaded ?
            <Button
                color="primary"
                type="submit"
                disabled={this.state.file === null || this.state.uploading}
                onClick={() => this.uploadFile()}
            >
              Dodaj
            </Button>
        : null}
      </div>
    );
  };

  readFileDataAsBase64(e) {
    const file = e;

    return new Promise((resolve, reject) => {
      const reader = new FileReader();

      reader.onload = (event) => {
        resolve(event.target.result);
      };

      reader.onerror = (err) => {
        reject(err);
      };

      reader.readAsDataURL(file);
    });
  }

  formatDate(date, format, locale) {
    return dateFnsFormat(date, format, { locale });
  }

  FORMAT = 'MM/dd/yyyy';

  medRecordData() {

    const isDoctor = this.props.user.role === 'doctor';

    return (<div>
      <GridContainer>
        { !isDoctor ?
        <GridItem xs={12} sm={9} md={6}>
          <CustomInput
              labelText="Bolnica"
              id="hosiptal"
              formControlProps={{
                fullWidth: true
              }}
              name='hospital'
              onChange={this.onChange}
          />
        </GridItem> : null}

        <GridItem xs={12} sm={9} md={6}>
          <CustomInput
              labelText="Odjel"
              id="department"
              formControlProps={{
                fullWidth: true
              }}
              name='department'
              onChange={this.onChange}
          />
        </GridItem>
      </GridContainer>
      { !isDoctor ?
      <GridContainer>
        <GridItem xs={12} sm={12} md={9}>
          <CustomInput
              labelText="Ime doktora"
              id="doctor-name"
              formControlProps={{
                fullWidth: true
              }}
              name='doctor'
              onChange={this.onChange}
          />
        </GridItem>
      </GridContainer> : null }
      <GridContainer>
        <GridItem xs={12} sm={6} md={3}>
          <div>
            <p style={{
              fontSize: '11px',
              marginTop: '23px',
              lineHeight: '16px',
              color: grayColor[1]
            }}>Datum</p>
            <DatePicker
                dateFormat="dd.MM.yyyy."
                selected={this.state.date}
                onChange={date => this.handleDateChange(date)}
                locale="hr"
            />
          </div>
        </GridItem>
        <GridItem xs={12} sm={12} md={9}>
          <CustomInput
              labelText="Dijagnoze"
              id="diagnosis"
              formControlProps={{
                fullWidth: true
              }}
              name='diagnosis'
              onChange={this.onChange}
          />
        </GridItem>
      </GridContainer>
    </div>);
  }

  render() {
    return (
        <div>
          { this.state.file !== null ? this.medRecordData() : null }
          <div className="Upload">
            <div className="Content">
              <div>
                {this.state.file === null ?
                  <Dropzone
                    onFilesAdded={this.onFilesAdded}
                    onWrongFormat={this.onWrongFormat}
                    disabled={this.state.uploading || this.state.successfullUploaded}
                  /> : null

                }
              </div>
              <div className="Files">
                { this.state.file !== null ?
                    <div key={this.state.file.name} className="Row">
                      <span className="Filename">{this.state.file.name}</span>
                      {this.renderProgress(this.state.file)}
                    </div> : null
                }
              </div>
            </div>
            <div className="Actions">{this.renderActions()}</div>
          </div>
        </div>
    );
  }
}

export default Upload;
