import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from "components/CustomButtons/Button.js";

export default function AlertDialog(props) {

    const {
        title,
        message,
        open,
        setOpen,
        onOk,
        onCancel,
        okButton,
        cancelButton,
        okButtonColor,
        ...rest
    } = props;

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleCloseOk = () => {
        setOpen(false);
        onOk();
    };
    const handleCloseCancel = () => {
        onCancel();
        setOpen(false);
    };

    return (
        <div>
            <Dialog
                open={open}
                onClose={handleCloseCancel}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {message}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    {cancelButton != null ?
                    <Button onClick={handleCloseCancel} color="secondary">
                        {cancelButton}
                    </Button> : null }
                    <Button onClick={handleCloseOk} color={okButtonColor} autoFocus>
                        {okButton}
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}