import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import avatar from "assets/img/faces/marc.jpg";

const styles = {
  loginCardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  loginCardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function ResetPasswordCard(props) {
  const classes = useStyles();

  const {
    onLogin,
    onLogout
  } = props;

  const [loginForm, setLoginForm] = React.useState({ username: '', code: '', newPassword: ''});
  const [error, setError] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const [unknownError, setUnknownError] = React.useState(false);

  const [sentToMail, setSentToMail] = React.useState(false);
  const [isSending, setIsSending] = React.useState(false);

  function onChange(event) {
    const {name, value} = event.target;
    setError(false);
    setLoginForm(oldForm => ({...oldForm, [name]: value}))
  }

  function login() {
    const body = `username=${loginForm.username}&password=${loginForm.newPassword}`;
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: body
    };
    fetch('/api/login', options)
        .then(response => {
          if (response.status === 401) {
            setError(true);
          } else if (response.status === 200) {
            setSuccess(true);
            onLogin();
          } else {
            setUnknownError(true);
          }
        });
  }

  function sendCode() {
    setIsSending(true);
    const options = {
      method: 'POST'
    };

    fetch('api/forgotten-password/send-code/?email=' + loginForm.username, options)
        .then(response => {
          setIsSending(false);
          if (response.status === 200) {
            setSentToMail(true);
          } else {
            setError(true);
          }
        });
  }

  function changePassword() {

    setIsSending(true);
    const options = {
      method: 'POST'
    };

    fetch('api/forgotten-password/reset-password/?email=' + loginForm.username +
        '&passcode=' + loginForm.code + '&newpassword=' + loginForm.newPassword, options)
        .then(response => {
          setIsSending(false);
          if (response.status === 200) {
            setSuccess(true);
            login();
          } else {
            setError(true);
          }
        });
  }

  function onSubmitLogout(e) {
    e.preventDefault();
    setError("");
    const body = `username=${loginForm.username}&password=${loginForm.password}`;
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: body
    };
    fetch('/logout', options)
        .then(response => {
          onLogout();

        });
  }

  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.loginCardTitleWhite}>Resetiranje lozinke</h4>
            </CardHeader>
            <CardBody>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    labelText="Email"
                    id="email-address"
                    formControlProps={{
                      fullWidth: true
                    }}
                    name='username'
                    success={success ? success : null}
                    error={error ? error : null}
                    onChange={onChange}
                    value={loginForm.username}
                  />
                </GridItem>
              </GridContainer>
              {sentToMail ?
              <div>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={6}>
                    <CustomInput
                        labelText="6-znamenkasti kod"
                        id="code"
                        formControlProps={{
                          fullWidth: true
                        }}
                        name='code'
                        success={success ? success : null}
                        error={error ? error : null}
                        onChange={onChange}
                        value={loginForm.username}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <CustomInput
                      labelText="Lozinka (min. 8 znakova)"
                      id="newPassword"
                      formControlProps={{
                        fullWidth: true
                      }}
                      success={success ? success : null}
                      error={error ? error : null}
                      type="password"
                      name='newPassword'
                      onChange={onChange}
                      value={loginForm.password}
                    />
                  </GridItem>
                </GridContainer>
              </div> : null }
            </CardBody>
            {!sentToMail ?
              <CardFooter>
                <Button
                    color="primary"
                    type="submit"
                    onClick={sendCode}
                    disabled={isSending}
                >Pošalji kod</Button>
                { !isSending ?
                <a href='#'
                   className={classes.a}
                   onClick={() => {
                     setError(false);
                     setSentToMail(true);
                   }}
                >
                  Već imam kod
                </a> : null }
              </CardFooter> :
                <CardFooter>
                  <Button
                      color="primary"
                      type="submit"
                      onClick={changePassword}
                  >Prijava</Button>
                  <Button
                      color="secondary"
                      type="submit"
                      onClick={sendCode}
                      disabled={isSending}
                  >Pošalji kod ponovno</Button>
                </CardFooter>}
          </Card>
        </GridItem>
        {unknownError ?
          <GridItem xs={12} sm={12} md={12}>
            <span>
              Dogodila se greška
            </span>
          </GridItem> : null}
      </GridContainer>
    </div>
  );
}
