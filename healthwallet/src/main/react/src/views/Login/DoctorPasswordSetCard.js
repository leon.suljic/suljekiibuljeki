import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import avatar from "assets/img/faces/marc.jpg";

const styles = {
  loginCardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  loginCardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function DoctorPasswordCard(props) {

  const {
    onLogin,
      docCode
  } = props;

  const classes = useStyles();

  const [data, setData] = React.useState({ email: '', password: ''});
  const [error, setError] = React.useState(false);
  const [success, setSuccess] = React.useState(false);

  function onChange(event) {
    const {name, value} = event.target;
    setError(false);
    setData(oldForm => ({...oldForm, [name]: value}))
  }

  function onSubmit(e) {
    e.preventDefault();
    setError("");

    const options = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    };
    if(data.password === data.password2) {
      fetch('/api/doctors/register/' + docCode, options)
          .then(response => {
            if (response.status === 200) {
              setSuccess(true);
              loginAsNewUser();
            } else {
              setError(true);
            }
          });
    } else {
      setError(true);
    }
  }

  function loginAsNewUser() {
    const body = `username=${data.email}&password=${data.password}`;
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: body
    };
    fetch('/api/login', options)
        .then(response => {
          if (response.status === 401) {
            setError(true);
          } else if (response.status === 200) {
            setSuccess(true);
            onLogin();
          }
        });
  }

  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12}>
          <Card>
            <CardHeader color="primary">
              <h3 className={classes.loginCardTitleWhite}>Doktor</h3>
              <h4 className={classes.loginCardTitleWhite}>Postavljanje lozinke</h4>
            </CardHeader>
            <CardBody>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    labelText="Ponovite email"
                    id="email-address"
                    formControlProps={{
                      fullWidth: true
                    }}
                    name='email'
                    success={success ? success : null}
                    error={error ? error : null}
                    onChange={onChange}
                    value={data.email}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                <CustomInput
                    labelText="Postavite lozinku (min. 8 znakova)"
                    id="password"
                    formControlProps={{
                      fullWidth: true
                    }}
                    success={success ? success : null}
                    error={error ? error : null}
                    type="password"
                    name='password'
                    onChange={onChange}
                />
              </GridItem>

                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                      labelText="Ponovite lozinku"
                      id="password2"
                      formControlProps={{
                        fullWidth: true
                      }}
                      success={success ? success : null}
                      error={error ? error : null}
                      type="password"
                      name='password2'
                      onChange={onChange}
                  />
                </GridItem>

              </GridContainer>
            </CardBody>
            <CardFooter>
              <Button
                  color="primary"
                  type="submit"
                  onClick={onSubmit}
              >Registracija</Button>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}
