import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import avatar from "assets/img/faces/marc.jpg";

const styles = {
  loginCardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  loginCardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function LoginCard(props) {
  const classes = useStyles();

  const {
    onChangeType,
      onLogin
  } = props;

  const [data, setData] = React.useState({ email: '', password: '', name: '', oib: '', address : '', zipCode: ''});
  const [errors, setErrors] = React.useState([]);
  const [success, setSuccess] = React.useState(false);

  function onChange(event) {
    const {id, value} = event.target;
    removeError(id);
    setSuccess(false);
    setData(oldForm => ({...oldForm, [id]: value}))
  }

  function removeError(err) {
    setErrors(e => {
      const index = e.indexOf(err);
      if (index > -1)
        e.splice(index, 1);
      return e;
    });
  }

  function onSubmit(e) {
    e.preventDefault();

    const options = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    };
    fetch('/api/hospitals/register', options)
        .then(response => {
          if (response.status >= 200 && response.status < 300) {
            setSuccess(true);
            loginAsNewUser();
            return [];
          } else if (response.status >= 400 && response.status < 500){
            return response.json();
          }
          return [];
        })
        .then(data => {
          setErrors(data);
        });
  }

  function loginAsNewUser() {
    const body = `username=${data.email}&password=${data.password}`;
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: body
    };
    fetch('/api/login', options)
        .then(response => {
          if (response.status === 401) {
            setErrors(['email', 'password']);
          } else if (response.status === 200) {
            setSuccess(true);
            onLogin();
          }
        });
  }

  return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12}>
            <Card>
              <CardHeader color="primary">
                <h3 className={classes.loginCardTitleWhite}>Registracija</h3>
                <h4 className={classes.loginCardTitleWhite}>Bolnica</h4>
              </CardHeader>
              <CardBody>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <CustomInput
                        labelText="Ime bolnice"
                        id="name"
                        formControlProps={{
                          fullWidth: true
                        }}
                        name='name'
                        onChange={onChange}
                        success={success}
                        error={errors.indexOf('name') > -1}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <CustomInput
                        labelText="OIB bolnice (11 znamenki)"
                        id="oib"
                        formControlProps={{
                          fullWidth: true
                        }}
                        success={success}
                        error={errors.indexOf('oib') > -1}
                        name='oib'
                        onChange={onChange}

                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <CustomInput
                        labelText="Email"
                        id="email"
                        formControlProps={{
                          fullWidth: true
                        }}
                        name='email'
                        success={success}
                        error={errors.indexOf('email') > -1}
                        onChange={onChange}
                        value={data.username}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <CustomInput
                        labelText="Lozinka (min. 8 znakova)"
                        id="password"
                        formControlProps={{
                          fullWidth: true
                        }}
                        success={success}
                        error={errors.indexOf('password') > -1}
                        type="password"
                        name='password'
                        onChange={onChange}
                        value={data.password}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <CustomInput
                        labelText="Adresa"
                        id="address"
                        formControlProps={{
                          fullWidth: true
                        }}
                        success={success}
                        error={errors.indexOf('address') > -1}
                        name='address'
                        onChange={onChange}
                        value={data.password}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <CustomInput
                        labelText="Poštanski broj (5 znamenki)"
                        id="zipCode"
                        formControlProps={{
                          fullWidth: true
                        }}
                        success={success}
                        error={errors.indexOf('zipCode') > -1}
                        name='zipCode'
                        onChange={onChange}
                        value={data.password}
                    />
                  </GridItem>
                </GridContainer>
              </CardBody>
              <CardFooter>
                <Button
                    color="primary"
                    type="submit"
                    onClick={onSubmit}
                >Registracija</Button>
              </CardFooter>


            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={12}>

          <span>
            Niste bolnica?
            {" "}
            <a href='#'
                className={classes.a}
                onClick={onChangeType}
            >
              Registrirajte se kao pacijent
            </a>
          </span>
          </GridItem>
        </GridContainer>
      </div>
  );
}
