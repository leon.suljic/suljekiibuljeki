import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import avatar from "assets/img/faces/marc.jpg";

const styles = {
  loginCardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  loginCardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function DoctorRegistrationCard(props) {

  const {
    onCreateDoctor,
    onCancel
  } = props;

  const classes = useStyles();

  const [data, setData] = React.useState({ email: '', firstName: '', lastName: '', oib: '', licenceNumber: '', speciality: '', birthDate: '', sex: 'OTHER', address: '', zipCode: '00000'});
  const [errors, setErrors] = React.useState([]);
  const [success, setSuccess] = React.useState(false);

  function onChange(event) {
    const {id, value} = event.target;
    setData(oldForm => ({...oldForm, [id]: value}));
    removeError(id);
    setSuccess(false);
  }

  function removeError(err) {
    setErrors(e => {
      const index = e.indexOf(err);
      if (index > -1)
        e.splice(index, 1);
      return e;
    });
  }

  function onSubmit(e) {
    e.preventDefault();

    const options = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    };
    fetch('/api/hospitals/add-doctor', options)
        .then(response => {
            if (response.status >= 200 && response.status < 300) {
              onCreateDoctor();
              setSuccess(true);
              return [];
            }
            return response.json();
        })
        .then(data => {
          setErrors(data);
        });

  }

  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.loginCardTitleWhite}>Novi doktor</h4>
            </CardHeader>
            <CardBody>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    labelText="Email"
                    id="email"
                    formControlProps={{
                      fullWidth: true
                    }}
                    name='email'
                    success={success}
                    error={errors.indexOf('email') > -1}
                    onChange={onChange}
                    value={data.email}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={6} sm={6} md={6}>
                  <CustomInput
                      labelText="Ime"
                      id="firstName"
                      formControlProps={{
                        fullWidth: true
                      }}
                      name='firstName'
                      onChange={onChange}
                      success={success}
                      error={errors.indexOf('firstName') > -1}
                  />

                </GridItem>
                <GridItem xs={6} sm={6} md={6}>
                  <CustomInput
                      labelText="Prezime"
                      id="lastName"
                      formControlProps={{
                        fullWidth: true
                      }}
                      name='lastName'
                      onChange={onChange}
                      success={success}
                      error={errors.indexOf('lastName') > -1}
                  />

                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={6} sm={6} md={6}>
                  <CustomInput
                      labelText="OIB (11 znamenki)"
                      id="oib"
                      formControlProps={{
                        fullWidth: true
                      }}
                      name='oib'
                      onChange={onChange}
                      success={success}
                      error={errors.indexOf('oib') > -1}
                  />

                </GridItem>
                <GridItem xs={6} sm={6} md={6}>
                  <CustomInput
                      labelText="Broj licence"
                      id="licenceNumber"
                      formControlProps={{
                        fullWidth: true
                      }}
                      name='licenceNumber'
                      onChange={onChange}
                      success={success}
                      error={errors.indexOf('licenceNumber') > -1}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                      labelText="Specijalnost"
                      id="speciality"
                      formControlProps={{
                        fullWidth: true
                      }}
                      name='speciality'
                      onChange={onChange}
                      success={success}
                      error={errors.indexOf('speciality') > -1}
                  />
                </GridItem>
              </GridContainer>
            </CardBody>
            <CardFooter>
              <Button
                  color="primary"
                  type="submit"
                  onClick={onSubmit}
              >Registracija</Button>
              <Button
                  color="secondary"
                  type="cancel"
                  onClick={onCancel}
              >Odustani</Button>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}
