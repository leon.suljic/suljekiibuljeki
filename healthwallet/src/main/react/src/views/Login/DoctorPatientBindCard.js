import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import avatar from "assets/img/faces/marc.jpg";

const styles = {
  loginCardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  loginCardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function DoctorPatientBindCard(props) {

  const {
    onBindPatient,
    onCancel
  } = props;

  const classes = useStyles();

  const [data, setData] = React.useState({ patientOIB: '', patientPIN: ''});
  const [error, setError] = React.useState(false);
  const [success, setSuccess] = React.useState(false);

  function onChange(event) {
    const {name, value} = event.target;
    setError(false);
    setData(oldForm => ({...oldForm, [name]: value}))
  }

  function onSubmit(e) {
    e.preventDefault();
    setError("");

    const options = {
      method: 'POST'
    };

    fetch('/api/doctors/add-patient/?patientOIB='
        +data.patientOIB+'&patientPIN='+data.patientPIN, options)
        .then(response => {
          if (response.status === 200) {
            setSuccess(true);
            return response.json();
          } else {
            setError(true);
          }
          return null;
        }).then(data => {
          if (data != null)
            onBindPatient(data);
        });
  }

  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.loginCardTitleWhite}>Dodaj pacijenta</h4>
            </CardHeader>
            <CardBody>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                      labelText="OIB"
                      id="patientOIB"
                      formControlProps={{
                        fullWidth: true
                      }}
                      name='patientOIB'
                      onChange={onChange}
                      success={success ? success : null}
                      error={error ? error : null}
                  />

                </GridItem>
                <GridItem xs={6} sm={6} md={6}>
                  <CustomInput
                      labelText="PIN"
                      id="patientPIN"
                      formControlProps={{
                        fullWidth: true
                      }}
                      name='patientPIN'
                      onChange={onChange}
                      success={success ? success : null}
                      error={error ? error : null}
                  />
                </GridItem>
              </GridContainer>

            </CardBody>
            <CardFooter>
              <Button
                  color="primary"
                  type="submit"
                  onClick={onSubmit}
              >Dodaj</Button>
              <Button
                  color="secondary"
                  type="cancel"
                  onClick={onCancel}
              >Odustani</Button>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}
