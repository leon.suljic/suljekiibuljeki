import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import avatar from "assets/img/faces/marc.jpg";

const styles = {
  loginCardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  loginCardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function PatientRegistrationCard(props) {

  const {
    onChangeType,
    onLogin
  } = props;

  const classes = useStyles();

  const [data, setData] = React.useState({ email: '', password: '', password2: '', firstName: '', lastName: ''});
  const [errors, setErrors] = React.useState([]);
  const [success, setLoggedIn] = React.useState(false);

  const [passConf, setPassConf] = React.useState('');

  function onSubmit(e) {
    e.preventDefault();

    if (data.password !== data.password2) {
      const index = errors.indexOf('password');
      if (index < 0) {
      setErrors(e => e.concat('password'));}
      return;
    }

    const options = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    };
    fetch('/api/patients/register', options)
        .then(response => {
          if (response.status >= 200 && response.status < 300) {
            loginAsNewUser();
            return [];
          }
          return response.json();
        })
        .then(data => {
          setErrors(data);
        });
  }

  function onChange(event) {
    const {id, value} = event.target;
    setData(oldForm => ({...oldForm, [id]: value}));
    removeError(id);
  }

  function removeError(err) {
    setErrors(e => {
      const index = e.indexOf(err);
      if (index > -1)
        e.splice(index, 1);
      return e;
    });
  }

  function loginAsNewUser() {
    const res = props.loginAsUser(data.email, data.password, evaluateLogin);
  }

  function evaluateLogin(res) {
    if (res) {
      setLoggedIn(true);
      onLogin();
    } else {
      setErrors(['email', 'password']);
    }
  }

  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12}>
          <Card>
            <CardHeader color="primary">
              <h3 className={classes.loginCardTitleWhite}>Registracija</h3>
              <h4 className={classes.loginCardTitleWhite}>Pacijent</h4>
            </CardHeader>
            <CardBody>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    labelText="Email"
                    id="email"
                    formControlProps={{
                      fullWidth: true
                    }}
                    name='email'
                    success={success}
                    error={errors.indexOf('email') > -1}
                    onChange={onChange}
                    value={data.email}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={10} sm={5} md={6}>
                  <CustomInput
                      labelText="Ime"
                      id="firstName"
                      formControlProps={{
                        fullWidth: true
                      }}
                      onChange={onChange}
                      success={success}
                      error={errors.indexOf('firstName') > -1}
                  />
                </GridItem>
                <GridItem xs={10} sm={5} md={6}>
                  <CustomInput
                      labelText="Prezime"
                      id="lastName"
                      formControlProps={{
                        fullWidth: true
                      }}
                      onChange={onChange}
                      success={success}
                      error={errors.indexOf('lastName') > -1}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    labelText="Lozinka (min. 8 znakova)"
                    id="password"
                    formControlProps={{
                      fullWidth: true
                    }}
                    success={success}
                    error={errors.indexOf('password') > -1}
                    type="password"
                    name='password'
                    onChange={onChange}
                    value={data.password}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                      labelText="Ponovljena lozinka"
                      id="password2"
                      formControlProps={{
                        fullWidth: true
                      }}
                      success={success}
                      error={errors.indexOf('password') > -1}
                      type="password"
                      name='password2'
                      onChange={onChange}
                      value={data.password}
                  />
                </GridItem>
              </GridContainer>
            </CardBody>
            <CardFooter>
              <Button
                  color="primary"
                  type="submit"
                  onClick={onSubmit}
              >Registracija</Button>
            </CardFooter>


          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={12}>

          <span>
            Niste pacijent?
            {" "}
            <a href='#'
                className={classes.a}
                onClick={onChangeType}
            >
              Registrirajte se kao bolnica
            </a>
          </span>
        </GridItem>
      </GridContainer>
    </div>
  );
}
