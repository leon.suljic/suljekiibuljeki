import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import DatePicker from 'react-datepicker';
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import hr from 'date-fns/locale/hr';
import "react-datepicker/dist/react-datepicker.css";

import avatar from "assets/img/faces/marc.jpg";
import Select from "react-select";

import {
  grayColor,
} from "assets/jss/material-dashboard-react.js";

const styles = {
  loginCardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  loginCardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function PatientConfirmRegistrationCard(props) {

  const {
    onComplete,
    onCancel,
    patientCode
  } = props;

  const classes = useStyles();

  const [data, setData] = React.useState({ email: '', password: '', oib: '', address: '', zipCode: '', healthCardNumber: '', sex: '', birthDate: ''});
  const [errors, setErrors] = React.useState([]);
  const [success, setSuccess] = React.useState(false);

  registerLocale('hr', hr)

  function onChange(event) {
    const {id, value} = event.target;
    removeError(id);
    setSuccess(false);
    setData(oldForm => ({...oldForm, [id]: value}))
  }

  function handleDateChange(selectedDate) {
    setData(oldForm => ({...oldForm, birthDate: selectedDate}));
  }

  function removeError(err) {
    setErrors(e => {
      const index = e.indexOf(err);
      if (index > -1)
        e.splice(index, 1);
      return e;
    });
  }

  function onSubmit(e) {
    e.preventDefault();

    const options = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    };


    fetch('/api/patients/register/continue/' + patientCode, options)
        .then(response => {
          if (response.status >= 200 && response.status < 300) {
            setSuccess(true);
            onComplete();
            return [];
          } else if (response.status >= 400 && response.status < 500){
            return response.json();
          }
          return [];
        })
        .then(data => {
          setErrors(data);
        });
  }

  const setSex = (e) => {
    const {value, label} = e;
    onChange({target: {id: 'sex', value: value}});
  };

  const sexOptions = [
    { value: 'MALE', label: 'Muški' },
    { value: 'FEMALE', label: 'Ženski' },
    { value: 'OTHER', label: 'Ostalo' }
  ];

  const SexDropdown = () =>  {
    if (data === '')
      return ;
    let sex = sexOptions.find(e => e.value === data.sex);

    return (
        <div>
          <p style={{
            fontSize: '11px',
            marginTop: '23px',
            lineHeight: '5px',
            color: grayColor[1]
          }}>Spol</p>
          <Select
              options={sexOptions}
              onChange={setSex}
          />
        </div>);
  };

  const birthDatePicker = () => {
      return (
          <div style={{
            font: '20px'
          }}>
            <p style={{
              fontSize: '11px',
              marginTop: '23px',
              lineHeight: '10px',
              color: grayColor[1]
            }}>Datum rođenja</p>
            <DatePicker
                className="CustomInput"
                label="datum"
                dateFormat="dd.MM.yyyy."
                selected={data.birthDate !=null ? Date.parse(data.birthDate) :Date.parse('')}
                onChange={date => handleDateChange(date)}
                locale="hr"
                id="birthDate"
            />
          </div>
      );
  };

  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.loginCardTitleWhite}>Potvrda registracije</h4>
            </CardHeader>
            <CardBody>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    labelText="Ponovite email"
                    id="email"
                    formControlProps={{
                      fullWidth: true
                    }}
                    name='email'
                    success={success}
                    error={errors.indexOf('email') > -1}
                    onChange={onChange}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                      labelText="Ponovite lozinku"
                      id="password"
                      formControlProps={{
                        fullWidth: true
                      }}
                      success={success}
                      error={errors.indexOf('password') > -1}
                      type="password"
                      name='password'
                      onChange={onChange}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={6} sm={6} md={6}>
                  <CustomInput
                      labelText="OIB (11 znamenki)"
                      id="oib"
                      formControlProps={{
                        fullWidth: true
                      }}
                      name='oib'
                      onChange={onChange}
                      success={success}
                      error={errors.indexOf('oib') > -1}
                  />

                </GridItem>
                <GridItem xs={6} sm={6} md={6}>
                  {SexDropdown()}
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                      labelText="Adresa"
                      id="address"
                      formControlProps={{
                        fullWidth: true
                      }}
                      name='address'
                      onChange={onChange}
                      success={success}
                      error={errors.indexOf('address') > -1}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                      labelText="Poštanski broj (5 znamenki)"
                      id="zipCode"
                      formControlProps={{
                        fullWidth: true
                      }}
                      name='zipCode'
                      success={success}
                      error={errors.indexOf('zipCode') > -1}
                      onChange={onChange}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  {birthDatePicker()}
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                      labelText="Broj zdravstvene iskaznice (9 znamenki)"
                      id="healthCardNumber"
                      formControlProps={{
                        fullWidth: true
                      }}
                      name='healthCardNumber'
                      success={success}
                      error={errors.indexOf('healthCardNumber') > -1}
                      onChange={onChange}
                  />
                </GridItem>
              </GridContainer>
            </CardBody>
            <CardFooter>
              <Button
                  color="primary"
                  type="submit"
                  disabled={data.sex === '' || data.birthDate === ''}
                  onClick={onSubmit}
              >Potvrdi</Button>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}
