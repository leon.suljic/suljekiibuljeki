import React from "react";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import avatar from "assets/img/faces/marc.jpg";

const styles = {
  loginCardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  loginCardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function LoginCard(props) {
  const classes = useStyles();

  const {
    onLogin,
    onLogout,
    passwordReset
  } = props;

  const [loginForm, setLoginForm] = React.useState({ username: '', password: ''});
  const [error, setError] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const [unknownError, setUnknownError] = React.useState(false);


  function onChange(event) {
    const {name, value} = event.target;
    setError(false);
    setLoginForm(oldForm => ({...oldForm, [name]: value}))
  }

  function onSubmit(e) {
    e.preventDefault();
    setError("");
    const body = `username=${loginForm.username}&password=${loginForm.password}`;
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: body
    };
    fetch('/api/login', options)
        .then(response => {
          if (response.status === 401) {
            setError(true);
          } else if (response.status === 200) {
            setSuccess(true);
            onLogin();
          } else {
            setUnknownError(true);
          }
        });
  }

  function onSubmitLogout(e) {
    e.preventDefault();
    setError("");
    const body = `username=${loginForm.username}&password=${loginForm.password}`;
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: body
    };
    fetch('/logout', options)
        .then(response => {
          onLogout();

        });
  }

  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12}>
          <Card>
            <CardHeader color="primary">
              <h3 className={classes.loginCardTitleWhite}>Prijava</h3>
            </CardHeader>
            <CardBody>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    labelText="Email"
                    id="email-address"
                    formControlProps={{
                      fullWidth: true
                    }}
                    name='username'
                    success={success ? success : null}
                    error={error ? error : null}
                    onChange={onChange}
                    value={loginForm.username}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    labelText="Lozinka"
                    id="password"
                    formControlProps={{
                      fullWidth: true
                    }}
                    success={success ? success : null}
                    error={error ? error : null}
                    type="password"
                    name='password'
                    onChange={onChange}
                    value={loginForm.password}
                  />
                </GridItem>
              </GridContainer>
            </CardBody>
            <CardFooter>
              <Button
                  color="primary"
                  type="submit"
                  onClick={onSubmit}
              >Prijava</Button>
            </CardFooter>
          </Card>
          <span>
                Zaboravili ste lozinku?
            {" "}
            <a href='#'
                className={classes.a}
                onClick={passwordReset}
            >
                  Resetirajte lozinku
                </a>
              </span>
        </GridItem>
        {unknownError ?
          <GridItem xs={12} sm={12} md={12}>
            <span>
              Dogodila se greška
            </span>
          </GridItem> : null}
      </GridContainer>
    </div>
  );
}
