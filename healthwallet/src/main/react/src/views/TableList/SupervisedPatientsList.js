import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import Button from "components/CustomButtons/Button.js";
import DoctorPatientBindCard from "../Login/DoctorPatientBindCard";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const useStyles = makeStyles(styles);

export default function PatientsListView(props) {
  const classes = useStyles();

  const [patients, setPatients] = React.useState([]);
  const [patientsListRaw, setPatientsListRaw] = React.useState([]);
  const [isAddingPatient, setIsAddingPatient] = React.useState(false);

  const {
    patient,
    setPatient
  } = props;

  function getPatients() {
    fetch('/api/doctors/patients')
        .then(response => response.json())
        .then(data => {
          setPatientsListRaw(data);
          setCleanedPatients(data);
        });
  }

  function getPatient(patientId) {
    patientsListRaw.map(pat => {
      if (pat.id === patientId) {
        setPatient(pat);
        return;
      }
    });
  }

  function setCleanedPatients(patientsList) {
    const data = [];
    patientsList.map(doc => {
      data.push([doc.id, doc.firstName, doc.lastName, doc.email])
    });
    setPatients(data);
  }

  function addPatient() {
    setIsAddingPatient(false);
    getPatients();
  }

  React.useEffect(() => {
    getPatients();
  }, []);

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        {!isAddingPatient ?
            <Card list>
              <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>Doktori</h4>
              </CardHeader>
              <CardBody scrollable>
                <Button
                    onClick={() => setIsAddingPatient(true)}
                >Dodaj novog pacijenta</Button>
                <Table
                    tableHeaderColor="primary"
                    tableHead={["id", "Ime", "Prezime", "Email"]}
                    tableData={patients}
                    clickable={true}
                    onClick={(id) => {
                      getPatient(id);
                    }}
                    idIndex={0}
                />
              </CardBody>
            </Card>
            :
            <DoctorPatientBindCard
                onBindPatient={addPatient}
                onCancel={() => setIsAddingPatient(false)}
            />
        }
      </GridItem>
    </GridContainer>
  );
}
