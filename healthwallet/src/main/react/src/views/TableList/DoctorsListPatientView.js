import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import Button from "components/CustomButtons/Button.js";
import DoctorRegistrationCard from "../Login/DoctorRegisterCard";
import DoctorProfileCard from "../UserProfile/DoctorProfileInfo";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const useStyles = makeStyles(styles);

export default function DoctorsListView(props) {
  const classes = useStyles();

  const [doctors, setDoctors] = React.useState([]);
  const [doctorsListRaw, setDoctorsListRaw] = React.useState([]);
  const [isViewingDoctor, setIsViewingDoctor] = React.useState(false);
  const [currentDoctor, setCurrentDoctor] = React.useState(null);

  function getDoctors() {
    let url = '/api/patients/doctors';
    if (props.patient != null && props.user.role === 'patient')
      url = url + '/?email=' + props.patient.email;
    fetch(url)
        .then(response => {
          if (response.status >= 200 && response.status < 300)
            return response.json();
          else
            return [];
        })
        .then(data => {
          setDoctorsListRaw(data);
          setCleanedDoctors(data);
        });
  }

  function getDoctor(doctorEmail) {
    doctorsListRaw.map(doc => {
      if (doc.email === doctorEmail) {
        setCurrentDoctor(doc);
        setIsViewingDoctor(true);
        return;
      }
    });
  }

  function setCleanedDoctors(doctorsList) {
    const data = [];
    doctorsList.map(doc => {
      data.push([doc.firstName, doc.lastName, doc.email, doc.specialty, doc.hospital])
    });
    setDoctors(data);
  }



  React.useEffect(() => {
    getDoctors();
  }, []);

  const doctorCard = () => {
    return (
        <DoctorProfileCard
          doctor={currentDoctor}
          onBack={() => setIsViewingDoctor(false)}
          refreshList={getDoctors}
          {...props}
        />
    );
  }

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        {!isViewingDoctor ?
            <Card list>
              <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>Doktori</h4>
              </CardHeader>
              <CardBody scrollable>
                <Table
                    tableHeaderColor="primary"
                    tableHead={["Ime", "Prezime", "Email", "Specijalnost", "Bolnica"]}
                    tableData={doctors}
                    clickable={true}
                    onClick={(doctorEmail) => {
                      getDoctor(doctorEmail);
                    }}
                    idIndex={2}
                />
              </CardBody>
            </Card>
            :
            doctorCard()
        }
      </GridItem>
    </GridContainer>
  );
}
