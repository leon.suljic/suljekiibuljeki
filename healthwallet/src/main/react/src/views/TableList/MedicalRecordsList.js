import React from "react";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import MedicalRecord from "../../components/MedicalRecord/MedicalRecord";
import Button from "components/CustomButtons/Button.js";

import DatePicker from 'react-datepicker'
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import hr from 'date-fns/locale/hr';
import "react-datepicker/dist/react-datepicker.css";
import './DatePickerStyle.css';

import { parse } from 'date-fns';
import CreatableSelect from "react-select/creatable";
import AlertDialog from "../../components/Alert/Alert";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const useStyles = makeStyles(styles);

export default function MedicalRecordsList(props) {
  const classes = useStyles();

  const [medRecords, setMedRecords] = React.useState([]);
  const [tableData, setTableData] = React.useState([]);

    // amazon server url for med record
    const [medUrlPath, setMedUrlPath] = React.useState(null);
    // url of local file copy (not download)
    const [urlFile, setUrlFile] = React.useState(null);
    const [fileName, setFileName] = React.useState(null);
    const [currentRecord, setCurrentRecord] = React.useState(null);
    const [isViewingFile, setIsViewingFile] = React.useState(false);

    const [startDate, setStartDate] = React.useState(null);
    const [endDate, setEndDate] = React.useState(null);
    const [searchParams, setSearchParams] = React.useState(null);

    const [searchSuggestions, setSearchSuggestions] = React.useState([]);

    const [deleteSuccessInfo, setDeleteSuccessInfo] = React.useState(false);
    const [deleteErrorInfo, setDeleteErrorInfo] = React.useState(false);


    registerLocale('hr', hr);

    React.useEffect(() => {
        getRecords();
    }, []);

    function getfile(file) {
        setFileName(file);
        let path = '/patients/medrecords';
        if (props.patient !== null && props.user.role === 'doctor')
                path = '/doctors/' + props.patient.id + '/records';

        setCurrentRecord(medRecords.find((element) => {
            return element.file === file;
        }));

        path = '/api' + path + '/' + file + '/download';
        if (props.patient !== null && props.user.role === 'patient')
            path = path + '/?email=' + props.patient.email;

        fetch(path, { method: 'GET' })
            .then(response => {
                if (response.status >= 200 && response.status < 300)
                    return response.json();
                return null;
            })
            .then(file => {
                setMedUrlPath(file);
            });
    }

    function deleteFile() {

        let path = '/patients/medrecords';
        if (props.patient !== null && props.user.role === 'doctor')
            path = '/doctors/' + props.patient.id + '/records';

        if (currentRecord === null)
            return;

        let item = tableData.find(x => x[5] === currentRecord.file);
        let tableEntry = tableData.indexOf(item);
        if (tableEntry >= 0)
            tableData.splice(tableEntry, 1);

        path = '/api' + path + '/' + currentRecord.recordID + '/delete';
        if (props.patient !== null && props.user.role === 'patient')
            path = path + '/?email=' + props.patient.email;

        fetch(path, { method: 'DELETE' })
            .then(response => {
                console.log(response);
                if (response.status >= 200 && response.status < 300) {
                    closeFile();

                    getRecords();
                    setDeleteSuccessInfo(true);
                } else
                    setDeleteErrorInfo(true)
            });
    }

    React.useEffect( () => {
      if (medUrlPath === null)
          return;
        fetch(medUrlPath, {mode: 'cors'})
            .then(function (response) {
                  const reader = response.body.getReader();
                    return new ReadableStream({
                        start(controller) {
                            return pump();
                            function pump() {
                                return reader.read().then(({ done, value }) => {
                                    // When no more data needs to be consumed, close the stream
                                    if (done) {
                                        controller.close();
                                        return;
                                    }
                                    // Enqueue the next data chunk into our target stream
                                    controller.enqueue(value);
                                    return pump();
                                });
                            }
                        }
                    })
            })
            .then(stream => new Response(stream))
            .then(response => response.blob())
            .then(blob => URL.createObjectURL(blob))
            .then(url => {
                setUrlFile(url)
            });
    }, [medUrlPath]);

  function setRecords(records) {
      setMedRecords(records);
      const data = [];

      function getStringVal(item) {
          if (item == null || item === 'null' || (typeof item === 'string' && item.trim() === ''))
              return '----';
          return item;
      }

      records.map(rec => {
          let hospital = rec.hospital === null ? rec.oldRecordHospital : rec.hospital.name;
          let doctor = rec.doctor === null ? rec.oldRecordDoctor : (rec.doctor.firstName + ' ' + rec.doctor.lastName);
          let department = rec.departmentName;
          let diagnosis = rec.diagnosis;

          hospital = getStringVal(hospital);
          doctor = getStringVal(doctor);
          department = getStringVal(department);
          diagnosis = getStringVal(diagnosis);

          let dateString = rec.date;
          if (rec.date !== null) {
              dateString = getLocalDate(rec.date);
          }

          data.push([dateString, hospital, doctor, department, diagnosis, rec.file])
      });
      findSearchSuggestions(records);
      setTableData(data);
  }

  function getLocalDate(date) {
      let d = date.split('T')[0];
      var result = parse(d, 'yyyy-MM-dd', new Date())
      return result.toLocaleDateString();
  }

  function getRecords() {
      let path = '/patients/medrecords';
      if (props.patient !== null && props.user.role === 'doctor')
          path = '/doctors/' + props.patient.id + '/records';

      path = '/api' + path;
      if (props.patient !== null && props.user.role === 'patient')
          path = path + '/?email=' + props.patient.email;

      fetch(path, { method: 'GET' })
          .then(response => {
              if (response.status >= 200 && response.status < 300)
                return response.json();
              return []
          })
          .then(records => setRecords(records))
  }

  React.useEffect(() => getSearchedRecords(searchParams), [startDate, endDate]);

    function getSearchedRecords(search) {
      if (search === null || search.length === 0) {
          search = [];
          if (startDate === null && endDate === null) {
              getRecords();
              return;
          }
      }

        let path = '/patients/search';
        if (props.patient !== null && props.user.role === 'doctor')
            path = '/doctors/search/' + props.patient.id;

        let searchParams = '/?search=';
        search.map((item) => {
            searchParams = searchParams.concat(' ' + item.value);
        });

        let start = startDate !== null ? startDate.toISOString().slice(0,10) : '';
        let end = endDate !== null ? endDate.toISOString().slice(0,10) : '';
        searchParams = searchParams.concat('&dateFrom=' +
            (start) + '&dateTo=' + (end));

        path = '/api' + path + searchParams;
        if (props.patient !== null && props.user.role === 'patient')
            path = path + '&patientEmail=' + props.patient.email;

        fetch(path, { method: 'GET' })
            .then(response => {
                if (response.status >= 200 && response.status < 300) {
                    console.log(searchParams);
                    console.log(response);
                    return response.json();
                }
                return [];
            })
            .then(records => setRecords(records))
    }

    function findSearchSuggestions(records) {
        let suggestions = {};

        function addSuggestion(item) {
            if (item == null || item === 'null' || (typeof item === 'string' && item.trim() === ''))
                return;
            if (!suggestions[item])
                suggestions[item] = 1;
            else
                suggestions[item] = suggestions[item]+1;
        }

        for (let rec of records) {
            addSuggestion(rec.diagnosis);
            addSuggestion(rec.departmentName);
            if (rec.doctor === null)
                addSuggestion(rec.oldRecordDoctor);
            else {
                addSuggestion(rec.doctor.firstName);
                addSuggestion(rec.doctor.lastName);
            }
            if (rec.hospital === null)
                addSuggestion(rec.oldRecordHospital);
            else
                addSuggestion(rec.hospital.name);
        }
        let search = [];
        for (let key in suggestions) {
            search.push({ value: key, label: key, count: suggestions[key] })
        }
        search.sort((a, b) => (a.count > b.count) ? -1 : 1);
        setSearchSuggestions(search);
    }

  function openFile(file) {
      getfile(file);
      setIsViewingFile(true);
  }

   function closeFile() {
    setMedUrlPath(null);
    setUrlFile(null);
    setIsViewingFile(false);
   }

   const getEditRestriction = () => {
        if (props.user.role === 'doctor') {
            return currentRecord.doctor === null || props.user.user.id !== currentRecord.doctor.id;
        }
        return props.editRestriction;
    }

    const deleteSuccessAlert = () => {
        return (
            <AlertDialog
                title='Brisanje'
                message='Nalaz uspješno uklonjen'
                open={deleteSuccessInfo}
                setOpen={setDeleteSuccessInfo}
                onOk={() => setDeleteSuccessInfo(false)}
                onCancel={() => {}}
                okButton='Ok'
                okButtonColor='primary'
            />
        );
    };

    const deleteErrorAlert = () => {
        return (
            <AlertDialog
                title='Greška'
                message='Dogodila se greška prilikom brisanja nalaza. Nalaz trenutno nije moguće izbrisati, pokušajte kasnije.'
                open={deleteErrorInfo}
                setOpen={setDeleteErrorInfo}
                onOk={() => setDeleteErrorInfo(false)}
                onCancel={() => {}}
                okButton='Ok'
                okButtonColor='primary'
            />
        );
    };

   const searchBar = () => {
      return (
          <div style={{
              display: 'inline',
              //overflow: 'column wrap',
              marginTop: '10px',
              marginLeft: 'auto',
              marginRight: 'auto',
              marginBottom: '0px',
              width: '95%'
          }}>
              <DatePicker
                  selected={startDate}
                  onChange={date => setStartDate(date)}
                  selectsStart
                  startDate={startDate}
                  endDate={endDate}
                  dateFormat="dd.MM.yyyy."
                  placeholderText="Početni datum"
                  isClearable
              />
              <DatePicker
                  selected={endDate}
                  onChange={date => setEndDate(date)}
                  selectsEnd
                  startDate={startDate}
                  endDate={endDate}
                  minDate={startDate}
                  dateFormat="dd.MM.yyyy."
                  placeholderText="Završni datum"
                  isClearable
              />
              <span style={{
                  width: '50%',
                  minWidth: '500px'
              }}>
              <CreatableSelect
                  isClearable
                  onChange={(e) => {
                      setSearchParams(e);
                      getSearchedRecords(e);
                  }}
                  onInputChange={(e) => { }}
                  options={searchSuggestions}
                  isMulti
                  name="colors"
                  className="basic-multi-select"
                  classNamePrefix="select"
                  placeholder='Pretražite...'
              />
              </span>
          </div>
      );
   }

  return (
      <div>
          {deleteSuccessAlert()}
          {deleteErrorAlert()}
      {!isViewingFile ?
    <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
        <Card list>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>Moji nalazi</h4>

          </CardHeader>
            {searchBar()}
          <CardBody scrollable>

            <Table
              tableHeaderColor="primary"
              tableHead={["Datum", "Bolnica", "Doktor", "Odjel", "Dijagnoza", "File name"]}
              clickable={true}
              onClick={openFile}
              tableData={tableData}
              idIndex={5}
            />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
          :
          <GridContainer>
          <MedicalRecord
              file={urlFile}
              fileName={fileName}
              downloadLink={medUrlPath}
              onClose={closeFile}
              onDelete={deleteFile}
              user={props.user}
              editRestriction={getEditRestriction}
          /></GridContainer>  }
      </div>
  );
}
