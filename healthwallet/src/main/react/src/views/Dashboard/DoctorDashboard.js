import React from "react";

// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Accessibility from "@material-ui/icons/Accessibility";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Tasks from "components/Tasks/Tasks.js";
import CustomTabs from "components/CustomTabs/CustomTabs.js";
import Danger from "components/Typography/Danger.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import { bugs, website, server } from "variables/general.js";

import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts.js";

import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";
import {parse} from "date-fns";

const useStyles = makeStyles(styles);

export default function DoctorDashboard(props) {
  const classes = useStyles();

  const {
    statistics,
    ...rest
  } = props;

  const [medRecordTable, setMedRecordTable] = React.useState([]);
  const [patientsTable, setPatientsTable] = React.useState([]);

  React.useEffect(() => {
    if (statistics != null) {
      setRecords(statistics.recentRecords);
      setPatients(statistics.recentPatients);
    }
  }, [statistics, ]);

  function setRecords(records) {
    if (records == null)
      return;
    const data = [];
    records.map(rec => {
      const hospital = rec.hospital === null ? rec.oldRecordHospital : rec.hospital.name;
      const doctor = rec.doctor === null ? rec.oldRecordDoctor : (rec.doctor.firstName + ' ' + rec.doctor.lastName);

      let dateString = rec.date;
      if (rec.date !== null) {
        dateString = getLocalDate(rec.date);
      }
      data.push([dateString, hospital, doctor, rec.departmentName, rec.diagnosis])
    });
    setMedRecordTable(data);
  }

  function setPatients(patients) {
    if (patients == null)
      return;
    const data = [];
    patients.map(pat => {
      data.push([pat.firstName, pat.lastName, pat.email])
    });
    setPatientsTable(data);
  }

  function getLocalDate(date) {
    let d = date.split('T')[0];
    var result = parse(d, 'yyyy-MM-dd', new Date())
    return result.toLocaleDateString();
  }


  return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={6} md={6}>
            <Card>
              <CardHeader color="primary" stats icon>
                <CardIcon color="primary">
                  <Icon>content_copy</Icon>
                </CardIcon>
                <p className={classes.cardCategory}>Broj nalaza</p>
                <h3 className={classes.cardTitle}>
                  {statistics.numberOfRecords}
                </h3>
              </CardHeader>
              <CardFooter>
                <div>
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={6}>
            <Card>
              <CardHeader color="info" stats icon>
                <CardIcon color="info">
                  <Accessibility />
                </CardIcon>
                <p className={classes.cardCategory}>Broj pacijenata</p>
                <h3 className={classes.cardTitle}>{statistics.numberOfPatients}</h3>
              </CardHeader>
              <CardFooter>
                <div></div>
              </CardFooter>
            </Card>
          </GridItem>

        </GridContainer>
        <GridContainer>

          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>Najnoviji nalazi</h4>
              </CardHeader>
              <CardBody>
                <Table
                    tableHeaderColor="primary"
                    tableHead={["Datum", "Bolnica", "Doktor", "Odjel", "Dijagnoza"]}
                    //clickable={true}
                    //onClick={openFile}
                    tableData={medRecordTable}
                    //idIndex={5}
                />
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardHeader color="info">
                <h4 className={classes.cardTitleWhite}>Zadnji pacijenti</h4>
              </CardHeader>
              <CardBody>
                <Table
                    tableHeaderColor="info"
                    tableHead={["Ime", "Prezime", "Email"]}
                    //clickable={true}
                    //onClick={openFile}
                    tableData={patientsTable}
                    //idIndex={5}
                />
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
  );
}
