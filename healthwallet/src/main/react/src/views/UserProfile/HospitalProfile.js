import React, {useEffect} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import DatePicker from 'react-datepicker'
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import hr from 'date-fns/locale/hr';
import "react-datepicker/dist/react-datepicker.css";
import PasswordChangeCard from "./PasswordChangeCard";

import avatar from "assets/img/faces/marc.jpg";
import AlertDialog from "../../components/Alert/Alert";

const styles = {
    cardCategoryWhite: {
        color: "rgba(255,255,255,.62)",
        margin: "0",
        fontSize: "14px",
        marginTop: "0",
        marginBottom: "0"
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none"
    }
};

const useStyles = makeStyles(styles);

export default function PatientProfile(props) {

    const classes = useStyles();
    const { user, updateUser } = props;
    const [info,setInfo] = React.useState(new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate()));
    const [data, setData] = React.useState("");

    const [errors, setErrors] = React.useState([]);
    const [success, setSuccess] = React.useState(false);
    const [isAlertOpen, onSetIsAlertOpen] = React.useState(false);
    const [deleteError, setDeleteError] = React.useState(false);

    const [isEditing, setIsEditing] = React.useState(false);
    const [isDeleting, setIsDeleting] = React.useState(false);


    registerLocale('hr', hr)
    useEffect(() => {
        getUser();
    }, [user, ]);


    const deleteAlert = () => {
        return (
            <AlertDialog
                title='Ukloni bolnicu'
                message='Uklanjanjem bolnice izgubit ćete nepovratno sve njene podatke kao i podatke svih njenih registriranih doktora.'
                open={isAlertOpen}
                setOpen={onSetIsAlertOpen}
                onOk={removeHospital}
                onCancel={() => setIsDeleting(false)}
                okButton='Ukloni'
                cancelButton='Odustani'
                okButtonColor='rose'
            />
        );
    };

    function removeHospital() {
        setIsDeleting(true);
        const options = {
            method: 'DELETE'
        };
        fetch('/api/hospitals/remove-self', options)
            .then(response => {
                if (response.status === 200) {
                    setSuccess(true);
                    updateUser();
                } else {
                    setDeleteError(true);
                }
            });
    }

    function getUser() {
        if(user != null){
            if(user.user == null){
                setInfo(user.role);
            } else {
                setData(user.user);
            }
        }
    }

    function onChange(event) {
        const {id, value} = event.target;
        setData(oldForm => ({...oldForm, [id]: value}));
    }

    function removeError(err) {
        setErrors(e => {
            const index = e.indexOf(err);
            if (index > -1)
                e.splice(index, 1);
            return e;
        });
    }

    function cancelEdit() {
        setIsEditing(false);
        setErrors([]);
        setSuccess(false);
        getUser();
    }

    function SubmitFunction(e){
        e.preventDefault();

        const options = {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        fetch('/api/hospitals/update-my-info', options)
            .then(response => {
                if (response.status >= 200 && response.status < 300) {
                    setSuccess(true);
                    setIsEditing(false);
                    updateUser();
                    return [];
                } else {
                }
                return response.json();
            })
            .then(data => {
                setErrors(data);
            });
    }


    return (
        <div>
            {deleteAlert()}
            <GridContainer>
                <GridItem xs={12} sm={12} md={10}>
                    <Card>
                        <CardHeader color="primary">
                            <h4 className={classes.cardTitleWhite}>Uredi profil bolnice</h4>
                        </CardHeader>
                        <CardBody>
                            <GridContainer>
                                <GridItem xs={12} sm={12} md={5}>
                                    <CustomInput
                                        labelText="Korisničko ime"
                                        id="email"
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            readOnly: true,
                                            disabled: isEditing,
                                            value: data.email != null ? data.email:"" // tekst default
                                        }}
                                        onChange={onChange}
                                    />
                                </GridItem>
                            </GridContainer>
                            <GridContainer>
                                <GridItem xs={12} sm={12} md={6}>
                                    <CustomInput
                                        labelText="Ime bolnice"
                                        id="name"
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            readOnly: !isEditing,
                                            value: data.name != null ? data.name :"", // tekst default
                                        }}
                                        onChange={onChange}
                                        success={success}
                                        error={errors.indexOf('name') > -1}
                                    />
                                </GridItem>
                            </GridContainer>
                            <GridContainer>
                                <GridItem xs={12} sm={12} md={4}>
                                    <CustomInput
                                        labelText="OIB"
                                        id="oib"
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            readOnly: !isEditing,
                                            value: data.oib != null ? data.oib :""
                                        }}
                                        onChange={onChange}
                                        success={success}
                                        error={errors.indexOf('oib') > -1}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={8}>
                                    <CustomInput
                                        labelText="Adresa"
                                        id="address"
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            readOnly: !isEditing,
                                            value: data.address != null ? data.address :""
                                        }}
                                        onChange={onChange}
                                        success={success}
                                        error={errors.indexOf('address') > -1}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={4}>
                                    <CustomInput
                                        labelText="Poštanski broj"
                                        id="zipCode"
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            readOnly: !isEditing,
                                            value: data.zipCode != null ? data.zipCode :""
                                        }}
                                        onChange={onChange}
                                        success={success}
                                        error={errors.indexOf('zipCode') > -1}
                                    />
                                </GridItem>
                            </GridContainer>
                        </CardBody>
                        {isEditing ?
                            <CardFooter>
                                <Button color="secondary"
                                        onClick={cancelEdit}
                                >Odustani</Button>
                                <Button color="primary"
                                        onClick={SubmitFunction}
                                >Pohrani</Button>
                            </CardFooter>
                            :
                            <CardFooter>
                                <Button
                                    color="rose"
                                    onClick={() => onSetIsAlertOpen(true)}
                                    disabled={isDeleting}
                                >
                                    Ukloni račun
                                </Button>
                                <Button color="primary"
                                        onClick={() => setIsEditing(true)}
                                        disabled={isDeleting}
                                >Uredi</Button>
                            </CardFooter>
                        }
                        <CardFooter>{deleteError ? <p>Dogodila se greška prilikom brisanja računa</p> : null}</CardFooter>
                    </Card>
                </GridItem>
            </GridContainer>
            <GridContainer>
                <GridItem xs={12} sm={12} md={10}>
                    <PasswordChangeCard
                        {...props}
                    />
                </GridItem>
            </GridContainer>
        </div>
    );
}
