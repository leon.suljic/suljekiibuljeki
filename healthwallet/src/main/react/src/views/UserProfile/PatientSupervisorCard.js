import React, {useEffect} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import DatePicker from 'react-datepicker'
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import hr from 'date-fns/locale/hr';
import "react-datepicker/dist/react-datepicker.css";

import avatar from "assets/img/faces/marc.jpg";
import Dropdown from "react-dropdown";
import Select from 'react-select';

import {
    grayColor,
} from "assets/jss/material-dashboard-react.js";
import AlertDialog from "../../components/Alert/Alert";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function PatientSupervisorCard(props) {

  const classes = useStyles();
  const { user, updateUser } = props;
  const [data, setData] = React.useState('');

    const [errors, setErrors] = React.useState([]);
    const [success, setSuccess] = React.useState(false);

    const [hasSupervisor, setHasSupervisor] = React.useState(false);
    const [isSupervisorAlertOpen, setIsSupervisorAlertOpen] = React.useState(false);


    registerLocale('hr', hr)
    useEffect(() => {
        if(user != null ){
            if(user.user != null){
                if (user.user.supervisor !== null) {
                    setHasSupervisor(true);
                    setData(user.user.supervisor);
                } else {
                    setHasSupervisor(false);
                    setData('');
                }
            }
        }
    }, [user]);


    function onChange(event) {
        const {name, value} = event.target;
        setData(oldForm => ({...oldForm, [name]: value}));
        removeError(name);
        setSuccess(false);
    }

    function removeError(err) {
        setErrors(e => {
            const index = e.indexOf(err);
            if (index > -1)
                e.splice(index, 1);
            return e;
        });
    }

    function setSupervisor(){

        const options = {
            method: 'POST'
        };

        fetch('/api/patients/add-supervisor/?email='+data.email, options)
            .then(response => {
                if (response.status === 200) {
                    setSuccess(true);
                    updateUser();
                    return [];
                } else {
                    return ["email"];
                }
                return response.json();
            })
            .then(data => {
                setErrors(data);
            });
    }

    function removeSupervisor(){

        const options = {
            method: 'POST'
        };

        fetch('/api/patients/delete-supervised/?email='+data.email, options)
            .then(response => {
                if (response.status >= 200 && response.status < 200) {
                    setSuccess(true);
                    updateUser();
                    return [];
                } else if (response.status >= 400 && response.status < 500) {
                } else
                    return [];
                return response.json();
            })
            .then(data => {
                setErrors(data);
            });
    }

    const supervisorAlert = () => {
        return (
            <AlertDialog
                title='Postavi nadziratelja'
                message='Postavljanjem nadziratelja omogućit ćete mu kontolu nad vašim računom te time izgubiti mogućnost promjene svojih podataka, dodavanja nalaza te uklanjanja doktora. Ovaj postupak može poništiti samo Vaš nadziratelj. Želite li postaviti nadziratelja?'
                open={isSupervisorAlertOpen}
                setOpen={setIsSupervisorAlertOpen}
                onOk={setSupervisor}
                onCancel={() => setIsSupervisorAlertOpen(false)}
                okButton='Postavi nadziratelja'
                cancelButton='Odustani'
                okButtonColor='primary'
            />
        );
    };

    const supervisorInfo = () => {
        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                        labelText="Ime"
                        id="sup-firstName"
                        formControlProps={{
                            fullWidth: true
                        }}
                        inputProps={{
                            readOnly: true,
                            value: data.firstName != null ? data.firstName :"" // tekst default
                        }}
                        name="firstName"
                        success={success}
                    />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                        labelText="Prezime"
                        id="sup-lastName"
                        formControlProps={{
                            fullWidth: true
                        }}
                        inputProps={{
                            readOnly: true,
                            value: data.lastName != null ? data.lastName :""
                        }}
                        name="lastName"
                        success={success}
                    />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                        labelText="OIB"
                        id="sup-oib"
                        formControlProps={{
                            fullWidth: true
                        }}
                        inputProps={{
                            readOnly: true,
                            value: data.oib != null ? data.oib :""
                        }}
                        name="oib"
                        success={success}
                    />
                </GridItem>
            </GridContainer>
        );
    };


    const supervisorCard = () => {
        return (
            <Card>
                {supervisorAlert()}
                <CardHeader color="primary">
                    <h4 className={classes.cardTitleWhite}>Nadziratelj</h4>
                </CardHeader>
                { props.statistics.numberOfPatients > 0 ?
                    <CardBody>
                        Ne možete postaviti nadziratelja jer ste Vi nadziratelj drugih pacijenata
                    </CardBody> :
                <CardBody>
                    <div>
                        <p>Postavljanjem nadziratelja gubite mogućnost izmjene vlastitih podataka.</p>
                    </div>
                    <GridContainer>
                        <GridItem xs={12} sm={12} md={8}>
                            <CustomInput
                                labelText="Korisničko ime nadziratelja"
                                id="sup-email"
                                formControlProps={{
                                    fullWidth: true
                                }}
                                inputProps={{
                                    disabled: false,
                                    value: data.email != null ? data.email : ""
                                }}
                                name="email"
                                onChange={onChange}
                                error={errors.indexOf('email') > -1}
                            />
                        </GridItem>
                    </GridContainer>
                    {hasSupervisor ? supervisorInfo() : null}
                </CardBody>}
                {!hasSupervisor && props.statistics.numberOfPatients === 0?
                    <CardFooter>
                        <Button color="primary"
                                onClick={() => setIsSupervisorAlertOpen(true)}
                        >Postavi nadziratelja</Button>
                    </CardFooter> : null}
            </Card>
        );
    }

    return (
        <div>
            {props.userConfirmed ?
                supervisorCard() :
                <Card>
                    <CardHeader color="primary">
                        <h4 className={classes.cardTitleWhite}>Nadziratelj</h4>
                    </CardHeader>
                    <CardBody>
                        Postavljanje nadziratelja moguće je tek nakon potvrde korisničkog računa
                    </CardBody>
                </Card>
            }
        </div>
    )
}
