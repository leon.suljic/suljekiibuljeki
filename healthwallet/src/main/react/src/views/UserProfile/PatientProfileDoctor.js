import React, {useEffect} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import DatePicker from 'react-datepicker'
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import hr from 'date-fns/locale/hr';
import "react-datepicker/dist/react-datepicker.css";

import avatar from "assets/img/faces/marc.jpg";
import Dropdown from "react-dropdown";
import Select from 'react-select';

import {
    grayColor,
} from "assets/jss/material-dashboard-react.js";
import AlertDialog from "../../components/Alert/Alert";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function PatientProfileDoctor(props) {

  const classes = useStyles();
  const { patient, setPatient} = props;
  const [info, setInfo] = React.useState(new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate()));

    const [isDeleting, setIsDeleting] = React.useState(false);
    const [isDeleteAlertOpen, setIsDeleteAlertOpen] = React.useState(false);
    const [success, setSuccess] = React.useState(false);
    const [error, setError] = React.useState(false);

    registerLocale('hr', hr)

    const getSex = () => {
        if (patient === '' || patient.sex === null)
            return '';
        let sex = sexOptions.find(e => e.value === patient.sex);

        return sex.label;
    };

    const sexOptions = [
        { value: 'MALE', label: 'Muški' },
        { value: 'FEMALE', label: 'Ženski' },
        { value: 'OTHER', label: 'Ostalo' }
    ];


    function removePatient() {
        setIsDeleting(true);
        const options = {
            method: 'POST'
        };
        fetch('/api/doctors/unbind-patient/' + patient.id, options)
            .then(response => {
                if (response.status === 200) {
                    setSuccess(true);
                    setPatient(null);
                    props.history.push('/dashboard/patients');
                } else {
                    setError(true);
                }
            });
    }

    const birthDatePicker = () => {

            return (
                <div style={{
                    font: '20px'
                }}>
                    <CustomInput
                        labelText="Datum rođenja"
                        id="birthDate"
                        formControlProps={{
                            fullWidth: true
                        }}
                        inputProps={{
                            readOnly: true,
                            value: patient.birthDate != null ? new Date(patient.birthDate).toLocaleDateString('hr', {
                                day : 'numeric',
                                month : 'short',
                                year : 'numeric'
                            }) :""
                        }}
                    />
                </div>
            )
    };

    const removePatientAlert = () => {
        return (
            <AlertDialog
                title='Ukloni pacijenta'
                message='Uklanjanjem pacijenta gubite pristup njegovim podacima. Jeste li sigurni?'
                open={isDeleteAlertOpen}
                setOpen={setIsDeleteAlertOpen}
                onOk={removePatient}
                onCancel={() => setIsDeleteAlertOpen(false)}
                okButton='Ukloni'
                cancelButton='Odustani'
                okButtonColor='rose'
            />
        );
    };


  return (
      <Card>
          {removePatientAlert()}
        <CardHeader color="primary">
          <h4 className={classes.cardTitleWhite}>Pacijent</h4>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={5}>
              <CustomInput
                labelText="Korisničko ime"
                id="email"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                    readOnly: true,
                    value: patient.email != null ? patient.email:"" // tekst default
                }}
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Ime"
                id="firstName"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                    readOnly: true,
                    value: patient.firstName != null ? patient.firstName :"", // tekst default
                    variant: "outlined"
                }}
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Prezime"
                id="lastName"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                    readOnly: true,
                    value: patient.lastName != null ? patient.lastName :""
                }}
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
              <GridItem xs={12} sm={12} md={4}>

                  {birthDatePicker()}
              </GridItem>
            <GridItem xs={12} sm={12} md={4}>
              <CustomInput
                labelText="OIB"
                id="oib"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                    readOnly: true,
                    value: patient.oib != null ? patient.oib :""
                }}
              />
            </GridItem>
              <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                      labelText="Adresa"
                      id="address"
                      formControlProps={{
                          fullWidth: true
                      }}
                      inputProps={{
                          readOnly: true,
                          value: patient.address != null ? patient.address :""
                      }}
                  />
              </GridItem>
              <GridItem xs={12} sm={12} md={4}>
              <CustomInput
                  labelText="Poštanski broj"
                  id="zipCode"
                  formControlProps={{
                      fullWidth: true
                  }}
                  inputProps={{
                      readOnly: true,
                      value: patient.zipCode != null ? patient.zipCode :""
                  }}
              />
            </GridItem>
              <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                      labelText="Spol"
                      id="sex"
                      formControlProps={{
                          fullWidth: true
                      }}
                      inputProps={{
                          readOnly: true,
                          value: getSex()
                      }}
                  />
              </GridItem>
              <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                      labelText="Matični broj osiguranika"
                      id="healthCardNumber"
                      formControlProps={{
                          fullWidth: true
                      }}
                      inputProps={{
                          readOnly: true,
                          value: patient.healthCardNumber != null ? patient.healthCardNumber :""
                      }}
                  />
              </GridItem>
              <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                      labelText="PIN"
                      id="pin"
                      formControlProps={{
                          fullWidth: true
                      }}
                      inputProps={{
                          readOnly: true,
                          value: patient.pin != null ? patient.pin :""
                      }}
                  />
              </GridItem>
          </GridContainer>
        </CardBody>
          {success ?
              <CardFooter>
              <Button
                  color="secondary"
                  onClick={() => {
                     props.history.push('/patients');
                  }}
              >Nazad</Button>
                  <p>Pacijent uspješno uklonjen</p>
          </CardFooter> :
          <CardFooter>
              {!isDeleting ?
                  <Button
                      color="rose"
                      onClick={() => setIsDeleteAlertOpen(true)}
                  >Ukloni pacijenta</Button> : null}
              {error ? <p>Dogodila se greška prilikom uklanjanja</p> : null}
          </CardFooter> }
      </Card>
  );
}
