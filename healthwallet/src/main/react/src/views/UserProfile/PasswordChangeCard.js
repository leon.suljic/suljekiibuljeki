import React, {useEffect} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import DatePicker from 'react-datepicker'
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import hr from 'date-fns/locale/hr';
import "react-datepicker/dist/react-datepicker.css";

import avatar from "assets/img/faces/marc.jpg";
import Dropdown from "react-dropdown";
import Select from 'react-select';

import {
    grayColor,
} from "assets/jss/material-dashboard-react.js";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function PasswordCard(props) {

  const classes = useStyles();
  const { user, updateUser } = props;
  const [data, setData] = React.useState({oldPassword: '', newPassword1: '', newPassword2: ''});

    const [errors, setErrors] = React.useState([]);
    const [success, setSuccess] = React.useState(false);

    function onChange(event) {
        const {name, value} = event.target;
        setData(oldForm => ({...oldForm, [name]: value}));
        removeError(name);
        setSuccess(false);
    }

    function removeError(err) {
        setErrors(e => {
            const index = e.indexOf(err);
            if (index > -1)
                e.splice(index, 1);
            return e;
        });
    }

    function onSubmit(e) {
        e.preventDefault();
        const options = {
            method: 'POST'
        };
        let path = '/api/forgotten-password/change-password';
        path += '/?oldPassword=' + data.oldPassword;
        path += '&newPassword1=' + data.newPassword1;
        path += '&newPassword2=' + data.newPassword2;

        fetch(path, options)
            .then(response => {
                if (response.status >= 200 && response.status < 300) {
                    setSuccess(true);
                    return [];
                } else if (response.status >= 400 && response.status < 500)
                    return response.json();
                return [];
            })
            .then(data => {
                setErrors(data);
            });
    }

    return (
        <div>
            <Card>
                <CardHeader color="primary">
                    <h4 className={classes.cardTitleWhite}>Promjena lozinke</h4>
                </CardHeader>
                <CardBody>
                    <GridContainer>
                        <GridItem xs={12} sm={12} md={12}>
                            <CustomInput
                                labelText="Stara lozinka"
                                id="oldPassword"
                                formControlProps={{
                                    fullWidth: true
                                }}
                                success={success}
                                error={errors.indexOf('oldPassword') > -1}
                                type="password"
                                name='oldPassword'
                                onChange={onChange}
                            />
                        </GridItem>
                        <GridItem xs={12} sm={12} md={12}>
                            <CustomInput
                                labelText="Nova lozinka (min. 8 znakova)"
                                id="newPassword1"
                                formControlProps={{
                                    fullWidth: true
                                }}
                                success={success}
                                error={errors.indexOf('newPassword') > -1}
                                type="password"
                                name='newPassword1'
                                onChange={onChange}
                            />
                        </GridItem>
                        <GridItem xs={12} sm={12} md={12}>
                            <CustomInput
                                labelText="Potvrdite novu lozinku"
                                id="newPassword2"
                                formControlProps={{
                                    fullWidth: true
                                }}
                                success={success}
                                error={errors.indexOf('newPassword') > -1}
                                type="password"
                                name='newPassword2'
                                onChange={onChange}
                            />
                        </GridItem>
                    </GridContainer>
                </CardBody>
                <CardFooter>
                    <Button
                        color="primary"
                        type="submit"
                        onClick={onSubmit}
                    >Promijeni lozinku</Button>
                </CardFooter>
            </Card>
        </div>
    )
}
