import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import PatientProfileCard from "./PatientProfileCard";
import PatientSupervisorCard from "./PatientSupervisorCard";
import PasswordChangeCard from "./PasswordChangeCard";

import avatar from "assets/img/faces/marc.jpg";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function PatientProfile(props) {

  const classes = useStyles();
  const { user, updateUser } = props;

  const userConfirmed = user.user.oib !== null;

  return (
    <div>
      <GridContainer>
          <GridItem xs={12} sm={12} md={10}>
            <PatientProfileCard
                userConfirmed={userConfirmed}
                {...props}/>
          </GridItem>
          { props.patient === null ?
          <GridItem xs={12} sm={12} md={10}>
            <PasswordChangeCard
                {...props}/>
          </GridItem> : null}
          { props.patient === null ?
          <GridItem xs={12} sm={12} md={10}>
            <PatientSupervisorCard
                userConfirmed={userConfirmed}
                {...props}/>
          </GridItem> : null}
      </GridContainer>
    </div>
  );
}
