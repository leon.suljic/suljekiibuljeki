import React, {useEffect} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import AlertDialog from "../../components/Alert/Alert";
import DatePicker from 'react-datepicker'
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import hr from 'date-fns/locale/hr';
import "react-datepicker/dist/react-datepicker.css";

import avatar from "assets/img/faces/marc.jpg";
import Dropdown from "react-dropdown";
import Select from 'react-select';

import {
    grayColor,
} from "assets/jss/material-dashboard-react.js";
import {func} from "prop-types";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function PatientProfileCard(props) {

  const classes = useStyles();
  const { user, updateUser, patient, setPatient } = props;
  const [info, setInfo] = React.useState(new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate()));
  const [data, setData] = React.useState("");

//{ id: '', email: '', password: '', confirmationLink: '', forgottenPassCode : '', oib: '', firstName: '', lastName: '', birthDate: ''}

    const [errors, setErrors] = React.useState([]);
    const [wasError, setWasError] = React.useState(false);
    const [success, setSuccess] = React.useState(false);

    const [canEdit, setCanEdit] = React.useState(false);
    const [isEditing, setIsEditing] = React.useState(false);
    const [isDeleting, setIsDeleting] = React.useState(false);
    const [deleteError, setDeleteError] = React.useState(false);

    const [isAlertOpen, onSetIsAlertOpen] = React.useState(false);
    const [isSelfDeleteAlertOpen, setIsDeleteSelfAlertOpen] = React.useState(false);


    const supervised = patient != null && user.role === 'patient';

    registerLocale('hr', hr)
    useEffect(() => {
        getUser();
    }, [user, patient, ]);

    function getUser() {
        if (patient != null) {
            setData(patient);
            setCanEdit(true);
            return;
        }
        if(user != null){
            if(user.user == null){
                setInfo(user.role);
            } else {
                setData(user.user);
                setCanEdit(user.user.supervisor == null && props.userConfirmed);
            }
        }
    }

    function handleDateChange(selectedDate) {
        setData(oldForm => ({...oldForm, birthDate: selectedDate}));
    }

    function onChange(event) {
        const {id, value} = event.target;
        setData(oldForm => ({...oldForm, [id]: value}));
        removeError(id);
        setSuccess(false);
    }

    function removeError(err) {
        setErrors(e => {
            const index = e.indexOf(err);
            if (index > -1)
                e.splice(index, 1);
            return e;
        });
    }

    function cancelEdit() {
        setIsEditing(false);
        setErrors([]);
        setWasError(false);
        setSuccess(false);
        getUser();
    }

    function SubmitFunction(e){
        e.preventDefault();

        const options = {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        let url = '/api/patients/update-my-info';
        if (supervised)
            url = url + '?id=' + patient.email;
        fetch(url, options)
            .then(response => {
                if (response.status >= 200 && response.status < 300) {
                    setSuccess(true);
                    setIsEditing(false);
                    updateUser();
                    updatePatient();
                    return [];
                }
                return response.json();
            })
            .then(data => {
                if (data.length > 0)
                    setWasError(true);
                setErrors(data);
            });
    }

    function updatePatient() {
        if (patient != null) {
            let url = '/api/patients/get-supervised/?id=' + patient.id;
            fetch(url)
                .then(response => response.json())
                .then(data => {
                    if (data.length > 0)
                        setPatient(data[0]);
                });
        }
    }

    function removePatient() {
       setIsDeleting(true);
        const options = {
            method: 'DELETE'
        };
        fetch('/api/patients/remove-self', options)
            .then(response => {
                if (response.status === 200) {
                    setSuccess(true);
                    updateUser();
                    setPatient(null);
                } else {
                    setDeleteError(true);
                }
            });
    }

    function removeSupervised() {

        if (patient != null) {
            let url = '/api/patients/remove-supervised/?email=' + patient.email;
            fetch(url, {method: 'POST'})
                .then(response => {
                    if (response.status === 200) {
                        setPatient(null);
                        props.history.push('/dashboard');
                    }
                });
        }
    }

    const getSex = () => {
        if (data === '' || data.sex == null)
            return '';
        let sex = sexOptions.find(e => e.value === data.sex);

        return sex.label;
    }

    const setSex = (e) => {
        const {value, label} = e;
        onChange({target: {id: 'sex', value: value}});
    };

    const sexOptions = [
        { value: 'MALE', label: 'Muški' },
        { value: 'FEMALE', label: 'Ženski' },
        { value: 'OTHER', label: 'Ostalo' }
    ];

    const SexDropdown = () =>  {
        if (data === '')
            return ;
        let sex = sexOptions.find(e => e.value === data.sex);

        return (
            <div>
            <p style={{
                fontSize: '11px',
                marginTop: '23px',
                lineHeight: '5px',
                color: grayColor[1]
            }}>Spol</p>
            <Select
                defaultValue={sex}

                options={sexOptions}
                onChange={setSex}
            />
            </div>);
    };

    const birthDatePicker = () => {

        if (isEditing)
            return (
                <div style={{
                    font: '20px'
                }}>
                    <p style={{
                        fontSize: '11px',
                        marginTop: '23px',
                        lineHeight: '5px',
                        color: grayColor[1]
                    }}>Datum rođenja</p>
                    <DatePicker
                        className="CustomInput"
                        label="datum"
                        dateFormat="dd.MM.yyyy."
                        selected={data.birthDate !=null ? Date.parse(data.birthDate) :Date.parse(info)}
                        onChange={date => handleDateChange(date)}
                        locale="hr"
                        id="birthDate"
                    />
                </div>
            );
        else
            return (
                <div style={{
                    font: '20px'
                }}>
                    <CustomInput
                        labelText="Datum rođenja"
                        id="birthDate"
                        formControlProps={{
                            fullWidth: true
                        }}
                        inputProps={{
                            readOnly: true,
                            value: data.birthDate != null ? new Date(data.birthDate).toLocaleDateString('hr', {
                                day : 'numeric',
                                month : 'short',
                                year : 'numeric'
                            }) :""
                        }}//TODO tu sam maknuo onchange ako bude slucajno prouzrocilo probleme stavi natrag
                    />
                </div>
            )
    };

    const cantEditMessage = () => {
        if (!props.userConfirmed)
            return (<div>Molimo potvrdite svoj korisnički račun preko linka kojeg ste zaprimili na email adresu kako bi mogli uređivati podatke</div>);
        return (<div>Onemogućeno uređivanje jer imate postavljenog nadziratelja</div>);
    };


    const deleteSupervisedAlert = () => {
        return (
            <AlertDialog
                title='Ukloni nadziranog pacijenta'
                message='Uklanjanjem pacijenta izgubit ćete mogućnost kontrole nad njegovim podacima. Pacijenta možete ponovno nadzirati samo ako Vas on ponovno postavi za nadziratelja.'
                open={isAlertOpen}
                setOpen={onSetIsAlertOpen}
                onOk={removeSupervised}
                onCancel={() => {}}
                okButton='Ukloni'
                cancelButton='Odustani'
                okButtonColor='rose'
            />
        );
    };

    const deleteSelfAlert = () => {
        return (
            <AlertDialog
                title='Izbriši korisnički račun'
                message='Brisanjem korisničkog računa izgubit ćete nepovratno pristup Vašem računu te će se ukloniti svi Vaši podaci'
                open={isSelfDeleteAlertOpen}
                setOpen={setIsDeleteSelfAlertOpen}
                onOk={removePatient}
                onCancel={() => setIsDeleting(false)}
                okButton='Izbriši račun'
                cancelButton='Odustani'
                okButtonColor='rose'
            />
        );
    };

  return (
      <Card>
          {deleteSupervisedAlert()}
          {deleteSelfAlert()}
        <CardHeader color="primary">
            {patient == null ?
                <h4 className={classes.cardTitleWhite}>Moj profil</h4> :
                <h4 className={classes.cardTitleWhite}>Profil nadziranog pacijenta</h4>
            }
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Korisničko ime"
                id="email"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                    readOnly: true,
                    disabled: isEditing,
                    value: data.email != null ? data.email:"" // tekst default
                }}
                onChange={onChange}
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Ime"
                id="firstName"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                    readOnly: !isEditing,
                    value: data.firstName != null ? data.firstName :"", // tekst default
                }}
                onChange={onChange}
                success={success}
                error={errors.indexOf('firstName') > -1}
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Prezime"
                id="lastName"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                    readOnly: !isEditing,
                    value: data.lastName != null ? data.lastName :""
                }}
                onChange={onChange}
                success={success}
                error={errors.indexOf('lastName') > -1}
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
              <GridItem xs={12} sm={12} md={4}>
                  {birthDatePicker()}
              </GridItem>
            <GridItem xs={12} sm={12} md={4}>
              <CustomInput
                labelText={"OIB" + (wasError ? " (11 znamenki)" : "")}
                id="oib"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                    readOnly: !isEditing,
                    value: data.oib != null ? data.oib :""
                }}
                onChange={onChange}
                success={success}
                error={errors.indexOf('oib') > -1}
              />
            </GridItem>
              <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                      labelText="PIN"
                      id="pin"
                      formControlProps={{
                          fullWidth: true
                      }}
                      inputProps={{
                          readOnly: true,
                          disabled: isEditing,
                          value: data.pin != null ? data.pin :""
                      }}
                      onChange={onChange}
                  />
              </GridItem>
              <GridItem xs={12} sm={12} md={8}>
                  <CustomInput
                      labelText="Adresa"
                      id="address"
                      formControlProps={{
                          fullWidth: true
                      }}
                      inputProps={{
                          readOnly: !isEditing,
                          value: data.address != null ? data.address :""
                      }}
                      onChange={onChange}
                      success={success}
                      error={errors.indexOf('address') > -1}
                  />
              </GridItem>
              <GridItem xs={12} sm={12} md={4}>
              <CustomInput
                  labelText={"Poštanski broj" + (wasError ? " (5 znamenki)" : "")}
                  id="zipCode"
                  formControlProps={{
                      fullWidth: true
                  }}
                  inputProps={{
                      readOnly: !isEditing,
                      value: data.zipCode != null ? data.zipCode :""
                  }}
                  onChange={onChange}
                  success={success}
                  error={errors.indexOf('zipCode') > -1}
              />
            </GridItem>
              <GridItem xs={12} sm={12} md={4}>
                  {isEditing ?
                      SexDropdown() :

                  <CustomInput
                      labelText="Spol"
                      id="sex"
                      formControlProps={{
                          fullWidth: true
                      }}
                      inputProps={{
                          readOnly: true,
                          value: getSex()
                      }}
                      onChange={onChange}
                      success={success}
                      error={errors.indexOf('sex') > -1}
                  />}
              </GridItem>
              <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                      labelText={"Matični broj osiguranika" + (wasError ? " (9 znamenki)" : "")}
                      id="healthCardNumber"
                      formControlProps={{
                          fullWidth: true
                      }}
                      inputProps={{
                          readOnly: !isEditing,
                          value: data.healthCardNumber != null ? data.healthCardNumber :""
                      }}
                      onChange={onChange}
                      success={success}
                      error={errors.indexOf('healthCardNumber') > -1}
                  />
              </GridItem>
          </GridContainer>
        </CardBody>
          {isEditing ?
            <CardFooter>
                <Button color="secondary"
                        onClick={cancelEdit}
                >Odustani</Button>
                <Button color="primary"
                        onClick={SubmitFunction}
                >Pohrani</Button>
            </CardFooter>
              :
              <div>

              {!canEdit ?
                  <CardFooter>
                  <div>
                      {!isDeleting && user.user.supervisor == null && patient == null ?
                          <Button color = "rose"
                                  disabled = {isDeleting}
                                  onClick= {() => setIsDeleteSelfAlertOpen(true)}
                          >
                              Izbriši račun
                          </Button> :
                          null
                      }
                      {cantEditMessage()}
                  </div>
                  </CardFooter>
                  :
                  <CardFooter>
                      {!isDeleting && patient == null ?
                          <Button color = "rose"
                                  disabled = {isDeleting}
                                  onClick= {() => setIsDeleteSelfAlertOpen(true)}
                          >
                              Izbriši račun
                          </Button> :
                          null
                      }

                      <Button color="primary"
                                onClick={() => setIsEditing(true)}
                                disabled={!canEdit && isDeleting}
                      >Uredi</Button>
                      {supervised ?
                      <Button color="rose"
                              onClick={() => onSetIsAlertOpen(true)}
                              disabled={isDeleting}
                      >Ukloni</Button> : null }
                      {deleteError ? <p>Dogodila se greška prilikom brisanja računa</p> : null}
                  </CardFooter>
              }
              </div>
          }
      </Card>
  );
}
