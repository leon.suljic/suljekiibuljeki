import React, {useEffect} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import PasswordChangeCard from "./PasswordChangeCard";

import DatePicker from 'react-datepicker'
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import hr from 'date-fns/locale/hr';
import "react-datepicker/dist/react-datepicker.css";


import Select from 'react-select';

import {
    grayColor,
} from "assets/jss/material-dashboard-react.js";
import AlertDialog from "../../components/Alert/Alert";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function DoctorProfileCard(props) {

  const classes = useStyles();
  const { user, doctor, onBack, refreshList, updateUser } = props;
  const [info, setInfo] = React.useState(new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate()));
  const [data, setData] = React.useState("");
//{ id: '', email: '', password: '', confirmationLink: '', forgottenPassCode : '', oib: '', firstName: '', lastName: '', birthDate: ''}

    const [errors, setErrors] = React.useState([]);
    const [success, setSuccess] = React.useState(false);
    const [deleteError, setDeleteError] = React.useState(false);
    const [isDeleting, setIsDeleting] = React.useState(false);
    const [isEditing, setIsEditing] = React.useState(false);
    const [isAlertOpen, onSetIsAlertOpen] = React.useState(false);

    const [isDoctor, setIsDoctor] = React.useState(false);

    registerLocale('hr', hr)
    useEffect(() => {
        getDoctor();
    }, [doctor]);

    function getDoctor() {
        if (user != null && user.role === 'doctor') {
            setData(user.user);
            setIsDoctor(true);
            return;
        }
        if(doctor == null){
            setInfo('');
        } else {
            setData(doctor);
        }
    }

    function handleDateChange(selectedDate) {
        setData(oldForm => ({...oldForm, birthDate: selectedDate}));
    }

    function onChange(event) {
        const {id, value} = event.target;
        setData(oldForm => ({...oldForm, [id]: value}));
        removeError(id);
        setSuccess(false);
    }

    function removeError(err) {
        setErrors(e => {
            const index = e.indexOf(err);
            if (index > -1)
                e.splice(index, 1);
            return e;
        });
    }

    const deleteAlert = () => {
        return (
            <AlertDialog
                title='Ukloni doktora'
                message='Uklanjanjem doktora izgubit ćete nepovratno sve njegove podatke.'
                open={isAlertOpen}
                setOpen={onSetIsAlertOpen}
                onOk={removeDoctor}
                onCancel={() => {}}
                okButton='Ukloni'
                cancelButton='Odustani'
                okButtonColor='rose'
            />
        );
    };

    function cancelEdit() {
        setIsEditing(false);
        setErrors([]);
        setSuccess(false);
        getDoctor();
    }

    function SubmitFunction(e){
        e.preventDefault();

        const options = {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        fetch('/api/hospitals/update-doctor/'+doctor.id, options)
            .then(response => {
                if (response.status >= 200 && response.status < 300) {
                    setSuccess(true);
                    setIsEditing(false);
                    return [];
                }
                return response.json();
            })
            .then(data => {
                setErrors(data);
            });
    }

    function removeDoctor() {
        setIsDeleting(true);
        const options = {
            method: 'DELETE'
        };
        fetch('/api/hospitals/delete-doctor/' + doctor.id, options)
            .then(response => {
                if (response.status === 200) {
                    setSuccess(true);
                } else {
                    setDeleteError(true);
                }
            });
        updateUser();
    }

    const getSex = () => {
        if (data === '' || data.sex === null)
            return '';
        let sex = sexOptions.find(e => e.value === data.sex);

        return sex.label;
    }

    const setSex = (e) => {
        const {value, label} = e;
        onChange({target: {id: 'sex', value: value}});
    };

    const sexOptions = [
        { value: 'MALE', label: 'Muški' },
        { value: 'FEMALE', label: 'Ženski' },
        { value: 'OTHER', label: 'Ostalo' }
    ];

    const SexDropdown = () =>  {
        if (data === '')
            return ;
        let sex = sexOptions.find(e => e.value === data.sex);

        return (
            <div>
            <p style={{
                fontSize: '11px',
                marginTop: '23px',
                lineHeight: '5px',
                color: grayColor[1]
            }}>Spol</p>
            <Select
                defaultValue={sex}

                options={sexOptions}
                onChange={setSex}
            />
            </div>);
    };

    const birthDatePicker = () => {

        if (isEditing)
            return (
                <div style={{
                    font: '20px'
                }}>
                    <p style={{
                        fontSize: '11px',
                        marginTop: '23px',
                        lineHeight: '5px',
                        color: grayColor[1]
                    }}>Datum rođenja</p>
                    <DatePicker
                        className="CustomInput"
                        label="datum"
                        dateFormat="dd.MM.yyyy."
                        selected={data.birthDate !=null ? Date.parse(data.birthDate) :Date.parse(info)}
                        onChange={date => handleDateChange(date)}
                        locale="hr"
                        id="birthDate"
                    />
                </div>
            );
        else
            return (
                <div style={{
                    font: '20px'
                }}>
                    <CustomInput
                        labelText="Datum rođenja"
                        id="birthDate"
                        formControlProps={{
                            fullWidth: true
                        }}
                        inputProps={{
                            readOnly: true,
                            value: data.birthDate != null ? new Date(data.birthDate).toLocaleDateString('hr', {
                                day : 'numeric',
                                month : 'short',
                                year : 'numeric'
                            }) :""
                        }}
                        success={success}
                        error={errors.indexOf('birthDate') > -1}
                    />
                </div>
            )
    }


    const footer = () => {

        if (isDoctor)
            return null;

        if (isEditing)
            return (
                <CardFooter>
                    <Button color="secondary"
                            onClick={cancelEdit}
                    >Odustani</Button>
                    <Button color="primary"
                            onClick={SubmitFunction}
                    >Pohrani</Button>
                </CardFooter>);
        else
            return (<CardFooter>
                <Button
                    color="secondary"
                    onClick={() => {
                        if (success)
                            refreshList();
                        onBack();
                    }}
                >Nazad</Button>
                <div>
                    <Button color="primary"
                            onClick={() => setIsEditing(true)}
                            disabled={isDeleting}
                    >Uredi</Button>
                    {!isDeleting ?
                        <Button
                            color="rose"
                            onClick={() =>{onSetIsAlertOpen(true)}}
                        >Ukloni doktora</Button> :
                        null}
                </div>
                {deleteError ? <p>Dogodila se greška prilikom uklanjanja</p> : null}
            </CardFooter>);
    };

  return (
      <GridContainer>
          <GridItem xs={12} sm={12} md={10}>
      <Card>
          {deleteAlert()}
        <CardHeader color="primary">
          <h4 className={classes.cardTitleWhite}>Doktor</h4>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={5}>
              <CustomInput
                labelText="Korisničko ime"
                id="email"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                    readOnly: true,
                    disabled: isEditing,
                    value: data.email != null ? data.email:"" // tekst default
                }}
                onChange={onChange}
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Ime"
                id="firstName"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                    readOnly: !isEditing,
                    value: data.firstName != null ? data.firstName :"", // tekst default
                    variant: "outlined"
                }}
                onChange={onChange}
                success={success}
                error={errors.indexOf('firstName') > -1}
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <CustomInput
                labelText="Prezime"
                id="lastName"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                    readOnly: !isEditing,
                    value: data.lastName != null ? data.lastName :""
                }}
                onChange={onChange}
                success={success}
                error={errors.indexOf('lastName') > -1}
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
              <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                      labelText="Specijalnost"
                      id="speciality"
                      formControlProps={{
                          fullWidth: true
                      }}
                      inputProps={{
                          readOnly: !isEditing,
                          value: data.speciality != null ? data.speciality :""
                      }}
                      onChange={onChange}
                      success={success}
                      error={errors.indexOf('speciality') > -1}
                  />
              </GridItem>
              <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                      labelText="Broj liječničke licence"
                      id="licenceNumber"
                      formControlProps={{
                          fullWidth: true
                      }}
                      inputProps={{
                          readOnly: !isEditing,
                          value: data.licenceNumber != null ? data.licenceNumber :""
                      }}
                      onChange={onChange}
                      success={success}
                      error={errors.indexOf('licenceNumber') > -1}
                  />
              </GridItem>
              <GridItem xs={12} sm={12} md={4}>

                  {birthDatePicker()}
              </GridItem>
            <GridItem xs={12} sm={12} md={4}>
              <CustomInput
                labelText="OIB"
                id="oib"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                    readOnly: !isEditing,
                    value: data.oib != null ? data.oib :""
                }}
                onChange={onChange}
                success={success}
                error={errors.indexOf('oib') > -1}
              />
            </GridItem>
              <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                      labelText="Adresa"
                      id="address"
                      formControlProps={{
                          fullWidth: true
                      }}
                      inputProps={{
                          readOnly: !isEditing,
                          value: data.address != null ? data.address :""
                      }}
                      onChange={onChange}
                      success={success}
                      error={errors.indexOf('address') > -1}
                  />
              </GridItem>
              <GridItem xs={12} sm={12} md={4}>
              <CustomInput
                  labelText="Poštanski broj"
                  id="zipCode"
                  formControlProps={{
                      fullWidth: true
                  }}
                  inputProps={{
                      readOnly: !isEditing,
                      value: data.zipCode != null ? data.zipCode :""
                  }}
                  onChange={onChange}
                  success={success}
                  error={errors.indexOf('zipCode') > -1}
              />
            </GridItem>
              <GridItem xs={12} sm={12} md={4}>
                  {isEditing ?
                      SexDropdown() :

                  <CustomInput
                      labelText="Spol"
                      id="sex"
                      formControlProps={{
                          fullWidth: true
                      }}
                      inputProps={{
                          readOnly: true,
                          value: getSex()
                      }}
                      onChange={onChange}
                      success={success}
                      error={errors.indexOf('sex') > -1}
                  />}
              </GridItem>
          </GridContainer>
        </CardBody>
          {footer()}
      </Card>
      </GridItem>
        {isDoctor ?
              <GridItem xs={12} sm={12} md={10}>
                  <PasswordChangeCard {...props}/>
              </GridItem>
          : null}
      </GridContainer>
  );
}
