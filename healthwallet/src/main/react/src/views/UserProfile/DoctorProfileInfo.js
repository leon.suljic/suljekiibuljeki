import React, {useEffect} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import avatar from "assets/img/faces/marc.jpg";
import AlertDialog from "../../components/Alert/Alert";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function DoctorProfile(props) {

  const classes = useStyles();
  const { doctor, onBack, refreshList } = props;
  const [info,setInfo] = React.useState("");

  const [isDeleting, setIsDeleting] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const [error, setError] = React.useState(false);
  const [isDeleteAlertOpen, setIsDeleteAlertOpen] = React.useState(false);


    const deleteAlert = () => {
        return (
            <AlertDialog
                title='Ukloni doktora'
                message='Uklanjanjem doktora on više neće imati pristup Vašim podacima.'
                open={isDeleteAlertOpen}
                setOpen={setIsDeleteAlertOpen}
                onOk={removeDoctor}
                onCancel={() => setIsDeleting(false)}
                okButton='Izbriši račun'
                cancelButton='Odustani'
                okButtonColor='rose'
            />
        );
    };

    function removeDoctor() {
        setIsDeleting(true);
        const options = {
            method: 'POST'
        };
        let url = '/api/patients/doctors/remove/?email=' + doctor.email;
        if (props.patient != null && props.user.role === 'patient')
            url = url + '&patientEmail=' + props.patient.email;
        fetch(url, options)
            .then(response => {
                if (response.status === 200) {
                    setSuccess(true);
                } else {
                    setError(true);
                }
            });
    }

  return (
    <div>
        {deleteAlert()}
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Profil doktora</h4>
            </CardHeader>
            <CardBody>

              <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                    labelText="Ime"
                    id="firstName"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                        readOnly: true,
                        value: doctor.firstName
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                    labelText="Prezime"
                    id="lastName"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                        readOnly: true,
                        value: doctor.lastName
                    }}
                  />
                </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                      <CustomInput
                          labelText="Specijalnost"
                          id="speciality"
                          formControlProps={{
                              fullWidth: true
                          }}
                          inputProps={{
                              readOnly: true,
                              value: doctor.specialty
                          }}
                      />
                  </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                    <CustomInput
                        labelText="E-mail"
                        id="email"
                        formControlProps={{
                            fullWidth: true
                        }}
                        inputProps={{
                            readOnly: true,
                            value: doctor.email
                        }}
                    />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                    <CustomInput
                        labelText="Bolnica"
                        id="hospital"
                        formControlProps={{
                            fullWidth: true
                        }}
                        inputProps={{
                            readOnly: true,
                            value: doctor.hospital
                        }}
                    />
                </GridItem>
            </GridContainer>

            </CardBody>
            <CardFooter>
                <Button
                    color="secondary"
                    onClick={() => {
                        if (success)
                            refreshList();
                        onBack();
                    }}
                >Nazad</Button>
                {!isDeleting && !props.editRestriction ?
                <Button
                    color="rose"
                    onClick={()=> {setIsDeleteAlertOpen(true)}}
                >Ukloni doktora</Button> : null}
                {success ? <p>Doktor uspješno uklonjen</p> : null}
                {error ? <p>Dogodila se greška prilikom uklanjanja</p> : null}
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}
