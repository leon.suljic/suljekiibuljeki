import React, {useEffect} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";

import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import DatePicker from 'react-datepicker'
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import hr from 'date-fns/locale/hr';
import "react-datepicker/dist/react-datepicker.css";



const styles = {
    cardCategoryWhite: {
        color: "rgba(255,255,255,.62)",
        margin: "0",
        fontSize: "14px",
        marginTop: "0",
        marginBottom: "0"
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none"
    }
};

const useStyles = makeStyles(styles);

export default function PatientProfile(props) {

    const classes = useStyles();
    const { user,updateUser } = props;
    const [info,setInfo] = React.useState(new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate()));
    const [data, setData] = React.useState("");
//{ id: '', email: '', password: '', confirmationLink: '', forgottenPassCode : '', oib: '', firstName: '', lastName: '', birthDate: ''}
    registerLocale('hr', hr)
    useEffect(() => {
        if(user != null ){
            if(user.user == null){
                setInfo(user.role);
            } else {
                setData(user.user);
            }
        }
    }, [user]);

    function handleDateChange(selectedDate) {
        setData(oldForm => ({...oldForm, birthDate: selectedDate}));
    }

    function onChange(event) {
        const {id, value} = event.target;
        setData(oldForm => ({...oldForm, [id]: value}));
    }

    function SubmitFunction(e){
        e.preventDefault();

        const options = {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        fetch('/api/patients/update-my-info', options)
            .then(response => {
                if (response.status === 200) {
                    updateUser();
                }
            });

    }


    return (
        <div>
            <GridContainer>
                <GridItem xs={12} sm={12} md={8}>
                    <Card>
                        <CardHeader color="primary">
                            <h4 className={classes.cardTitleWhite}>Uredi svoj profil</h4>
                        </CardHeader>
                        <CardBody>
                            <GridContainer>
                                <GridItem xs={12} sm={12} md={5}>
                                    <CustomInput
                                        labelText="Korisničko ime"
                                        id="email"
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            disabled: true,
                                            value: data.email != null ? data.email:"" // tekst default
                                        }}
                                        onChange={onChange}
                                    />
                                </GridItem>
                            </GridContainer>
                            <GridContainer>
                                <GridItem xs={12} sm={12} md={6}>
                                    <CustomInput
                                        labelText="Ime"
                                        id="firstName"
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            disabled: false,
                                            value: data.firstName != null ? data.firstName :"" // tekst default
                                        }}
                                        onChange={onChange}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={6}>
                                    <CustomInput
                                        labelText="Prezime"
                                        id="lastName"
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            disabled: false,
                                            value: data.lastName != null ? data.lastName :""
                                        }}
                                        onChange={onChange}
                                    />
                                </GridItem>
                            </GridContainer>
                            <GridContainer>
                                <GridItem xs={12} sm={12} md={4}>
                                    <CustomInput
                                        labelText="OIB"
                                        id="oib"
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            disabled: false,
                                            value: data.oib != null ? data.oib :""
                                        }}
                                        onChange={onChange}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={4}>
                                    <CustomInput
                                        labelText="Adresa"
                                        id="address"
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            disabled: false,
                                            value: data.address != null ? data.address :""
                                        }}
                                        onChange={onChange}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={4}>
                                    <CustomInput
                                        labelText="Poštanski broj"
                                        id="zipCode"
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            disabled: false,
                                            value: data.zipCode != null ? data.zipCode :""
                                        }}
                                        onChange={onChange}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={4}>
                                    <CustomInput
                                        labelText="Spol"
                                        id="sex"
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            disabled: false,
                                            value: data.sex != null ? data.sex :""
                                        }}
                                        onChange={onChange}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={4}>
                                    <CustomInput
                                        labelText="Matični broj osiguranika"
                                        id="healthCardNumber"
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            disabled: false,
                                            value: data.healthCardNumber != null ? data.healthCardNumber :""
                                        }}
                                        onChange={onChange}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={4}>
                                    <CustomInput
                                        labelText="Nadziratelj"
                                        id="supervisor"
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            disabled: false,
                                            value: data.supervisor != null ? data.supervisor :""
                                        }}
                                        onChange={onChange}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={4}>
                                    <CustomInput
                                        labelText="PIN"
                                        id="pin"
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            disabled: false,
                                            value: data.pin != null ? data.pin :""
                                        }}
                                        onChange={onChange}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={4}>

                                    <CustomInput
                                        labelText="Datum rođenja"
                                        id="birthDate"
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            disabled: true,
                                            value: data.birthDate != null ? new Date(data.birthDate).toLocaleDateString('hr', {
                                                day : 'numeric',
                                                month : 'short',
                                                year : 'numeric'
                                            }) :""
                                        }}//TODO tu sam maknuo onchange ako bude slucajno prouzrocilo probleme stavi natrag
                                    />
                                    <DatePicker
                                        className="CustomInput"
                                        label="datum"
                                        dateFormat="dd.MM.yyyy."
                                        selected={data.birthDate !=null ? Date.parse(data.birthDate) :Date.parse(info)}
                                        onChange={date => handleDateChange(date)}
                                        locale="hr"
                                        id="birthdate"
                                    />
                                </GridItem>
                            </GridContainer>
                        </CardBody>
                        <CardFooter>
                            <Button color="primary"
                                    onClick={SubmitFunction}
                            >Ažuriraj podatke</Button>
                        </CardFooter>
                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    );
}
