import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import avatar from "assets/img/faces/marc.jpg";
import Upload from "../../components/FileUpload/Upload/Upload";
import MedicalRecord from "../../components/MedicalRecord/MedicalRecord";
import AlertDialog from "../../components/Alert/Alert";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const useStyles = makeStyles(styles);

export default function UploadCard(props) {
  const classes = useStyles();

  const {

  } = props;

  const [medFile, setMedFile] = React.useState(null);
  const [urlFile, setUrlFile] = React.useState(null);
  const [isAlertOpen, setIsAlertOpen] = React.useState(false);

  function setFile(file) {
    let url = null;
    if (file != null){
      url = URL.createObjectURL(file);
    }
    setUrlFile(url);
    setMedFile(file)
  }

  function onWrongFormatAlert() {
    setIsAlertOpen(true);
  }

  const deleteAlert = () => {
    return (
        <AlertDialog
            title='Neispravan format'
            message='Zahtijevani format je pdf. Pokušali ste dodati datoteku koja nije pdf. Upotrijebite neku drugu datoteku.'
            open={isAlertOpen}
            setOpen={setIsAlertOpen}
            onOk={() => {}}
            onCancel={() => {}}
            okButton='U redu'
            okButtonColor='primary'
        />
    );
  };

  return (
      <GridContainer>
        {deleteAlert()}
        {urlFile !== null ?
            <GridItem xs={12} sm={10} md={10} style={{
              minWidth: '350px'
            }}>
              <MedicalRecord
                  file={urlFile}
              />
            </GridItem> : null
        }
        <GridItem xs={12} sm={10} md={10} style={{
          minWidth: '350px'
        }}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Dodavanje nalaza</h4>
            </CardHeader>
            <CardBody>
              <Upload
                setMedFile={setFile}
                patient={props.patient}
                user={props.user}
                onWrongFormat={onWrongFormatAlert}
              />
            </CardBody>
          </Card>
        </GridItem>

      </GridContainer>
  );
}
